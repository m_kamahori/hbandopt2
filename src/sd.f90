subroutine shape_derivative
  use globals
  use io
  implicit none
  integer::i,j,k,ibril,ix,iy,itmp
  real(8),allocatable::sd_ev(:,:,:)
  real(8),allocatable::sd_sum(:)
  real(8)::rglrz

  allocate(sd_ev(nlsf,2,nbril))
  allocate(sd_sum(nlsf))

  sd_sum(:)=0.d0
  do ibril=1,nbril
     do k=1,2
        !calculate S_{ij}
        call sd_for_each_ev(k,ibril,sd_ev(:,k,ibril))
     end do !k
     !形状導関数を計算
     sd_sum(:)=sd_sum(:)+(sd_ev(:,2,ibril)-sd_ev(:,1,ibril))/(real(ev_slc(2,ibril))-real(ev_slc(1,ibril)))
  end do

  !形状導関数をupdatelsfに渡すための配列sdに代入
  open(1,file=file_sd)
  allocate(sd(0:xe,0:ye))
  itmp=0
  do iy=0,ye
     do ix=0,xe
        itmp=itmp+1
        sd(ix,iy)=sd_sum(itmp)
        if(sd(ix,iy).ne.0.d0) write(1,fmt_d3) xlsf(:,itmp),sd(ix,iy)
     end do
  end do
  close(1)

  deallocate(grid,sd_sum)

  ! call write_rmat(xe+1,xe+1,sd(0:xe,0:xe),200+step)

  call impose_symmetry_avg(xe,sd(:,:))

  ! call write_rmat(xe+1,xe+1,sd(0:xe,0:xe),300+step)
  ! do ix=0,xe
  !    do iy=0,ye
  !       if(sd(ix,iy) > 0.d0)then
  !          write(400+step,fmt_d3)ix/dble(xe)*avec(:,1)+iy/dble(ye)*avec(:,2),sd(ix,iy)
  !       elseif(sd(ix,iy) < 0.d0)then
  !          write(500+step,fmt_d3)ix/dble(xe)*avec(:,1)+iy/dble(ye)*avec(:,2),sd(ix,iy)
  !       end if
  !    end do
  ! end do
  
  
  deallocate(sd_ev,ugrid,dugrid)

contains

  subroutine sd_for_each_ev(evnum,ibril,sd)
    implicit none
    integer itmp,jtmp,i,j,k,ix,iy,flag,outdir
    real(8) lsf(0:xe,0:ye*2),ltcl
    real(8) v1(2),v2(2)
    real(8) x1(2),x2(2)
    real(8) dt1(2),dt2(2)
    real(8) vreg(nlsf)
    integer idx(0:6),mx(0:3),my(0:3)
    integer start,goal,selm,gelm
    integer,allocatable :: connect(:,:)
    integer iltc,jltc,ltcnd
    integer,intent(in)::evnum,ibril
    real(8)::rglrz
    real(8),intent(out)::sd(nlsf)
    type(lattice)::ltc(2,2)

    ! write(*,*) "# Shape derivative"
    rglrz=0.d0
    do i=1,nip
       rglrz=rglrz+(real(ugrid(i,evnum,ibril))**2+aimag(ugrid(i,evnum,ibril))**2)*menseki/dble(nip)
       ! rglrz=rglrz+(real(uin(i))**2+aimag(uin(i))**2)*ds
    end do

    ltcl=l1/dble(xe)
    idx=(/ 0, 1, 2, 3, 0, 1, 2/)
    mx=(/ 0, 1, 0, -1/)
    my=(/ -1, 0, 1, 0/)

    lsf(0:xe,0:ye)=lsfunc(:,:)

    ! 隣接関係を作成
    allocate(connect(2,edges(1)))
    do i=1,edges(1)! 各要素
       do j=i+1,edges(1)
          if(nd(1,i).eq.nd(2,j)) then
             connect(1,i)=j
             connect(2,j)=i
          else if(nd(2,i).eq.nd(1,j)) then
             connect(2,i)=j
             connect(1,j)=i
          end if
       end do
    end do

    jtmp=0
    sd(:)=0.d0
    vreg(:)=0.d0
    do iy=1,ye-1
       do ix=1,xe-1
          itmp=ix+(xe+1)*iy+1
          ! 格子点(ix,iy)を含む格子情報作成
          do i=ix,ix+1
             do j=iy,iy+1
                iltc=i-ix+1
                jltc=j-iy+1
                call lattice_info(lsf,ltcl,ix,iy,i,j,ltc(iltc,jltc))
             end do
          end do

          ! 各格子内で積分
          do i=ix,ix+1
             do j=iy,iy+1
                iltc=i-ix+1
                jltc=j-iy+1
                flag =ltc(iltc,jltc)%flag    !格子内に積分対象となる境界要素が存在する(1)orしない(0)
                if(flag.eq.1) then
                   if(l2e(i,j)%n.eq.0) cycle
                   ltcnd=ltc(iltc,jltc)%n       !移動する節点を含む境界を構成する節点数
                   if(ltcnd.eq.2) then
                      x1(:)=ltc(iltc,jltc)%x1(:,1)   !第１節点格子内座標
                      x2(:)=ltc(iltc,jltc)%x2(:,1)   !第２節点格子内座標
                      v1(:)=ltc(iltc,jltc)%v1(:,1)   !第１節点の速度ベクトル
                      v2(:)=ltc(iltc,jltc)%v2(:,1)   !第２節点の速度ベクトル
                      start=ltc(iltc,jltc)%start(1)  !第１節点の位置(0:下辺、1:右辺、2:上辺、3:左辺)
                      goal =ltc(iltc,jltc)%goal(1)   !第２節点の位置(0:下辺、1:右辺、2:上辺、3:左辺)
                      outdir=ltc(iltc,jltc)%outdir(1)!境界要素の始点(1)or終点(2)が4格子の外と繋がっている
                      ! 格子点まわりの格子
                      call near_lattice(ix,iy,i,j,x1,x2,v1,v2,start,goal,&
                           outdir,ltc,ltcl,connect,evnum,ibril,dt1,dt2,selm,gelm,vreg(itmp),sd(itmp))
                      ! ひとつ外側の格子
                      call nbr_lattice(ix,iy,i,j,lsf,start,goal,outdir,ltc,ltcl,connect,evnum,ibril,&
                           dt1,dt2,selm,gelm,vreg(itmp),sd(itmp))
                   else if(ltcnd.eq.4) then
                      do k=1,2
                         x1(:)=ltc(iltc,jltc)%x1(:,k)   !第１節点格子内座標
                         x2(:)=ltc(iltc,jltc)%x2(:,k)   !第２節点格子内座標
                         v1(:)=ltc(iltc,jltc)%v1(:,k)   !第１節点の速度ベクトル
                         v2(:)=ltc(iltc,jltc)%v2(:,k)   !第２節点の速度ベクトル
                         start=ltc(iltc,jltc)%start(k)  !第１節点の位置(0:下辺、1:右辺、2:上辺、3:左辺)
                         goal =ltc(iltc,jltc)%goal(k)   !第２節点の位置(0:下辺、1:右辺、2:上辺、3:左辺)
                         outdir=ltc(iltc,jltc)%outdir(k)!境界要素の始点(1)or終点(2)が4格子の外と繋がっている
                         ! 格子点まわりの格子
                         call near_lattice(ix,iy,i,j,x1,x2,v1,v2,start,goal,&
                              outdir,ltc,ltcl,connect,evnum,ibril,dt1,dt2,selm,gelm,vreg(itmp),sd(itmp))
                         call nbr_lattice(ix,iy,i,j,lsf,start,goal,outdir,ltc,ltcl,connect,evnum,ibril,&
                              dt1,dt2,selm,gelm,vreg(itmp),sd(itmp))
                      end do
                   end if
                end if
             end do
          end do
          ! if(vreg(itmp).eq.0.d0) vreg(itmp)=1.d0
          ! sd(itmp)=sd(itmp)/vreg(itmp)
          ! sd(itmp)=sd(itmp)*w**2.d0*(epsln(2)-epsln(1))/vreg(itmp)
          ! if(sd(itmp).ne.0.d0.and.ibril.eq.3.and.evnum.eq.1)then
          !    jtmp=jtmp+1
          !    write(2222,*) xlsf(:,itmp),sd(itmp)
          ! end if
       end do
    end do
    sd(:)=sd(:)/rglrz
    ! do i=1,nlsf
    !    if(sd(i).ne.0.d0) write(*,*)evnum,ibril,sd(i)
    ! end do

    ! ! 出力
    ! file_sens=trim(trim(objdirname)//"sd.el2")
    ! open(1,file=file_sens)
    ! itmp=1
    ! do iy=0,ye
    !    do ix=0,xe
    !       write(1,*) ix,iy,sd(itmp),vreg(itmp)
    !       itmp=itmp+1
    !    end do
    ! end do
    ! close(1)

  end subroutine sd_for_each_ev

  ! function sol(a0,a1,a2,a3) result(ans)
  !   implicit none
  !   real(8) a0,a1,a2,a3
  !   real(8) a,b,c
  !   real(8) p,q,d,ans
  !   complex(8) w,u,v,y
  !   complex(8) x(3)
  !   integer i

  !   a=a2/a3
  !   b=a1/a3
  !   c=a0/a3

  !   w=(-1.d0+sqrt(cmplx(-3.d0,kind=8)))/2.d0
  !   p=-a**2.d0/9.d0+b/3.d0
  !   q=2.d0/27.d0*a**3.d0-a*b/3.d0+c
  !   d=q**2.d0+4.d0*p**3.d0
  !   u=((-cmplx(q,kind=8)+sqrt(dcmplx(d)))/2.d0)**(1.d0/3.d0)

  !   if(u.ne.0.d0) then
  !      v=-dcmplx(p)/u
  !      x(1)=u+v-dcmplx(a)/3.d0
  !      x(2)=u*w+v*w**2.d0-dcmplx(a)/3.d0
  !      x(3)=u*w**2.d0+v*w-dcmplx(a)/3.d0
  !   else
  !      y=(-dcmplx(q))**(1.d0/3.d0)
  !      x(1)=y-dcmplx(a)/3.d0
  !      x(2)=y*w-dcmplx(a)/3.d0
  !      x(3)=y*w**2.d0-dcmplx(a)/3.d0
  !   end if

  !   ans=-1.d0
  !   do i=1,3
  !      if((real(x(i)).ge.-1.d-6).and.(real(x(i)).le.(1.d0+1.d-6))) then
  !         ans=real(x(i))
  !         if(ans.lt.0.d0) ans=0.d0
  !         if(ans.gt.1.d0) ans=1.d0
  !      end if
  !   end do  

  ! end function sol

end subroutine shape_derivative
