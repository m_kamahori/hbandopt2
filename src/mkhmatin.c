#include "harith.h"

extern void assemble_hmatrix_in(pblock b, uint hrows, uint hcols, phmatrix G, real eps_aca, 
				uint dflag[hrows*hcols],
				field *xfwd, field *yfwd, uint nin, uint ne);

void
mkhmatin(pcluster root1, pcluster root2, real *eta, real eps_aca,
	 uint hrows, uint hcols, uint dflag[hrows*hcols], 
	 uint nin, uint ne, field xfwd[2*ne], field yfwd[3*nin]){
  pblock block;
  phmatrix hmat;

  /* ブロッククラスターツリーを生成 */
  block = build_nonstrict_block(root1, root2, eta, admissible_max_cluster);

  /* H-matrix構造体を生成 */
  hmat = build_from_block_hmatrix(block,0);

  /* ACAで行列要素計算 */
  assemble_hmatrix_in(block, hrows, hcols, hmat, eps_aca, dflag, xfwd, yfwd, nin, ne);

  del_block(block);
  del_hmatrix(hmat);
}

