module kernelfunc
  use math
  use globals
  use functions
  implicit none
  type :: type_kernels
     procedure(kernel_),pointer,nopass :: kernel
     procedure(kernel_R_),pointer,nopass :: kernel_R
     procedure(kernel_S_),pointer,nopass :: kernel_S
  end type type_kernels

  interface
     subroutine kernel_(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, out)
       real(8),intent(in) :: r_vec(2)
       real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
       real(8),intent(in) :: r
       complex(8),intent(in) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
       complex(8),intent(out) :: out(2,2)
     end subroutine kernel_

     subroutine kernel_R_(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, out)
       use globals
       real(8),intent(in) :: r_vec(2)
       real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
       real(8),intent(in) :: r
       complex(8),intent(in) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
       complex(8),intent(out) :: out(2,2)
     end subroutine kernel_R_

     subroutine kernel_S_(r_vec, n_vec_x, n_vec_y, r, out)
       use globals
       real(8),intent(in) :: r_vec(2)
       real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
       real(8),intent(in) :: r
       complex(8),intent(out) :: out(2,2)
     end subroutine kernel_S_
  end interface
contains
  ! 積分
  ! 複数のkernelを同時に積分する
  subroutine integrator(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, kernels, out)
    real(8),intent(in) :: x_vec(2) ! 選点or観測点
    real(8),intent(in) :: y1_vec(2), y2_vec(2) ! 要素端
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    type(type_kernels),intent(in) :: kernels(:)
    complex(8),intent(out) :: out(2,2,size(kernels))
    real(8) :: r_vec(2)
    real(8) :: r
    complex(8) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    integer :: i, j!, n_kernels
    real(8) :: dist
    complex(8) :: tmp(2,2)
    complex(8) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8) :: I_R(2,2,size(kernels)), F1(2,2,size(kernels)), F2(2,2,size(kernels))
    real(8) :: xi, xi0, p_vec(2), d_vec(2)
    integer,parameter :: pp=3

    ! 初期化
    out(:,:,:) = zero

    ! 観測点から要素の中心までの距離
    p_vec(:) = 0.5d0*(y1_vec(:)+y2_vec(:))-x_vec(:)
    dist = sqrt(dot_product(p_vec,p_vec))
    if(kT*dist > thres) then       
       ! Hankel関数を計算して積分
       do i=1,ng
          !r_vec(:) = 0.5d0*((1.d0-gzai(i))*y1_vec(:)+(1.d0+gzai(i))*y2_vec(:))-x_vec(:)
          r_vec(:) = x_vec(:) - 0.5d0*((1.d0-gzai(i))*y1_vec(:)+(1.d0+gzai(i))*y2_vec(:))
          r = sqrt(dot_product(r_vec(:),r_vec(:)))
          h0_kTr = besh0(kT*r)
          h1_kTr = besh1(kT*r)
          h0_kLr = besh0(kL*r)
          h1_kLr = besh1(kL*r)
          do j=1,size(kernels)
             call kernels(j)%kernel(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, tmp)
             out(:,:,j) = out(:,:,j) + wei(i)*tmp(:,:)
          end do
       end do
       out(:,:,:) = out(:,:,:)*le*0.5d0
       return
    else
       ! 正則項の積分
       ! Bnを計算して積分
       I_R(:,:,:) = zero
       do i=1,ng
          !r_vec(:) = 0.5d0*((1.d0-gzai(i))*y1_vec(:)+(1.d0+gzai(i))*y2_vec(:))-x_vec(:)
          r_vec(:) = x_vec(:) - 0.5d0*((1.d0-gzai(i))*y1_vec(:)+(1.d0+gzai(i))*y2_vec(:))
          r = sqrt(dot_product(r_vec(:),r_vec(:)))
          call array_Bn(kT*r,Bn_kTr)
          call array_Bn(kL*r,Bn_kLr)
          do j=1,size(kernels)
             call kernels(j)%kernel_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, tmp)
             I_R(:,:,j) = I_R(:,:,j) + wei(i)*tmp(:,:)
          end do
       end do
       ! 特異項の積分
       ! 観測点から一番近い要素上の無次元座標xi0を求める
       d_vec(:) = y2_vec(:)-y1_vec(:)
       xi0 = -2.d0*dot_product(p_vec,d_vec)/le**2
       if(xi0>1.d0) xi0 = 1.d0
       if(xi0<-1.d0) xi0 = -1.d0

       ! -1->xi0の積分
       F1(:,:,:) = zero
       if(xi0.ne.-1.d0) then ! xi0=-1なら積分する必要なし
          do i=1,ng
             xi = -(1.d0+xi0)*((1.d0-gzai(i))*0.5d0)**pp + xi0
             !r_vec(:) = 0.5d0*((1.d0-xi)*y1_vec(:)+(1.d0+xi)*y2_vec(:))-x_vec(:)
             r_vec(:) = x_vec(:) - 0.5d0*((1.d0-xi)*y1_vec(:)+(1.d0+xi)*y2_vec(:))
             r = sqrt(dot_product(r_vec,r_vec))
             do j=1,size(kernels)
                call kernels(j)%kernel_S(r_vec, n_vec_x, n_vec_y, r, tmp)
                F1(:,:,j) = F1(:,:,j) + wei(i)*tmp(:,:)*((1.d0-gzai(i))*0.5d0)**(pp-1)
             end do
          enddo
          F1(:,:,:) = F1(:,:,:)*(1.d0+xi0)*pp*0.5d0
       endif
       ! xi0->1の積分
       F2(:,:,:) = zero
       if(xi0.ne.1.d0) then ! xi0=1なら積分する必要なし
          do i=1,ng
             xi = (1.d0-xi0)*((1.d0+gzai(i))*0.5d0)**pp + xi0
             !r_vec(:) = 0.5d0*((1.d0-xi)*y1_vec(:)+(1.d0+xi)*y2_vec(:))-x_vec(:)
              r_vec(:) = x_vec(:) - 0.5d0*((1.d0-xi)*y1_vec(:)+(1.d0+xi)*y2_vec(:))
             r = sqrt(dot_product(r_vec,r_vec))
             do j=1,size(kernels)
                call kernels(j)%kernel_S(r_vec, n_vec_x, n_vec_y, r, tmp)
                F2(:,:,j) = F2(:,:,j) + wei(i)*tmp(:,:)*((1.d0+gzai(i))*0.5d0)**(pp-1)
             end do
          enddo
          F2(:,:,:) = F2(:,:,:)*(1.d0-xi0)*pp*0.5d0
       endif
       out(:,:,:) = (I_R(:,:,:)+F1(:,:,:)+F2(:,:,:))*le*0.5d0
       return
    end if
  end subroutine integrator

  ! Dirichletの成分 (Burton-Millerなし)
  subroutine po_dirichlet(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: x_vec(2) ! 選点or観測点
    real(8),intent(in) :: y1_vec(2), y2_vec(2) ! 要素端
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    type(type_kernels) :: kernels(1)
    complex(8) :: tmp(2,2,1)

    kernels(1)%kernel => kernel_G
    kernels(1)%kernel_R => kernel_G_R
    kernels(1)%kernel_S => kernel_G_S
    call integrator(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, kernels, tmp)
    out(:,:) = -tmp(:,:,1)
  end subroutine po_dirichlet

  subroutine po_dirichlet_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le
    complex(8),intent(out) :: out(2,2)

    call integ_G_diag(d_vec, n_vec_x, n_vec_y, le, out)
    out(:,:) = -out(:,:)
  end subroutine po_dirichlet_diag

  ! dirichletの成分 (Burton-Millerあり)
  subroutine po_dirichlet_b(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, out)!, out_st)
    real(8),intent(in) :: x_vec(2) ! 選点or観測点
    real(8),intent(in) :: y1_vec(2), y2_vec(2) ! 要素端
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    type(type_kernels) :: kernels(2)
    complex(8) :: tmp(2,2,2)

    kernels(1)%kernel => kernel_G
    kernels(1)%kernel_R => kernel_G_R
    kernels(1)%kernel_S => kernel_G_S
    kernels(2)%kernel => kernel_Hstar
    kernels(2)%kernel_R => kernel_Hstar_R
    kernels(2)%kernel_S => kernel_Hstar_S
    call integrator(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, kernels, tmp)
    out(:,:) = -tmp(:,:,1)-alpha*tmp(:,:,2)
  end subroutine po_dirichlet_b

  subroutine po_dirichlet_b_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le
    complex(8),intent(out) :: out(2,2)
    integer :: k, l

    call integ_G_diag(d_vec, n_vec_x, n_vec_y, le, out)
    out(:,:) = -out(:,:)
    do l=1,2
       do k=1,2
          out(k,l) = out(k,l)+alpha*0.5d0*k_delta(k,l)
       end do
    end do
  end subroutine po_dirichlet_b_diag

  ! Neumannの成分 (Burton-Millerなし)
  subroutine po_neumann(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, out)!, out_st)
    real(8),intent(in) :: x_vec(2) ! 選点or観測点
    real(8),intent(in) :: y1_vec(2), y2_vec(2) ! 要素端
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    type(type_kernels) :: kernels(1)
    complex(8) :: tmp(2,2,1)

    kernels(1)%kernel => kernel_H
    kernels(1)%kernel_R => kernel_H_R
    kernels(1)%kernel_S => kernel_H_S
    call integrator(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, kernels, tmp)
    out(:,:) = tmp(:,:,1)
  end subroutine po_neumann

  subroutine po_neumann_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    integer :: k, l

    do l=1,2
       do k=1,2
          if(k.eq.l) then
             out(k,l) = (0.5d0,0.d0)
          else
             out(k,l) = zero
          end if
       end do
    end do
  end subroutine po_neumann_diag

  ! Neumannの成分 (Burtom-Millerあり)
  subroutine po_neumann_b(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: x_vec(2) ! 選点or観測点
    real(8),intent(in) :: y1_vec(2), y2_vec(2) ! 要素端
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    type(type_kernels) :: kernels(2)
    complex(8) :: tmp(2,2,2), K1(2,2), K2(2,2)

    kernels(1)%kernel => kernel_H
    kernels(1)%kernel_R => kernel_H_R
    kernels(1)%kernel_S => kernel_H_S
    kernels(2)%kernel => kernel_T
    kernels(2)%kernel_R => kernel_T_R
    kernels(2)%kernel_S => kernel_T_S
    call integrator(x_vec, y1_vec, y2_vec, n_vec_x, n_vec_y, le, kernels, tmp)
    !call func_K(y1_vec-x_vec, n_vec_x, K1)
    call func_K(x_vec-y1_vec, n_vec_x, K1)
    !call func_K(y2_vec-x_vec, n_vec_x, K2)
    call func_K(x_vec-y2_vec, n_vec_x, K2)
    out(:,:) = tmp(:,:,1)+alpha*(rho*w**2*tmp(:,:,2)+K1(:,:)-K2(:,:))
  end subroutine po_neumann_b

  subroutine po_neumann_b_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le ! 要素長
    complex(8),intent(out) :: out(2,2)
    integer :: i
    !real(8) :: r_vec(2), r
    !complex(8) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8) :: tmp(2,2), K(2,2)
    
    call integ_T_diag(d_vec, n_vec_x, n_vec_y, le, out)
    call func_K(-0.5d0*d_vec(:), n_vec_x, K)
    out(:,:) = out(:,:)*rho*w**2 -2.d0*K(:,:)
    out(:,:) = alpha*out(:,:)
    out(1,1) = out(1,1) + 0.5d0
    out(2,2) = out(2,2) + 0.5d0
    
  end subroutine po_neumann_b_diag
  
  !
  ! 各積分作用素の核
  !
  subroutine kernel_G(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: r
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    complex(8),intent(in) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: A, B
    
    A = func_A(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    B = func_B(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)

    do l=1,2
       do k=1,2
          out(k,l) = A*k_delta(k,l)+B*r_vec(k)*r_vec(l)/r**2
       enddo
    enddo
    out(:,:) = out(:,:)*ione/(4.d0*dm)
    
  end subroutine kernel_G

  subroutine kernel_G_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: sum_an, sum_bn
    
    sum_an = func_sum_an(r, Bn_kTr, Bn_kLr)
    sum_bn = func_sum_bn(r, Bn_kTr, Bn_kLr)
    do l=1,2
       do k=1,2
          out(k,l) = beta*k_delta(k,l)-ione*(dl+dm)*r_vec(k)*r_vec(l)/(pi*(dl+2.d0*dm)*r**2)&
               +(sum_an*k_delta(k,l)+sum_bn*2.d0*(r_vec(k)*r_vec(l))/(kT*r**2)) /(kT*r)
       enddo
    enddo
    out(:,:) = out(:,:)*ione/(4.d0*dm)
  end subroutine kernel_G_R

  subroutine kernel_G_S(r_vec, n_vec_x, n_vec_y, r, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(out) :: out(2,2)
    integer:: k, l

    out(:,:) = zero
    do l=1,2
       do k=1,2
          if(k.eq.l) out(k,l) = 1.d0
       end do
    end do
    out(:,:) = -(dl+3.d0*dm)/(4.d0*pi*dm*(dl+2.d0*dm))*log(r)*out(:,:)
  end subroutine kernel_G_S

  ! kernel_Gの対角項の積分
  subroutine integ_G_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le
    complex(8),intent(out) :: out(2,2)
    complex(8) :: I_S(2,2), I_R(2,2)
    integer :: k, l
    integer :: gm
    complex(8) :: tmp(2,2)
    complex(8) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    real(8) :: r_vec(2), r

    ! 特異項
    I_S(:,:) = 2.d0*(log(0.5d0*le)-1.d0) ! logrの[0,1]積分
    I_S(:,:) = -(dl+3.d0*dm)/(4.d0*pi*dm*(dl+2.d0*dm))*I_S(:,:)
    
    do l=1,2
       do k=1,2
          I_S(k,l) = I_S(k,l)*k_delta(k,l)
       end do
    end do
    ! 正則高
    I_R(:,:) = zero
    do gm=1,ng
       r_vec(:) = 0.25*(gzai(gm)+1.d0)*d_vec(:)
       r = sqrt(dot_product(r_vec,r_vec))
       call array_Bn(kT*r,Bn_kTr)
       call array_Bn(kL*r,Bn_kLr)
       call kernel_G_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, tmp)
       I_R(:,:) = I_R(:,:) + wei(gm)*tmp(:,:)
    enddo
    out(:,:) = le*0.5d0*(I_S(:,:)+I_R(:,:))
  end subroutine integ_G_diag
  
  subroutine kernel_H(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: r
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    complex(8),intent(in) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    real(8) :: rdotn
    complex(8) :: D, E, F

    rdotn = dot_product(r_vec,n_vec_y)
    D = func_D(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    E = func_E(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    F = func_F(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)

    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(D*k_delta(k,l)+E*r_vec(k)*r_vec(l)/r**2)+D*r_vec(l)*n_vec_y(k)+F*r_vec(k)*n_vec_y(l)
       enddo
    enddo
    out(:,:) = -out(:,:)*ione/(4.d0*r)
  end subroutine kernel_H

  ! kernel_Hの正則項
  subroutine kernel_H_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: sum_dn, sum_en, sum_fn
    real(8) :: rdotn

    rdotn = dot_product(r_vec,n_vec_y)
    sum_dn = func_sum_dn(r, Bn_kTr, Bn_kLr)
    sum_en = func_sum_en(r, Bn_kTr, Bn_kLr)
    sum_fn = func_sum_fn(r, Bn_kTr, Bn_kLr)

    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(k_delta(k,l)*sum_dn+r_vec(k)*r_vec(l)/r**2*sum_en)+r_vec(l)*n_vec_y(k)*sum_dn+r_vec(k)*n_vec_y(l)*sum_fn
       enddo
    enddo
    out(:,:) = -out(:,:)*ione*kT/(4.d0*r)
  end subroutine kernel_H_R

  ! kernel_Hの特異項
  subroutine kernel_H_S(r_vec, n_vec_x, n_vec_y, r, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    real(8) :: rdotn

    rdotn = dot_product(r_vec,n_vec_y)
    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(dm*k_delta(k,l)+2.d0*(dl+dm)*r_vec(k)*r_vec(l)/r**2)&
               +dm*(r_vec(l)*n_vec_y(k)-r_vec(k)*n_vec_y(l))
       enddo
    enddo
    out(:,:) = out(:,:)/(2.d0*pi*r**2*(dl+2.d0*dm))
  end subroutine kernel_H_S

  subroutine kernel_Hstar(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: D, E, F
    real(8) :: rdotn
    
    D = func_D(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    E = func_E(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    F = func_F(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)

    rdotn = dot_product(r_vec, n_vec_x)

    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(D*k_delta(k,l)+E*r_vec(k)*r_vec(l)/r**2)+D*r_vec(k)*n_vec_x(l)+F*r_vec(l)*n_vec_x(k)
       end do
    end do
    out(:,:) = out(:,:)*ione/(4.d0*r)
  end subroutine kernel_Hstar

  subroutine kernel_Hstar_S(r_vec, n_vec_x, n_vec_y, r, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    real(8) :: rdotn

    rdotn = dot_product(r_vec,n_vec_x)
    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(dm*k_delta(k,l)+2.d0*(dl+dm)*r_vec(k)*r_vec(l)/r**2)&
               +dm*(r_vec(k)*n_vec_x(l)-r_vec(l)*n_vec_x(k))
       enddo
    enddo
    out(:,:) = -out(:,:)/(2.d0*pi*r**2*(dl+2.d0*dm))
  end subroutine kernel_Hstar_S

  subroutine kernel_Hstar_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: sum_dn, sum_en, sum_fn
    real(8) :: rdotn

    rdotn = dot_product(r_vec,n_vec_x)
    
    sum_dn = func_sum_dn(r, Bn_kTr, Bn_kLr)
    sum_en = func_sum_en(r, Bn_kTr, Bn_kLr)
    sum_fn = func_sum_fn(r, Bn_kTr, Bn_kLr)

    do l=1,2
       do k=1,2
          out(k,l) = rdotn*(k_delta(k,l)*sum_dn+r_vec(k)*r_vec(l)/r**2*sum_en)+r_vec(k)*n_vec_x(l)*sum_dn+r_vec(l)*n_vec_x(k)*sum_fn
       enddo
    enddo
    out(:,:) = out(:,:)*ione*kT/(4.d0*r)
  end subroutine kernel_Hstar_R

  subroutine kernel_T(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    complex(8) :: A, B
    real(8) :: rdotnx, rdotny, nxdotny
    real(8) :: tmp1(2,2), tmp2(2,2)
    
    A = func_A(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    B = func_B(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)

    rdotnx = dot_product(r_vec,n_vec_x)
    rdotny = dot_product(r_vec,n_vec_y)
    nxdotny = dot_product(n_vec_x,n_vec_y)

    do l=1,2
       do k=1,2
          tmp1(k,l) = dl*n_vec_x(k)*n_vec_y(l)+dm*(nxdotny*k_delta(k,l)+n_vec_x(l)*n_vec_y(k))
          tmp2(k,l) = r_vec(l)*(dl*rdotny*n_vec_x(k)+dm*(nxdotny*r_vec(k)+rdotnx*n_vec_y(k)))
       end do
    end do
    out(:,:) = ione/(4.d0*dm)*(A*tmp1(:,:)+B*tmp2(:,:)/r**2)
  end subroutine kernel_T

  subroutine kernel_T_S(r_vec, n_vec_x, n_vec_y, r, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(out) :: out(2,2)
    integer :: k, l
    real(8) :: rdotnx, rdotny, nxdotny

    rdotnx = dot_product(r_vec,n_vec_x)
    rdotny = dot_product(r_vec,n_vec_y)
    nxdotny = dot_product(n_vec_x,n_vec_y)
    
    do l=1,2
       do k=1,2
          out(k,l) = dl*n_vec_x(k)*n_vec_y(l)+dm*(nxdotny*k_delta(k,l)+n_vec_x(l)*n_vec_y(k))
       end do
    end do
    out(:,:) = -(dl+3.d0*dm)/(4.d0*pi*dm*(dl+2.d0*dm))*out(:,:)*log(r)
  end subroutine kernel_T_S

  subroutine kernel_T_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: r
    complex(8),intent(in) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8),intent(out) :: out(2,2)
    complex(8) :: sum_an, sum_bn
    real(8) :: tmp1(2,2), tmp2(2,2)
    real(8) :: rdotnx, rdotny, nxdotny
    integer :: k, l

    rdotnx = dot_product(r_vec,n_vec_x)
    rdotny = dot_product(r_vec,n_vec_y)
    nxdotny = dot_product(n_vec_x,n_vec_y)
    
    sum_an = func_sum_an(r, Bn_kTr, Bn_kLr)
    sum_bn = func_sum_bn(r, Bn_kTr, Bn_kLr)

    do l=1,2
       do k=1,2
          tmp1(k,l) = dl*n_vec_x(k)*n_vec_y(l)+dm*(nxdotny*k_delta(k,l)+n_vec_x(l)*n_vec_y(k))
          tmp2(k,l) = r_vec(l)*(dl*rdotny*n_vec_x(k)+dm*(nxdotny*r_vec(k)+rdotnx*n_vec_y(k)))
       end do
    end do
    out(:,:) = ione/(4.d0*dm)*(&
         (beta+sum_an/(kT*r))*tmp1(:,:)+(-ione*(dl+dm)/(pi*(dl+2.d0*dm))+2.d0*sum_bn/(kT**2*r))*tmp2(:,:)/r**2&
         )
  end subroutine kernel_T_R

  subroutine integ_T_diag(d_vec, n_vec_x, n_vec_y, le, out)
    real(8),intent(in) :: d_vec(2)
    real(8),intent(in) :: n_vec_x(2), n_vec_y(2)
    real(8),intent(in) :: le
    complex(8),intent(out) :: out(2,2)
    complex(8) :: I_S(2,2), I_R(2,2)
    integer :: k, l
    integer :: gm
    complex(8) :: tmp(2,2)
    complex(8) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    real(8) :: r_vec(2), r
    real(8) :: nxdotny
    
    nxdotny = dot_product(n_vec_x,n_vec_y)

    ! 特異項
    I_S(:,:) = 2.d0*(log(0.5d0*le)-1.d0) ! logrの[0,1]積分
    I_S(:,:) = -(dl+3.d0*dm)/(4.d0*pi*dm*(dl+2.d0*dm))*I_S(:,:)
    
    do l=1,2
       do k=1,2
          I_S(k,l) = I_S(k,l)*(dl*n_vec_x(k)*n_vec_y(l)+dm*(nxdotny*k_delta(k,l)+n_vec_x(l)*n_vec_y(k)))
       end do
    end do
    ! 正則高
    I_R(:,:) = zero
    do gm=1,ng
       r_vec(:) = -0.25*(gzai(gm)+1.d0)*d_vec(:)
       r = sqrt(dot_product(r_vec,r_vec))
       call array_Bn(kT*r,Bn_kTr)
       call array_Bn(kL*r,Bn_kLr)
       call kernel_G_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, tmp)
       I_R(:,:) = I_R(:,:) + wei(gm)*tmp(:,:)
    enddo
    out(:,:) = le*0.5d0*(I_S(:,:)+I_R(:,:))
  end subroutine integ_T_diag

  subroutine func_K(r_vec, n_vec_x, out)
    real(8),intent(in) :: r_vec(2)
    real(8),intent(in) :: n_vec_x(2)
    complex(8),intent(out) :: out(2,2)
    real(8) :: rt_vec(2), nt_vec_x(2)
    real(8) :: r
    complex(8) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8) :: D, E, F
    real(8) :: r_dot_nx, rt_dot_nx
    integer :: k, l
    complex(8) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8) :: sum_dn, sum_en, sum_fn
    real(8) :: tmp

    r = sqrt(dot_product(r_vec(:),r_vec(:)))
    if(kT*r > thres) then
       h0_kTr = besh0(kT*r)
       h1_kTr = besh1(kT*r)
       h0_kLr = besh0(kL*r)
       h1_kLr = besh1(kL*r)

       D = func_D(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
       E = func_E(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
       F = func_F(r, h0_kTr, h1_kTr, h0_kLr, h1_kLr)
    else
       call array_Bn(kT*r,Bn_kTr)
       call array_Bn(kL*r,Bn_kLr)
       sum_dn = func_sum_dn(r, Bn_kTr, Bn_kLr)
       sum_en = func_sum_en(r, Bn_kTr, Bn_kLr)
       sum_fn = func_sum_fn(r, Bn_kTr, Bn_kLr)
       tmp = 2.d0/(pi*(dl+2.d0*dm)*r)
       D = kT*func_sum_dn(r, Bn_kTr, Bn_kLr) + ione*dm*tmp
       E = kT*func_sum_en(r, Bn_kTr, Bn_kLr) + ione*2.d0*(dl+dm)*tmp
       F = kT*func_sum_fn(r, Bn_kTr, Bn_kLr) - ione*dm*tmp
    end if

    ! rt_vec(1) = r_vec(2)
    ! rt_vec(2) = -r_vec(1)
    ! nt_vec_x(1) = n_vec_x(2)
    ! nt_vec_x(2) = -n_vec_x(1)
    
    rt_vec(1) = -r_vec(2)
    rt_vec(2) = r_vec(1)
    nt_vec_x(1) = -n_vec_x(2)
    nt_vec_x(2) = n_vec_x(1)

    r_dot_nx = dot_product(r_vec,n_vec_x)
    rt_dot_nx = dot_product(rt_vec,n_vec_x)

    do l=1,2
       do k=1,2
          ! out(k,l) = D*(dl*rt_vec(l)*n_vec_x(k)+dm*(rt_dot_nx*k_delta(k,l)+rt_vec(k)*n_vec_x(l)))&
          !      +E*dm*r_vec(l)*(rt_dot_nx*r_vec(k)+r_dot_nx*rt_vec(k))/r**2&
          !      +F*(-dl*rt_vec(l)*n_vec_x(k)+dm*(-r_vec(k)*nt_vec_x(l)+r_dot_nx*e_eps(k,l)))
          out(k,l) = dl*(D-F)*rt_vec(l)*n_vec_x(k)+dm*&
               ( D*(rt_dot_nx*k_delta(k,l)+rt_vec(k)*n_vec_x(l))+E*r_vec(l)*(rt_dot_nx*r_vec(k)+r_dot_nx*rt_vec(k))/r**2 &
               -F*(r_vec(k)*nt_vec_x(l)+r_dot_nx*e_eps(k,l)) )
       end do
    end do
    !out(:,:) = ione/(4.d0*r)*out(:,:)
    out(:,:) = -ione/(4.d0*r)*out(:,:)
  end subroutine func_K
  

  subroutine test
    real(8) :: r_vec(2)
    real(8) :: n_vec_x(2), n_vec_y(2)
    real(8) :: r
    complex(8) :: h0_kTr, h1_kTr, h0_kLr, h1_kLr
    complex(8) :: Bn_kTr(2:ns), Bn_kLr(2:ns)
    complex(8) :: Hstar(2,2), Hstar_R(2,2), Hstar_S(2,2)

    r_vec(1) = 0.1d0
    r_vec(1) = 0.2d0
    r = sqrt(dot_product(r_vec(:),r_vec(:)))
    h0_kTr = besh0(kT*r)
    h1_kTr = besh1(kT*r)
    h0_kLr = besh0(kL*r)
    h1_kLr = besh1(kL*r)
    call array_Bn(kT*r,Bn_kTr)
    call array_Bn(kL*r,Bn_kLr)
    n_vec_x(1) = 0.5d0
    n_vec_x(2) = sqrt(3.d0)*0.5d0
    n_vec_y(1) = 0.d0
    n_vec_y(2) = 1.d0

    call kernel_Hstar(r_vec, n_vec_x, n_vec_y, r, h0_kTr, h1_kTr, h0_kLr, h1_kLr, Hstar)
    call kernel_Hstar_R(r_vec, n_vec_x, n_vec_y, r, Bn_kTr, Bn_kLr, Hstar_R)
    call kernel_Hstar_S(r_vec, n_vec_x, n_vec_y, r, Hstar_S)

    write(*,*) Hstar(1,1), Hstar_R(1,1)+Hstar_S(1,1)
    stop
  end subroutine test

  ! 入射波
  subroutine add_incwave_displacement(n, x, u)
    integer,intent(in) :: n ! 点の数
    real(8),intent(in) :: x(2,n)
    complex(8),intent(out) :: u(2*n)
    integer :: i
    complex(8) :: tmp

    do i=1,n
       tmp = exp(ione*wn*dot_product(pvec(:),x(:,i)))
       u(i  ) = u(i  ) + dvec(1)*tmp
       u(i+n) = u(i+n) + dvec(2)*tmp
    end do
  end subroutine add_incwave_displacement

  subroutine add_incwave_traction(n, x, n_vec, t)
    integer,intent(in) :: n ! 点の数
    real(8),intent(in) :: x(2,n)
    real(8),intent(in) :: n_vec(2,n)
    complex(8),intent(inout) :: t(2*n)
    integer :: i
    real(8) :: pd, pn, dn
    complex(8) :: tmp

    pd = dot_product(dvec,pvec)
    do i=1,n
       pn = dot_product(pvec(:),n_vec(:,i))
       dn = dot_product(dvec(:),n_vec(:,i))
       tmp = exp(ione*wn*dot_product(pvec(:),x(:,i)))
       t(i  ) = t(i  ) + ione*wn*(dl*pd*n_vec(1,i)+dm*(pn*dvec(1)+dn*pvec(1)))*tmp
       t(i+n) = t(i+n) + ione*wn*(dl*pd*n_vec(2,i)+dm*(pn*dvec(2)+dn*pvec(2)))*tmp
    end do
  end subroutine add_incwave_traction

  subroutine add_incwave_stress(n, x, sigma)
    integer,intent(in) :: n ! 点の数
    real(8),intent(in) :: x(2,n)
    complex(8),intent(out) :: sigma(3*n)
    integer :: i
    complex(8) :: tmp

    do i=1,n
       tmp = exp(ione*wn*dot_product(pvec(:),x(:,i)))
       sigma(i    ) = sigma(i    )+ione*wn*(dl*dot_product(dvec,pvec)+2.d0*dm*dvec(1)*pvec(1))*tmp
       sigma(i+n  ) = sigma(i+n  )+ione*wn*dm*(dvec(1)*pvec(2)+dvec(2)*pvec(1))*tmp
       sigma(i+n*2) = sigma(i+n*2)+ione*wn*(dl*dot_product(dvec,pvec)+2.d0*dm*dvec(2)*pvec(2))*tmp
    end do
  end subroutine add_incwave_stress
  
end module kernelfunc
