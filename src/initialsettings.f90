subroutine initialsettings
  use globals
  use io
  use omp_lib
  implicit none
  integer::i,j,k,itmp,gamma,ibril
  real(8)::rtmp,vtmp(2),theta360,thetarad
  real(8)::cr,ci
  character(len=32)::stepname


  read(*,*) numomp              !threadの数
  !-----------------configuration--------------------------------
  read(*,*) mflag               !!初期形状のmeshfileをレベルセット関数から生成(0:しない, 1:する)
  read(*,*) xe                  !x yousosu
  read(*,*) ye                  !y yousosu
  read(*,*) l1                  !|a_1|
  read(*,*) l2                  !|a_2|
  read(*,*) theta360            !a_1 to a_2 no nasu kaku
  read(*,*) symmetry            !0:assymmetry,1:1/4 symmetry,2:1/8 symmetry
  read(*,*) gamma               !0:gamma点なし, 1:gamma点のみ
  read(*,*) holerad             !ana(\gamma_q) no hankei
  !-----------------configuration--------------------------------
  ! -----------------SS-----------------------------------------
  read(*,*) islv                !1:hbmat only, 2:hmat_woodbury, 3:hamat
  read(*,*) nevmax              !ev no saidai kosu
  read(*,*) nskbn               !sekibun ten no kazu
  read(*,*) init_ncent               !sekibun en no kazu
  read(*,*) init_rad            !sekibun en no hankei no shokichi
  read(*,*) mrate               !sekibun en no margin
  read(*,*) nblck               !=L(ブロックの数)(SS)
  read(*,*) nhank               !=K(Hankel行列のサイズ)(SS)
  ! -----------------SS-----------------------------------------
  ! -----------------optimisation------------------------------------  
  read(*,*) step1               !start step
  read(*,*) step2               !end step
  read(*,*) idiv                !1youso no bunkatsusu
  read(*,*) mode(1)               !chiisaku shitai mode
  read(*,*) mode(2)               !okiku shitai mode
  ! -----------------optimisation------------------------------------
  ! -----------------update_lsf------------------------------------
  read(*,*) offset1
  read(*,*) offset2
  read(*,*) deltat1             !拡散方程式時間差分の幅
  read(*,*) deltat2             !拡散方程式時間差分の幅
  read(*,*) tloop1              !どのステップまでdeltat1で計算するか
  read(*,*) tloop2              !どのステップまでdeltat2で計算するか
  read(*,*) prm_c               !1d0で固定
  read(*,*) prm_tau             !複雑度係数
  ! -----------------update_lsf------------------------------------
  ! -----------------H-matrix-------------------------------------
  read(*,*) nmin                !ACAのn_min: clusterに含まれる要素数の最大値
  read(*,*) nminin              !ACAのn_min: 内点clusterに含まれる要素数の最大値
  read(*,*) eta                 !ACA のadmissible condition の eta(境界)
  read(*,*) eta_in              !ACA のadmissible condition の eta(内点)
  read(*,*) eps_aca             !eps_aca: ACAの許容誤差		   
  read(*,*) tol              !tol: H行列計算の許容誤差		   
  read(*,*) cflag               !ACA agglomeration: 0:しない 1:する 
  read(*,*) ceps                !agglomeration の許容誤差
  ! -----------------H-matrix-------------------------------------
  read(*,*) file_dir            !output先

  ! スレッド数を設定
  call omp_set_num_threads(numomp)
  allocate(krnl(0:numomp-1))

  ! kosi vector
  thetarad=pi*theta360/1.8d2          !rad ni henkan
  avec(1,1)=l1
  avec(2,1)=0.d0
  avec(1,2)=l2*cos(thetarad)
  avec(2,2)=l2*sin(thetarad)

  ! 逆格子ベクトル
  rtmp=-cos(thetarad)/(l1*l2)
  gvec(:,1)=avec(:,1)/(l1*l1)+avec(:,2)*rtmp
  gvec(:,2)=avec(:,1)*rtmp+avec(:,2)/(l2*l2)
  gvec=gvec*2.d0*pi/(sin(thetarad)*sin(thetarad))

  if(symmetry.eq.0)then      !assymmetry
     if(theta360.eq.60.d0)then !hexagonal lattice

        ! 第1brillouin zone の波数ベクトル
        nbril=3*nb*(nb+1)
        allocate(wv(2,nbril))
        vtmp(:)=(gvec(:,1)+gvec(:,2))*0.5d0/nb
        itmp=0
        do i=1,nb
           do j=0,nb-i
              itmp=itmp+1
              wv(:,itmp)=dble(i)/nb*gvec(:,2)*0.5d0+dble(j)*vtmp(:)
           end do
        end do
        do i=1,5                      !copy roop
           do j=1,nb*(nb+1)/2
              wv(1,itmp+j)=cos(i*thetarad)*wv(1,j)-sin(i*thetarad)*wv(2,j)
              wv(2,itmp+j)=sin(i*thetarad)*wv(1,j)+cos(i*thetarad)*wv(2,j)
           end do
           itmp=itmp+nb*(nb+1)/2
        end do
        ! 第1brillouin zone の波数ベクトル
     elseif(theta360.eq.90.d0)then !square lattice
        nbril=(2*nb+1)**2-1
        allocate(wv(2,nbril))
        itmp=0
        do i=1,2*nb+1
           do j=1,2*nb+1
              if(i.eq.nb+1.and.j.eq.nb+1) cycle
              itmp=itmp+1
              wv(1,itmp)=dble(j-nb-1)/nb*pi/l1
              wv(2,itmp)=dble(i-nb-1)/nb*pi/l2
           end do
        end do
     else
        stop "coming soon..."
     end if
  elseif(symmetry.eq.1)then  !1/4 symmetry
     if(theta360.eq.60.d0)then
        if(gamma.eq.0)then   !gamma ten nashi
           nbril=nb*3-1
           allocate(wv(2,nbril))
           rtmp=sqrt(gvec(1,1)*gvec(1,1)+gvec(2,1)*gvec(2,1))/sqrt(3.d0) !Gamma_K no nagasa
           do i=1,nb
              wv(:,i)=dble(nb-i+1)/nb*rtmp*avec(:,1)/l1 !K-Gamma
           end do
           do i=1,nb
              wv(:,nb+i)=dble(i)/nb*0.5d0*gvec(:,1) !Gamma-M
           end do
           do i=1,nb-1
              wv(:,2*nb+i)=gvec(:,1)+dble(i)/nb*rtmp*avec(:,2)/l2 !M-K
              wv(:,2*nb+i)=0.5d0*wv(:,2*nb+i)
           end do
        elseif(gamma.eq.1)then !gamma ten ari
           nbril=nb*3
           allocate(wv(2,nbril))
           rtmp=sqrt(gvec(1,1)*gvec(1,1)+gvec(2,1)*gvec(2,1))/sqrt(3.d0) !Gamma_K no nagasa
           do i=1,nb
              wv(:,i)=dble(nb-i+1)/nb*rtmp*avec(:,1)/l1 !K-Gamma
           end do
           do i=0,nb
              wv(:,nb+i+1)=dble(i)/nb*0.5d0*gvec(:,1) !Gamma-M
           end do
           do i=1,nb-1
              wv(:,2*nb+i+1)=gvec(:,1)+dble(i)/nb*rtmp*avec(:,2)/l2 !M-K
              wv(:,2*nb+i+1)=0.5d0*wv(:,2*nb+i+1)
           end do
        end if
     else
        stop "coming soon..."
     end if
  elseif(symmetry.eq.2)then  !1/8 symmetry
     if(theta360.eq.90.d0)then
        if(gamma.eq.0)then      !gamma ten nashi
           nbril=nb*3
           allocate(wv(2,nbril))
           do i=1,nb            !K-Gamma
              wv(1,i)=dble(nb-i+1)/nb*pi/l1-pi/l1/(2*nb)
              wv(2,i)=dble(nb-i+1)/nb*pi/l2-pi/l2/(2*nb)
           end do
           do i=1,nb            !Gamma-M
              wv(1,nb+i)=dble(i)/nb*pi/l1-pi/l1/(2*nb)
              wv(2,nb+i)=0.d0
           end do
           do i=1,nb         !M-K
              wv(1,2*nb+i)=pi/l1
              wv(2,2*nb+i)=dble(i)/nb*pi/l2-pi/l2/(2*nb)
           end do
        elseif(gamma.eq.1)then     !gamma ten ari
           nbril=nb*3
           allocate(wv(2,nbril))
           ! 縮約された第1brillouin zone の波数ベクトル
           do i=1,nb            !K-Gamma
              wv(1,i)=dble(nb-i+1)/nb*pi/l1
              wv(2,i)=dble(nb-i+1)/nb*pi/l2
           end do
           do i=0,nb            !Gamma-M
              wv(1,nb+i+1)=dble(i)/nb*pi/l1
              wv(2,nb+i+1)=0.d0
           end do
           do i=1,nb-1          !M-K
              wv(1,2*nb+i+1)=pi/l1
              wv(2,2*nb+i+1)=dble(i)/nb*pi/l2
           end do
           ! 縮約された第1brillouin zone の波数ベクトル
        endif
     else if(theta360.eq.60.d0)then
        if(gamma.eq.0)then   !gamma ten nashi
           nbril=nb*3
           allocate(wv(2,nbril))
           rtmp=sqrt(gvec(1,1)*gvec(1,1)+gvec(2,1)*gvec(2,1))/sqrt(3.d0) !Gamma_K no nagasa
           do i=1,nb
              wv(:,i)=dble(nb-i+1)/nb*rtmp*avec(:,1)/l1-rtmp*avec(:,1)/l1/(2*nb) !K-Gamma
           end do
           do i=1,nb
              wv(:,nb+i)=dble(i)/nb*0.5d0*gvec(:,1)-0.25d0*gvec(:,1)/nb !Gamma-M
           end do
           do i=1,nb
              wv(:,2*nb+i)=gvec(:,1)+dble(i)/nb*rtmp*avec(:,2)/l2-rtmp*avec(:,2)/l2/(2*nb) !M-K
              wv(:,2*nb+i)=0.5d0*wv(:,2*nb+i)
           end do
        elseif(gamma.eq.1)then !gamma ten ari
           nbril=nb*3
           allocate(wv(2,nbril))
           rtmp=sqrt(gvec(1,1)*gvec(1,1)+gvec(2,1)*gvec(2,1))/sqrt(3.d0) !Gamma_K no nagasa
           do i=1,nb
              wv(:,i)=dble(nb-i+1)/nb*rtmp*avec(:,1)/l1 !K-Gamma
           end do
           do i=0,nb
              wv(:,nb+i+1)=dble(i)/nb*0.5d0*gvec(:,1) !Gamma-M
           end do
           do i=1,nb-1
              wv(:,2*nb+i+1)=gvec(:,1)+dble(i)/nb*rtmp*avec(:,2)/l2 !M-K
              wv(:,2*nb+i+1)=0.5d0*wv(:,2*nb+i+1)
           end do
        end if
     else
        stop "coming soon..."
     end if
     !    stop "square only!"
     ! end if
  end if

#ifdef DETAIL
  do i=1,nbril
     write(222,*) wv(:,i)       !reduced first brillouin zone
  end do
#endif

  allocate(phase(2,nbril))
  do i=1,nbril
     phase(2,i)=exp(ione*dot_product(wv(:,i),avec(:,2)))
     phase(1,i)=exp(ione*dot_product(wv(:,i),avec(:,1)))
  end do

  nref=4*xe
  allocate(evv_ref(nref,2,nbril))
  allocate(xref(2,nref))
  call set_ref_point_on_pb(xref)

  nlsf=(xe+1)*(ye+1)
  ds=avec(1,1)*avec(2,2)/nlsf

  ! menseki=1.d0-pi*holerad**2

  allocate(ev_slc(2,nbril))
  if(step1 > 0)then             !途中のstepから再開する場合
     evslctr=1
     write(stepname,'(i4.4)') step1-1
     file_evslct=trim(trim(file_dir)//"/evslct/step"//trim(stepname)//".slc")
     file_ref=trim(trim(file_dir)//"/ref/step"//trim(stepname)//".ref")
     open(1,file=file_evslct)
     do ibril=1,nbril
        do j=1,2
           read(1,*) rtmp,cr,ci
           ev_slc(j,ibril)=cmplx(cr,ci,kind=8)
        end do
     end do
     close(1)
     write(*,*) "loaded",file_evslct
     open(1,file=file_ref)
     do ibril=1,nbril
        do k=1,2
           do i=1,4
              do j=1,nref/4
                 read(1,*) evv_ref(j+nref/4*(i-1),k,ibril)
              end do
           end do
        end do
     end do
     close(1)
     write(*,*) "loaded",file_ref
  end if

  return

contains
  subroutine set_ref_point_on_pb(a)
    integer::k,n
    real(8)::x,y,dx,dy
    real(8),intent(inout)::a(:,:)

    n=size(a)/2
    do i=1,4
       dx=avec(1,mod(i-1,2)+1)/n*4
       dy=avec(2,mod(i-1,2)+1)/n*4
       ! set initial position
       if(i.le.2)then
          x=-dx/2;y=-dy/2
       else
          x=avec(1,5-i)-dx/2;y=avec(2,5-i)-dy/2
       end if
       do j=1,n/4
          x=x+dx
          y=y+dy
          a(1,j+n/4*(i-1))=x
          a(2,j+n/4*(i-1))=y
       end do
    end do

#ifdef DETAIL
    do i=1,n
       write(999,*) a(:,i)
    end do
#endif
    
  end subroutine set_ref_point_on_pb

end subroutine initialsettings
