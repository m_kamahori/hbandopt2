program main
  use globals
  use io
  implicit none
  integer::i,iflag_bounded,itmp
  real(8),parameter::semicon=0.2d0,tsdmin=0.1d0
  integer::sflg=1                !flag of sensitivity. 1:shape 2:topological derivative
  real(8)::st,vt,tconv,tsd=0.d0,rtmp
  logical::flag

  call initialsettings

  allocate(hstry(0:step2))
  hstry(:)%flag=0
  file_hist=trim(trim(file_dir)//"/history.dat")
  open(11,file=file_hist)
  do i=0,step1-1
     read(11,*) itmp,hstry(i)%f,rtmp,tsd
     ! if(sflg.eq.1)then
     !    tsd
     ! else
     !    sdcnt=0
     ! end if
  end do
  do i=step1,step2
     vt=step2time(i)
     write(*,*)"# Step:",i
     write(*,*)"# Virtual time:",vt

     call convergence_test(flag,i,tconv) !収束判定
     if(flag.eqv..true.)exit

     tins=0.d0
     call checktime(tins,tint)
     st=tins

     call set_io_filename(i)

     if(i.eq.0) then
        if(mflag.eq.1)then
           call initiallsf         !初期形状のレベルセット関数を生成
           call lsf2elm(iflag_bounded)
        end if
     else
        call lsf2elm(iflag_bounded)
     end if
 
     call SSM(i)
     write(11,fmt_i1d3) i,hstry(i)%f,vt,tsd          !history of full bandgap width 

     call checktime(tins,tint)
     write(*,*)"# tsd/tsdmin   ",tsd/tsdmin
     write(*,*)"# tconv/semicon",tconv/semicon
     if(tsd >= tsdmin.and.tconv >= semicon)then
        write(*,*) "# Calculating topological derivatives"
        sflg=2
        call topological_derivative
        tsd=0.d0
     else
        write(*,*) "# Calculating shape derivatives"
        sflg=1
        call tan_comp
        call shape_derivative
        tsd=tsd+step2time(i+1)-vt
     end if
     deallocate(u,un,ut,xlsf,l2epre,e2lpre,nd4at,orgat,ltc2nd,l2e,e2l) 
     deallocate(p,nd,an,at,c,lngth)
     call checktime(tins,tint) 
 
     write(*,*)"# Lsf evolution" 
     if(mflag.eq.0)stop "@no lsf file" 
     call update_lsf(sflg,i) 
     call checktime(tins,tint) 
 
     call checktime(st,tint) 
     time(3)=tint              !1stepの時間 
     open(22,file="time.dat",access='append') 
     write(22,'(I4,4e24.16,2I3,I4)') ne,time(3),time(1),time(2),edges(1)/dble(sum(edges(2:5))),islv,i 
     close(22) 

  end do
  close(11)

  deallocate(ev_slc,evv_ref)
  stop

contains
  subroutine convergence_test(flag,step,dt)
    real(8),parameter::conv=5.d0
    logical,intent(out)::flag
    integer,intent(in)::step
    integer::s
    real(8),intent(out)::dt

    if(step > 0)then
       s=step_of_the_max_gap(step)
       dt=step2time(step)-step2time(s)
       if(dt >= conv)then
          write(*,*) "# Full bandgap width is Converged."
          flag=.true.
       else
          flag=.false.
       end if
    else
       dt=0.d0;flag=.false.
    end if
    
  end subroutine convergence_test

  integer function steps_from_the_max_gap(en)
    integer::loc(1)
    integer,intent(in)::en

    loc(:)=maxloc(hstry(0:en-1)%f)
    steps_from_the_max_gap=en-(loc(1)-1)

  end function steps_from_the_max_gap

  integer function step_of_the_max_gap(en)
    integer::loc(1)
    integer,intent(in)::en

    loc(:)=maxloc(hstry(0:en-1)%f)
    step_of_the_max_gap=loc(1)-1

  end function step_of_the_max_gap

  real(8) function step2time(ii)
    integer,intent(in)::ii

    if(ii < tloop2)then
       step2time=deltat1*ii
    else
       step2time=deltat1*tloop2+deltat2*(ii-tloop2)
    end if

  end function step2time
    

end program main
