subroutine load(step)
  use class_hbem
  use io
  implicit none
  integer,intent(in)::step
  integer::itmp,i,j

  !メッシュデータの読み込み
  open(2,file=file_mesh)
  read(2,*) np
  allocate(p(2,np))
  do i=1,np
     read(2,*)itmp,p(:,i)
  end do
  read(2,*) ne
  allocate(nd(2,np))
  allocate(ibc(ne))
  allocate(iperi(2,ne))
  edges=0
  do i=1,ne
     read(2,*)itmp,nd(:,i),ibc(i),iperi(:,i)
     if(ibc(i).eq.1) then
        edges(1)=edges(1)+1     !\Gamma_q
     else if(ibc(i).eq.10)then
        if(iperi(1,i).eq.-2) edges(2)=edges(2)+1   !\Gamma_{I_1}
        if(iperi(1,i).eq.-1) edges(3)=edges(3)+1   !\Gamma_{I_2}
        if(iperi(1,i).eq. 2) edges(4)=edges(4)+1   !\Gamma_{D_1}
        if(iperi(1,i).eq. 1) edges(5)=edges(5)+1   !\Gamma_{D_2}
     end if
  end do
  write(*,*) "edges(:)=",edges(:)
  close(2)

  if(allocated(u).eqv..false.)allocate(u(ne,2,nbril))
  if(allocated(un).eqv..false.)allocate(un(ne,2,nbril))
  if(allocated(ut).eqv..false.)allocate(ut(edges(1),2,nbril))

  allocate(ridt(25),cidt(25))
  ridt=0;cidt=0
  do i=1,25
     do j=1,mod(i-1,5)
        ridt(i)=ridt(i)+edges(j)
     end do
     do j=1,(i-1)/5
        cidt(i)=cidt(i)+edges(j)
     end do
     ! write(*,*) i,ridt(i),cidt(i)
  end do

#ifdef USE_AUTOSLCSLV
!!!!!!!!!!!!!!!!!!!!!!select solver!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if(dble(edges(1))/sum(edges(2:5)).lt.br)then
     if(ne.lt.3400)then
        islv=-1
     else
        islv=3
     end if
  else
     if(ne.lt.3400)then
        islv=0
     else
        islv=2
     end if
  end if
!!!!!!!!!!!!!!!!!!!!!!select solver!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif

!!!!!!!!!!!!!!!!!!!!!!prepare bem matrices!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  select case(islv)
  case(-1)
     write(*,*) "solver: direct_conv"
     allocate(smat(ne,edges(1)+1:ne,0:numomp-1))
     allocate(dmat(ne,ne,0:numomp-1))
     allocate(amat(ne,ne,0:numomp-1))
  case(0)
     write(*,*) "solver: woodbury_conv"
     allocate(smat(ne,edges(1)+1:ne,0:numomp-1))
     allocate(dmat(ne,ne,0:numomp-1))
     allocate(subc(sum(edges(2:5)),edges(1),0:numomp-1))
     allocate(cbinv(sum(edges(2:5)),edges(1),0:numomp-1))
     allocate(subb(edges(1),edges(1),0:numomp-1))
     allocate(ipiv_b(edges(1),0:numomp-1))
  case(2)
     write(*,*) "solver: woodbury_hmat"
     allocate(hbmat(0:numomp-1))
     allocate(hcmat(0:numomp-1))
     allocate(hcbinvmat(0:numomp-1))
     allocate(hdmat_i(4,0:numomp-1))
     allocate(hemat_i(16,0:numomp-1))
     allocate(hdmat_d(4,0:numomp-1))
     allocate(hemat_d(16,0:numomp-1))
  case(3)
     write(*,*) "solver: direct_hmat"
     allocate(hbmat(0:numomp-1))
     allocate(hcmat(0:numomp-1))
     allocate(hdmat_i(4,0:numomp-1))
     allocate(hemat_i(16,0:numomp-1))
     allocate(hdmat_d(4,0:numomp-1))
     allocate(hemat_d(16,0:numomp-1))
     ! allocate(smat(ne,ne,0:numomp-1))
     ! allocate(dmat(ne,ne,0:numomp-1))
     ! allocate(subb(edges(1),edges(1),0:numomp-1))
     ! allocate(subc(sum(edges(2:5)),edges(1),0:numomp-1))
  case default
     stop "islv must be between -1 and 3."
  end select
!!!!!!!!!!!!!!!!!!!!!!prepare bem matrices!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  open(2,file=file_ip2)
  read(2,*) nip
  allocate(grid(2,nip))
  allocate(ugrid(nip,2,nbril))
  allocate(dugrid(2,nip,2,nbril))
  do i=1,nip
     read(2,*) itmp,grid(:,i)
  end do
  close(2)

  ! if(step.eq.step1.and.step1.ne.0)then
  !    evvref

  allocate(c(2,ne))
  do i=1,ne
     c(:,i)=0.5d0*(p(:,nd(1,i))+p(:,nd(2,i)))
  end do

  allocate(an(2,ne))
  allocate(at(2,ne))
  allocate(lngth(ne))

  call normal_vectors

  open(1,file=file_bp)
  do i=1,ne
     do j=1,2
        write(1,fmt_d2) p(:,nd(j,i))
     end do
     write(1,*) 
     write(1,*) 
  end do
  close(1)


#ifdef DETAIL
  do i=edges(1)+1,sum(edges(1:2))
     write(701,*) c(:,i)
     write(701,*) c(:,iperi(2,i))
     write(701,*) 
  end do

  do i=sum(edges(1:2))+1,sum(edges(1:3))
     write(702,*) c(:,i)
     write(702,*) c(:,iperi(2,i))
     write(702,*)
  end do

  do i=sum(edges(1:3))+1,sum(edges(1:4))
     write(703,*) c(:,i)
     write(703,*) c(:,iperi(2,i))
     write(703,*)
  end do

  do i=sum(edges(1:4))+1,sum(edges)
     write(704,*) c(:,i)
     write(704,*) c(:,iperi(2,i))
     write(704,*)
  end do
#endif

  open(86,file=file_ip)
  do i=1,nip
     write(86,*) grid(:,i)
  end do
  close(86)


end subroutine load
