subroutine lattice_info(lsf,ltcl,ix,iy,i,j,ltc)
  use globals
  implicit none
  
  real(8),intent(in) :: lsf(0:xe,0:ye*2),ltcl
  integer,intent(in) :: ix,iy,i,j
  type(lattice),intent(out) :: ltc
  real(8) :: x1(2,2),x2(2,2),v1(2,2),v2(2,2),phis
  integer :: start,goal,start2,goal2,flag,outdir,outdir2,nnd
  real(8) :: v1tmp(2,2),v2tmp(2,2),x1tmp(2,2),x2tmp(2,2),phi(0:3),org(2)
  integer :: k,rot(2,2),nrot,idx(0:6)

  idx=(/ 0, 1, 2, 3, 0, 1, 2/)
  
  v1(:,:)=0.d0
  v2(:,:)=0.d0
  v1tmp(:,:)=0.d0
  v2tmp(:,:)=0.d0
  x1tmp(:,:)=0.d0
  x2tmp(:,:)=0.d0
  flag=0
  start=-1
  start2=-1
  goal=-1
  goal2=-1
  outdir=0
  outdir2=0
  nnd=0
  phi(0)=lsf(ix,iy)
  ! 格子内をHermite splineが通る場合は端点における速度ベクトルを計算
  ! 点(ix,iy)が左下にあると仮定して速度ベクトルを計算した後、点(ix,iy)が実際の場所
  ! に戻るように各ベクトルを回転するので、そのための情報を用意する。
  if ((ix.eq.i).and.(iy.eq.j)) then !点(ix,iy)は格子(i,j)の右上
     ! 反時計まわりに番号付け
     phi(1)=lsf(i-1,j)
     phi(2)=lsf(i-1,j-1)
     phi(3)=lsf(i,j-1)
     !格子左下から(ix,iy)へのベクトル
     org(1)=ltcl
     org(2)=ltcl
     ! 回転行列
     rot(1,1)=-1
     rot(1,2)=0
     rot(2,1)=0
     rot(2,2)=-1
     ! 回転角=nrot*pi/2
     nrot=2
  else if ((ix.eq.i).and.(iy.ne.j)) then !点(ix,iy)は格子(i,j)の右下
     phi(1)=lsf(i,j)
     phi(2)=lsf(i-1,j)
     phi(3)=lsf(i-1,j-1)
     org(1)=ltcl
     org(2)=0.d0
     rot(1,1)=0
     rot(1,2)=-1
     rot(2,1)=1
     rot(2,2)=0
     nrot=1
  else if ((ix.ne.i).and.(iy.eq.j)) then !点(ix,iy)は格子(i,j)の左上
     phi(1)=lsf(i-1,j-1)
     phi(2)=lsf(i,j-1)
     phi(3)=lsf(i,j)
     org(1)=0.d0
     org(2)=ltcl
     rot(1,1)=0
     rot(1,2)=1
     rot(2,1)=-1
     rot(2,2)=0
     nrot=3
  else if ((ix.ne.i).and.(iy.ne.j)) then !点(ix,iy)は格子(i,j)の左下
     phi(1)=lsf(i,j-1)
     phi(2)=lsf(i,j)
     phi(3)=lsf(i-1,j)
     org(1)=0.d0
     org(2)=0.d0
     rot(1,1)=1
     rot(1,2)=0
     rot(2,1)=0
     rot(2,2)=1
     nrot=0
  end if

  if((phi(0)*phi(1).lt.0.d0).and.(phi(0)*phi(3).lt.0.d0)) then
     flag=1
     if((phi(1)*phi(2).lt.0.d0).and.(phi(2)*phi(3).lt.0.d0)) then
        phis=phi(0)+phi(1)+phi(2)+phi(3)
        if(phi(0).lt.0.d0) then
           if(phis.gt.0.d0) then
              v1tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
              v2tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
              x1tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
              x1tmp(2,1)=0.d0
              x2tmp(1,1)=0.d0
              x2tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
              start=0
              goal=3
              nnd=2
           else if(phis.le.0.d0) then
              v1tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
              x1tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
              x1tmp(2,1)=0.d0
              x2tmp(1,1)=ltcl
              x2tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
              start=0
              goal =1

              v2tmp(2,2)=-phi(3)/(phi(0)-phi(3))**2.d0
              x1tmp(1,2)=ltcl*phi(3)/(phi(3)-phi(2))
              x1tmp(2,2)=ltcl
              x2tmp(1,2)=0.d0
              x2tmp(2,2)=ltcl*phi(0)/(phi(0)-phi(3))
              start2=2
              goal2=3
              nnd=4
              outdir=2
              outdir2=1
           end if
        else
           if(phis.gt.0.d0) then
              v2tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
              x1tmp(1,1)=ltcl
              x1tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
              x2tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
              x2tmp(2,1)=0.d0
              start=1
              goal =0

              v1tmp(2,2)=-phi(3)/(phi(0)-phi(3))**2.d0
              x1tmp(1,2)=0.d0
              x1tmp(2,2)=ltcl*phi(0)/(phi(0)-phi(3))
              x2tmp(1,2)=ltcl*phi(3)/(phi(3)-phi(2))
              x2tmp(2,2)=ltcl
              start2=3
              goal2=2
              nnd=4
              outdir=1
              outdir2=2
           else if(phis.le.0.d0) then
              v1tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
              v2tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
              x1tmp(1,1)=0.d0
              x1tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
              x2tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
              x2tmp(2,1)=0.d0
              start=3
              goal=0
              nnd=2
           end if
        end if
     else
        nnd=2
        if(phi(0).lt.0.d0) then
           v1tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
           v2tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
           x1tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
           x1tmp(2,1)=0.d0
           x2tmp(1,1)=0.d0
           x2tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
           start=0
           goal=3
        else
           v1tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
           v2tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
           x1tmp(1,1)=0.d0
           x1tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
           x2tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
           x2tmp(2,1)=0.d0
           start=3
           goal=0
        end if
     end if
  else if(phi(0)*phi(3).lt.0.d0) then
     flag=1
     nnd=2
     if(phi(0).lt.0.d0) then
        v2tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
        goal=3
        outdir=1
        x2tmp(1,1)=0.d0
        x2tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
        if(phi(2)*phi(3).lt.0.d0) then
           start=2
           x1tmp(1,1)=ltcl*phi(3)/(phi(3)-phi(2))
           x1tmp(2,1)=ltcl
        else if(phi(1)*phi(3).lt.0.d0) then
           start=1
           x1tmp(1,1)=ltcl
           x1tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
        end if
     else
        v1tmp(2,1)=-phi(3)/(phi(0)-phi(3))**2.d0
        start=3
        outdir=2
        x1tmp(1,1)=0.d0
        x1tmp(2,1)=ltcl*phi(0)/(phi(0)-phi(3))
        if(phi(2)*phi(3).lt.0.d0) then
           goal=2
           x2tmp(1,1)=ltcl*phi(3)/(phi(3)-phi(2))
           x2tmp(2,1)=ltcl
        else if(phi(2)*phi(1).lt.0.d0) then
           goal=1
           x2tmp(1,1)=ltcl
           x2tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
        end if
     end if
  else if(phi(0)*phi(1).lt.0.d0) then
     flag=1
     nnd=2
     if(phi(0).lt.0.d0) then
        v1tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
        start=0
        outdir=2
        x1tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
        x1tmp(2,1)=0.d0
        if(phi(2)*phi(1).lt.0.d0) then
           goal=1
           x2tmp(1,1)=ltcl
           x2tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
        else if(phi(2)*phi(3).lt.0.d0) then
           goal=2
           x2tmp(1,1)=ltcl*phi(3)/(phi(3)-phi(2))
           x2tmp(2,1)=ltcl
        end if
     else
        v2tmp(1,1)=-phi(1)/(phi(0)-phi(1))**2.d0
        goal=0
        outdir=1
        x2tmp(1,1)=ltcl*phi(0)/(phi(0)-phi(1))
        x2tmp(2,1)=0.d0
        if(phi(2)*phi(1).lt.0.d0) then
           start=1
           x1tmp(1,1)=ltcl
           x1tmp(2,1)=ltcl*phi(1)/(phi(1)-phi(2))
        else if(phi(2)*phi(3).lt.0.d0) then
           start=2 
           x1tmp(1,1)=ltcl*phi(3)/(phi(3)-phi(2))
           x1tmp(2,1)=ltcl
        end if
     end if
  end if
  ! 点(ix,iy)が実際の場所に戻るように各ベクトルを回転
  if(flag.eq.1) then
     if(nnd.eq.2) then
        start=idx(start+nrot)
        goal=idx(goal+nrot)

        v1(1,1)=rot(1,1)*v1tmp(1,1)+rot(1,2)*v1tmp(2,1)
        v1(2,1)=rot(2,1)*v1tmp(1,1)+rot(2,2)*v1tmp(2,1)
        v2(1,1)=rot(1,1)*v2tmp(1,1)+rot(1,2)*v2tmp(2,1)
        v2(2,1)=rot(2,1)*v2tmp(1,1)+rot(2,2)*v2tmp(2,1)

        x1(1,1)=rot(1,1)*x1tmp(1,1)+rot(1,2)*x1tmp(2,1)
        x1(2,1)=rot(2,1)*x1tmp(1,1)+rot(2,2)*x1tmp(2,1)
        x2(1,1)=rot(1,1)*x2tmp(1,1)+rot(1,2)*x2tmp(2,1)
        x2(2,1)=rot(2,1)*x2tmp(1,1)+rot(2,2)*x2tmp(2,1)

        x1(:,1)=x1(:,1)+org(:)
        x2(:,1)=x2(:,1)+org(:)
     else if(nnd.eq.4) then
        start=idx(start+nrot)
        goal=idx(goal+nrot)
        start2=idx(start2+nrot)
        goal2=idx(goal2+nrot)
        do k=1,2
           v1(1,k)=rot(1,1)*v1tmp(1,k)+rot(1,2)*v1tmp(2,k)
           v1(2,k)=rot(2,1)*v1tmp(1,k)+rot(2,2)*v1tmp(2,k)
           v2(1,k)=rot(1,1)*v2tmp(1,k)+rot(1,2)*v2tmp(2,k)
           v2(2,k)=rot(2,1)*v2tmp(1,k)+rot(2,2)*v2tmp(2,k)

           x1(1,k)=rot(1,1)*x1tmp(1,k)+rot(1,2)*x1tmp(2,k)
           x1(2,k)=rot(2,1)*x1tmp(1,k)+rot(2,2)*x1tmp(2,k)
           x2(1,k)=rot(1,1)*x2tmp(1,k)+rot(1,2)*x2tmp(2,k)
           x2(2,k)=rot(2,1)*x2tmp(1,k)+rot(2,2)*x2tmp(2,k)

           x1(:,k)=x1(:,k)+org(:)
           x2(:,k)=x2(:,k)+org(:)
        end do
     end if
  end if

  ltc%n=nnd
  ltc%x1(:,:)=x1(:,:)
  ltc%x2(:,:)=x2(:,:)
  ltc%v1(:,:)=v1(:,:)
  ltc%v2(:,:)=v2(:,:)
  ltc%start(1)=start
  ltc%goal(1) =goal
  ltc%start(2)=start2
  ltc%goal(2) =goal2
  ltc%flag =flag
  ltc%outdir(1)=outdir
  ltc%outdir(2)=outdir2
  
end subroutine lattice_info

