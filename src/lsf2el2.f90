subroutine lsf2el2
  use globals
  use io
  implicit none

  ! real(8)::xlength
  ! real(8)::l2
  ! real(8)::minx,miny,x,y
  ! integer::l1,ye
  ! integer::iflag_bounded

  ! real(8),parameter::eps1=3.0d-1
  real(8),parameter::eps1=1.0d-8
  real(8),parameter::eps2=1.0d-6
  real(8),parameter::delta=1.d-7
  ! integer::idiv

  integer ix,iy,i,j,k,l
  integer::itmp
  ! real(8),allocatable::lsfunc(:,:)
  integer,allocatable::element(:,:)
  real(8),allocatable::nodepos(:,:)
  real(8),allocatable::refvec(:,:)
  !refvec:「voxelの中心から外側に向かう」or
  !       「レベルセット関数の値が最大の節点からvoxelの中心に向かう」ベクトル(cnt_nn=2のとき)
  !refvec:voxelの中心から外側に向かうベクトル (cnt_nn=4のとき)
  !refvec:固定設計領域から外側に向かうベクトル (cnt_z>0のとき)
  real(8) lsf(0:3)
  real(8) au,av,f1,f2,ww
  integer cnt_z,cnt_p,cnt_m
  integer nnode,nelement,cnt_max,cnt_nn
  real(8) lsfs,lsftmp
  real(8) xi,yi,xj,yj
  real(8) anorm
  integer imaxlsf,iminlsf
  real(8) dmaxlsf,dminlsf
  real(8),allocatable::elml(:) !elml(i): 要素iの長さ
  ! real(8),allocatable::an(:,:),at(:,:)
  integer::nlsf,intmp
  ! character(len=32)::stepname1
  ! character(len=32)::file_el2,file_lsf,file_ip
  real(8),allocatable::xlsf(:,:),xip(:,:)

  integer::ncross
  logical::lsfflg

  itmp=0
  nlsf=(xe+1)*(ye+1)            !setten no kazu
  allocate(lsfunc(0:xe,0:ye))
  allocate(xlsf(2,nlsf))
  allocate(xip(2,nlsf))
200 continue
!!!!!!!!!!load the level set function!!!!!!!!!!!!!!!

  open(100,file=file_lsf)
  intmp=0
  nip=0
  do iy=0,ye
     do ix=0,xe
        intmp=intmp+1           !固定設計領域の点の番号
        read(100,*) xlsf(:,intmp),lsfunc(ix,iy)
        ! lsfunc(ix,iy)=lsfunce(ix,iy)+offset
        if(abs(lsfunc(ix,iy))<eps2)lsfunc(ix,iy)=0.0d0
        !↑要検討↑ これがないとwwのあたりでbugるらしいが...ホンマか?
        if(ix.eq.0.or.ix.eq.xe.or.iy.eq.0.or.iy.eq.ye)then
           if(lsfunc(ix,iy).le.0.d0)write(82,*) ix,iy
        else
           if(lsfunc(ix,iy).le.0.d0)then
              write(82,*) ix,iy
           else                        !物体領域
              nip=nip+1                !内点の数
              xip(:,nip)=xlsf(:,intmp) !内点
           end if
        end if
        ! if(lsfunc(ix,iy).le.0.d0.or.ix.eq.0.or.ix.eq.xe.or.iy.eq.0.or.iy.eq.ye) then !空孔と境界
        !    write(22,*) ix,iy
        ! else                        !物体領域
        !    nip=nip+1                !内点の数
        !    xip(:,nip)=xlsf(:,intmp) !内点
        ! end if
     end do
  end do
  close(100)
!!!!!!!!!!load the level set function!!!!!!!!!!!!!!!

!!!!!!!!!!count nodes on periodic boundary!!!!!!!!!!
  ncross=0
  do ix=0,xe-1
     lsftmp=lsfunc(ix,0)
     if(lsfunc(ix+1,0)*lsftmp.lt.0.d0) ncross=ncross+1 !lsfの符号が変わっていたら
  end do
  if(ncross.eq.0.and.lsfunc(0,0).lt.0.d0) then !周期境界のlsfが全て負だったら
     ncross=-1
     stop "No periodic boundary."
  end if
!!!!!!!!!!count nodes on periodic boundary!!!!!!!!!!

  if(ncross.gt.0)then
     lsfflg = check_lsf(xe,lsfunc(:,:)) !check the PEC connectivity
     if(lsfflg.eqv..false.)then          !if the dielectric material is torn
        itmp=itmp+1
        lsfunc(:,:)=lsfunc(:,:)+deltalsf
        do iy=0,ye
           do ix=0,xe
              !制約条件
              if(lsfunc(ix,iy).gt. 1.d0) lsfunc(ix,iy)=1.d0
              if(lsfunc(ix,iy).lt.-1.d0) lsfunc(ix,iy)=-1.d0
           end do
        end do
        open(100,file=file_lsf,status='replace')
        intmp=0
        do iy=0,ye
           do ix=0,xe
              intmp=intmp+1
              write(100,*) xlsf(:,intmp),lsfunc(ix,iy)
           end do
        end do
        close(100)
        goto 200
     end if
     write(*,*) "# Increased lsf by",deltalsf*itmp
  end if

  ! menseki=nip/dble(nlsf)   !物体領域の割合
  menseki=nip/dble(nlsf)*avec(1,1)*avec(2,2)   !物体領域の面積
  ! write(*,*) "menseki=",menseki

  open(100,file=file_ip2)
  write(100,*) nip
  do i=1,nip
     ! grid(:,i)=xip(:,i)
     write(100,*) i,xip(:,i)! grid(:,i)
  end do
  close(100)

  deallocate(xip,xlsf)
  allocate(nodepos(2,xe*ye*4),element(2,xe*ye*2),refvec(2,xe*ye*2))

  nodepos(:,:)=0.d0
  element(:,:)=0

  nnode=0
  nelement=0
  cnt_max=0
  do iy=0,ye-1    !voxelの左下の節点のloop
     do ix=0,xe-1 !voxelの左下の節点のloop
        cnt_nn=0
        lsf(0)=lsfunc(ix,  iy  ) !voxelの左下の節点のlevel set関数
        lsf(1)=lsfunc(ix+1,iy  ) !voxelの右下の節点のlevel set関数
        lsf(2)=lsfunc(ix+1,iy+1) !voxelの右上の節点のlevel set関数
        lsf(3)=lsfunc(ix,  iy+1) !voxelの左上の節点のlevel set関数

        cnt_z=0 !voxel内でレベルセット関数がゼロの節点の数
        cnt_p=0 !voxel内でレベルセット関数が正の節点の数
        cnt_m=0 !voxel内でレベルセット関数が負の節点の数
        do i=0,3
           ! if(dabs(lsf(i)).le.0.d0) then !if(lsf(i).eq.0.d0)thenではないか
           if(abs(lsf(i)).eq.0.d0) then
              cnt_z=cnt_z+1
           elseif(lsf(i).gt.0.d0) then
              ! elseif(lsf(i).ge.0.d0) then !.ge.ではなく.gt.ではないか
              cnt_p=cnt_p+1
           else
              cnt_m=cnt_m+1
           end if
        end do

        if((cnt_z.eq.1.and.cnt_m.eq.3))cycle !当該voxelの中に要素は無い
        if(cnt_m.eq.4) cycle !これも上と同じような...?

        do i=0,3 !voxel内の節点loop
           if(i==0.or.i==3) au=dble(ix)
           if(i==1.or.i==2) au=dble(ix+1)
           if(i==0.or.i==1) av=dble(iy)
           if(i==2.or.i==3) av=dble(iy+1) !(au,av)が考えている節点の座標となる

           j=mod(i+1,4) !一つ先の(local)節点番号

           f1=lsf(i)
           f2=lsf(j)

           if (((f1>=0.d0).and.(f2<0.d0)).or.((f1<0.d0).and.(f2>=0.d0))) then ! f2.eq.0.and.f1>0.d0の場合は?
              ! if (((f1>=0.d0).and.(f2<0.d0)).or.((f1<0.d0).and.(f2>=0.d0))) then ! 辺の両端の符号が違ったら
              ww=abs(lsf(i)/(lsf(i)-lsf(j))) ! 節点iからの距離 lsf(i):x=(lsf(i)-lsf(j)):1
              ! if( dabs( lsf(i) )<eps1 ) ww=0.0d0 必要か？
              ! 節点座標計算
              if( i==0 ) au=au+ww
              if( i==1 ) av=av+ww
              if( i==2 ) au=au-ww
              if( i==3 ) av=av-ww
              ! 節点カウント
              cnt_nn=cnt_nn+1 !voxel内の境界要素の節点の数
              nnode=nnode+1
              nodepos(1,nnode)=au
              nodepos(2,nnode)=av
           end if
        end do

        ! 要素追加
        if(cnt_nn.eq.2)then
           nelement=nelement+1
           element(1,nelement)=nnode-1
           element(2,nelement)=nnode
           imaxlsf=0; dmaxlsf=lsf(0)
           iminlsf=0; dminlsf=lsf(0)
           do i=1,3
              if(lsf(i).gt.dmaxlsf) then
                 imaxlsf=i
                 dmaxlsf=lsf(i)
              end if
              if(lsf(i).lt.dminlsf) then
                 iminlsf=i
                 dminlsf=lsf(i)
              end if
           end do
           if(cnt_m.eq.1)then
              refvec(1,nelement)=dble(min(mod(iminlsf,3),1))-0.5d0
              refvec(2,nelement)=dble(iminlsf/2)-0.5d0
           elseif(cnt_p.eq.1)then
              refvec(1,nelement)=0.5d0-dble(min(mod(imaxlsf,3),1))
              refvec(2,nelement)=0.5d0-dble(imaxlsf/2)
           elseif(cnt_p.eq.2.and.cnt_m.eq.2)then
              refvec(1,nelement)=dble(min(mod(iminlsf,3),1))-0.5d0
              refvec(2,nelement)=dble(iminlsf/2)-0.5d0
           else
              write(*,*) ix,iy,lsf(:)
              write(*,*) cnt_p,cnt_m,cnt_z
              stop
           end if
        else if( cnt_nn.eq.4 )then
           lsfs=lsf(0)+lsf(1)+lsf(2)+lsf(3)
           if(lsfs.ge.0.0d0.and.lsf(0).ge.0.0d0)then
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode-2
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=-1.d0
              nelement=nelement+1
              element(1,nelement)=nnode-1
              element(2,nelement)=nnode
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=1.d0
           elseif(lsfs.le.0.d0.and.lsf(0).le.0.d0)then
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode-2
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=1.d0
              nelement=nelement+1
              element(1,nelement)=nnode-1
              element(2,nelement)=nnode
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=-1.d0
           elseif(lsfs.ge.0.d0.and.lsf(0).le.0.d0)then
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=-1.d0
              nelement=nelement+1
              element(1,nelement)=nnode-2
              element(2,nelement)=nnode-1
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=1.d0
           elseif(lsfs.le.0.d0.and.lsf(0).ge.0.d0)then
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=1.d0
              nelement=nelement+1
              element(1,nelement)=nnode-2
              element(2,nelement)=nnode-1
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=-1.d0
           end if
        end if
     end do
  end do

  open(99,file=file_log)
  write(99,*) "# nnode (before integration)=",nnode !統合前の節点数
  write(99,*) "# nelement (before integration)",nelement !統合前の要素数
  close(99)

  ! 重複する節点の統合 
  do i=1,nnode-1
     xi=nodepos(1,i)
     yi=nodepos(2,i)
     do j=i+1,nnode
        xj=nodepos(1,j)
        yj=nodepos(2,j)
        if(abs(xi-xj)<eps1.and.abs(yi-yj)<eps1) then ! iとjが一致したら
           do k=j,nnode-1
              nodepos(:,k)=nodepos(:,k+1) ! j+1 以降の点も一つずつ前にずらす
           end do
           do k=1,nelement
              do l=1,2
                 if(element(l,k).eq.j) then
                    element(l,k)=i ! 要素の節点jをiに置換し
                 else if(element(l,k).gt.j) then
                    element(l,k)=element(l,k)-1 ! j+1以降の点を一つずつ前にずらす
                 end if
              end do
           end do
           nnode=nnode-1 ! 節点が一つ減った
        end if
     end do
  end do

#ifdef DETAIL
  do i=1,nelement !境界要素の絵を描く
     do j=1,2
        write(88,*) nodepos(:,element(j,i))
     end do
     write(88,*) 
  end do
#endif

  allocate(an(2,nelement),at(2,nelement))
  ! 法線と接線を作って
  do i=1,nelement
     anorm=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))**2+(nodepos(2,element(2,i))-nodepos(2,element(1,i)))**2 !i番要素長
     anorm=1d0/sqrt(anorm)
     at(1,i)=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))*anorm
     at(2,i)=(nodepos(2,element(2,i))-nodepos(2,element(1,i)))*anorm
     an(1,i)= at(2,i)
     an(2,i)=-at(1,i)
     if(an(1,i)*refvec(1,i)+an(2,i)*refvec(2,i).le.0.d0)then ! 内積をとって向きが揃っていなければ
        itmp=element(1,i) !要素番号を入れ替え
        element(1,i)=element(2,i)
        element(2,i)=itmp
        an(1,i)=-an(1,i)  !法線をひっくり返す
        an(2,i)=-an(2,i)
        at(1,i)=-at(1,i)  !接線もひっくり返す
        at(2,i)=-at(2,i)  
     end if
  enddo

  ! 要素チェック
  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do
  open(99,file=file_log)
  write(99,*) "# Original mesh"
  write(99,*) '## nnode=',nnode !統合後の節点数
  write(99,*) '## nelement=',nelement !統合後の要素数
  write(99,*) "## (Longest edge)=",maxval(elml)
  write(99,*) "## (Shortest edge)=",minval(elml)
  write(99,*) "## (Longest edge)/(Shortest edge)=",maxval(elml)/minval(elml)
  close(99)
  deallocate(elml)

#ifdef DETAIL
  do i=1,nelement !要素改善後の境界要素の絵を描く
     do j=1,2
        write(83,*) nodepos(:,element(j,i))
     end do
     write(83,*) 
     write(84,*) (nodepos(:,element(1,i))+nodepos(:,element(2,i)))*0.5d0
     write(84,*) (nodepos(:,element(1,i))+nodepos(:,element(2,i)))*0.5d0+an(:,i)
     write(84,*)
     write(84,*)
  end do
#endif

  deallocate(an)

  ! 要素改善
  call improve_mesh(nnode,nelement,nodepos,element,eps1,ncross)

  ! deallocate(lsfunc)
  deallocate(element,nodepos,refvec)

contains
  logical function check_lsf(size,lsf)
    integer,intent(in)::size
    integer::i,j,itmp,cnt,p(2),npec
    integer,allocatable::st(:)
    integer::d(8,size*size/8),x(2,8,size*size/8),map(-1:size+1,-1:size+1),m(-1:size+1,-1:size+1)
    real(8),intent(in)::lsf(0:size,0:size)
    logical,allocatable::flg(:)

    ! make a dielectric material map
    map=1
    do i=size/2,size
       do j=size/2,i
          if(lsf(i,j).gt.0.d0)then
             map(i,j)=1         !dielectric
          else
             map(i,j)=0         !PEC
#ifdef DETAIL
             write(40,*) i,j
#endif
          end if
       end do
    end do

    ! determine the number of periodic PEC boundary
    i=size
    npec=0
    do j=size,size/2,-1
       if(j.eq.size)then
          if(map(i,j).eq.0)npec=npec+1
       else
          if(map(i,j).eq.0.and.map(i,j+1).eq.1)npec=npec+1
       end if
    end do

    ! determine initial position
    allocate(st(npec))
    i=size
    itmp=0
    do j=size,size/2,-1
       if(j.eq.size)then
          if(map(i,j).eq.0)then
             itmp=itmp+1
             st(itmp)=j
          end if
       else
          if(map(i,j).eq.0.and.map(i,j+1).eq.1)then
             itmp=itmp+1
             st(itmp)=j
          end if
       end if
    end do

    allocate(flg(npec))
    flg(:)=.false.
    do k=1,npec
!!!!!!check the PEC connectivity between diagonal line and periodic line!!!!!

       ! set initial position
       p(1)=size
       p(2)=st(k)

       ! set position of points around p
       x(:,1,1)=(/p(1)-1,p(2)+1/)
       x(:,2,1)=(/p(1)-1,p(2)  /)
       x(:,3,1)=(/p(1)-1,p(2)-1/)
       x(:,4,1)=(/p(1)  ,p(2)-1/)
       x(:,5,1)=(/p(1)+1,p(2)-1/)
       x(:,6,1)=(/p(1)+1,p(2)  /)
       x(:,7,1)=(/p(1)+1,p(2)+1/)
       x(:,8,1)=(/p(1)  ,p(2)+1/)

       ! initialize material map
       m=map

       ! set material flags around p
       d = 1
       d(1,1)=m(x(1,1,1),x(2,1,1))
       d(2,1)=m(x(1,2,1),x(2,2,1))
       d(3,1)=m(x(1,3,1),x(2,3,1))
       d(4,1)=m(x(1,4,1),x(2,4,1))
       d(8,1)=m(x(1,8,1),x(2,8,1))

       itmp=1;cnt=0
       do while(p(1).ne.p(2))   !iterate until p arrives at diagonal line
          do i=1,8
             if(d(i,itmp).eq.0)then
                d(i,itmp)=1        !change flag
                p(:)=x(:,i,itmp)   !move to next position
                itmp=itmp+1
                ! set position of points around p
                x(:,1,itmp)=(/p(1)-1,p(2)+1/)
                x(:,2,itmp)=(/p(1)-1,p(2)  /)
                x(:,3,itmp)=(/p(1)-1,p(2)-1/)
                x(:,4,itmp)=(/p(1)  ,p(2)-1/)
                x(:,5,itmp)=(/p(1)+1,p(2)-1/)
                x(:,6,itmp)=(/p(1)+1,p(2)  /)
                x(:,7,itmp)=(/p(1)+1,p(2)+1/)
                x(:,8,itmp)=(/p(1)  ,p(2)+1/)
                do j=1,8
                   d(j,itmp)=m(x(1,j,itmp),x(2,j,itmp)) !candidate diretion
                   m(x(1,j,itmp),x(2,j,itmp))=1 !can't pass the same route
                end do
                exit
             end if
          end do
#ifdef DETAIL
          write(40+k,*) p(:)
#endif
          if(sum(d(:,itmp)).eq.8)then
             if(itmp.eq.1)then
                flg(k)=.true.   !dielectric domain is not torn
                exit
             else
                itmp=itmp-1
             end if
          end if

          cnt=cnt+1
          if(cnt.gt.size**3)stop "@check_lsf:infinite iteration may occurs"
       end do
!!!!!!check the PEC connectivity between diagonal line and periodic line!!!!!!!!

#ifdef DETAIL
       write(40+k,*)
#endif
       if(flg(k).eqv..true.)cycle
!!!!!!check the PEC connectivity between horiziontal line and periodic line!!!!!

       ! set initial position
       p(1)=size
       p(2)=st(k)

       ! set position of points around p
       x(:,5,1)=(/p(1)-1,p(2)+1/)
       x(:,4,1)=(/p(1)-1,p(2)  /)
       x(:,3,1)=(/p(1)-1,p(2)-1/)
       x(:,2,1)=(/p(1)  ,p(2)-1/)
       x(:,1,1)=(/p(1)+1,p(2)-1/)
       x(:,8,1)=(/p(1)+1,p(2)  /)
       x(:,7,1)=(/p(1)+1,p(2)+1/)
       x(:,6,1)=(/p(1)  ,p(2)+1/)

       ! initialize material map
       m=map

       ! set material flags around p
       d = 1
       d(5,1)=m(x(1,5,1),x(2,5,1))
       d(4,1)=m(x(1,4,1),x(2,4,1))
       d(3,1)=m(x(1,3,1),x(2,3,1))
       d(2,1)=m(x(1,2,1),x(2,2,1))
       d(6,1)=m(x(1,6,1),x(2,6,1))

       itmp=1;cnt=0
       do while(p(2).ne.size/2) !iterate until p arrives at horizontal line
          do i=1,8
             if(d(i,itmp).eq.0)then
                d(i,itmp)=1        !change flag
                p(:)=x(:,i,itmp)   !move to next position
                itmp=itmp+1
                ! 
                x(:,5,itmp)=(/p(1)-1,p(2)+1/)
                x(:,4,itmp)=(/p(1)-1,p(2)  /)
                x(:,3,itmp)=(/p(1)-1,p(2)-1/)
                x(:,2,itmp)=(/p(1)  ,p(2)-1/)
                x(:,1,itmp)=(/p(1)+1,p(2)-1/)
                x(:,8,itmp)=(/p(1)+1,p(2)  /)
                x(:,7,itmp)=(/p(1)+1,p(2)+1/)
                x(:,6,itmp)=(/p(1)  ,p(2)+1/)
                do j=1,8
                   d(j,itmp)=m(x(1,j,itmp),x(2,j,itmp)) !candidate direction
                   m(x(1,j,itmp),x(2,j,itmp))=1 !can't pass the same route
                end do
                exit
             end if
          end do
#ifdef DETAIL
          write(40+k,*) p(:)
#endif
          if(sum(d(:,itmp)).eq.8)then
             if(itmp.eq.1)then
                flg(k)=.true.   !dielectric domain is not torn
                exit
             else
                itmp=itmp-1
             end if
          end if

          cnt=cnt+1
          if(cnt.gt.size**3)stop "@check_lsf:infinite iteration may occurs"
       end do
!!!!!!check the PEC connectivity between horiziontal line and periodic line!!!!!
#ifdef DETAIL
       write(40+k,*)
#endif
    end do !k

    check_lsf=.true.           !dielectric domain is not torn
    do k=1,npec
       if(flg(k).eqv..false.)check_lsf=.false. !dielectric domain is torn
    end do

  end function check_lsf

end subroutine lsf2el2

subroutine improve_mesh(nnode,nelement,nodepos,element,eps1,ncross)
  use io
  use globals
  implicit none

  ! integer,intent(in)::xe,ye
  ! real(8),intent(in)::l1,l2
  integer,intent(in)::ncross

  real(8),parameter::preset_shortestedge=0.5d0
  real(8),parameter::dFerguson=0.5d0
  real(8),parameter::eps=1.d-1
  integer nnode,nelement
  integer element(2,nelement)
  real(8) nodepos(2,nnode)
  real(8) eps1
  real(8),allocatable::elml(:) !elml(i): 要素iの長さ
  integer,allocatable::elm(:,:) !elm(i,j): j番節点のi番要素
  real(8),allocatable::atnd(:,:) !atnd(i,j): j番節点における接線のx_i成分

  integer::i,j,k,l,k1,k2,itmp

  real(8) xcnt(2)

  real(8) au
  real(8) f1,f2,f3,f4
  real(8) anorm
  real(8) xi,xj,yi,yj

  integer,allocatable::ndiv(:)
  real(8) shortestedge,longestedge

  real(8),allocatable::nodeposnew(:,:)
  integer,allocatable::elementnew(:,:)

  real(8),allocatable::at2(:,:)

  itmp=0
  !短い辺を削除
103 continue
  itmp=itmp+1

  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do

  ! do i=1,nelement
  !    write(*,*) nodepos(:,element(1,i)),element(1,i)
  !    write(*,*) nodepos(:,element(2,i)),element(2,i)
  !    write(*,*)
  ! end do

  allocate(elm(2,nnode)) !elm(i,j): j番節点のi番要素を返す配列を作る
  elm=0
  do i=1,nnode
     do j=1,nelement
        if(i.eq.element(1,j))then
           ! write(*,*) i,j
           elm(1,i)=j
           goto 100
        elseif(i.eq.element(2,j))then
           elm(2,i)=j
           goto 100
        end if
100     continue
     end do
  end do

  if(symmetry.ge.1)then
     allocate(atnd(2,nnode)) !atnd(i,j): j番節点における接線のx_i成分
     do i=1,nnode
        if(elm(1,i).eq.0)then
           if((abs(nodepos(2,i)).le.eps1).or.&
                (abs(nodepos(2,i)-dble(ye)).le.eps1))then
              atnd(1,i)=0.d0
              atnd(2,i)=at(2,elm(2,i))
           else if((abs(nodepos(1,i)).le.eps1).or.&
                (abs(nodepos(1,i)-dble(xe)).le.eps1))then
              atnd(1,i)=at(1,elm(2,i))
              atnd(2,i)=0.d0
           end if
        else if(elm(2,i).eq.0)then
           if((abs(nodepos(2,i)).le.eps1).or.&
                (abs(nodepos(2,i)-dble(ye)).le.eps1))then
              atnd(1,i)=0.d0
              atnd(2,i)=at(2,elm(1,i))
           else if((abs(nodepos(1,i)).le.eps1).or.&
                (abs(nodepos(1,i)-dble(xe)).le.eps1))then
              atnd(1,i)=at(1,elm(1,i))
              atnd(2,i)=0.d0
           end if
        else
           atnd(:,i)=elml(elm(1,i))*at(:,elm(1,i))&
                +elml(elm(2,i))*at(:,elm(2,i))
        end if
        anorm=1.d0/sqrt(dot_product(atnd(:,i),atnd(:,i)))
        atnd(:,i)=atnd(:,i)*anorm
     end do
  else
     stop "Sorry, lsf2el2 cannot deal with assymmetric unit cells now."
  end if

#ifdef DETAIL
  do i=1,nnode !接線チェック
     ! gnuplot でplot "fort.102" w lp, "fort.88"w lpでチェックできる
     write(102,*) nodepos(:,i)
     write(102,*) nodepos(:,i)+atnd(:,i)
     write(102,*)
     write(102,*)
  end do
#endif

  au=0.5d0
  f1=2.d0*au**3-3.d0*au**2+1.d0
  f2=-2.d0*au**3+3.d0*au**2
  f3=au**3-2.d0*au**2+au
  f4=au**3-au**2
  do i=1,nelement
     if(elml(i).le.preset_shortestedge)then
        if(abs(nodepos(1,element(1,i))).le.eps1.or.&
             abs(nodepos(2,element(1,i))).le.eps1.or.&
             abs(nodepos(1,element(1,i))-dble(xe)).le.eps1.or.&
             abs(nodepos(2,element(1,i))-dble(ye)).le.eps1)then
           nodepos(:,element(2,i))=nodepos(:,element(1,i))
        elseif(abs(nodepos(1,element(2,i))).le.eps1.or.&
             abs(nodepos(2,element(2,i))).le.eps1.or.&
             abs(nodepos(1,element(2,i))-dble(xe)).le.eps1.or.&
             abs(nodepos(2,element(2,i))-dble(ye)).le.eps1)then
           nodepos(:,element(1,i))=nodepos(:,element(2,i))
        else
           !Ferguson曲線を作成
           xcnt(:)=nodepos(:,element(1,i))*f1&
                +nodepos(:,element(2,i))*f2&
                +atnd(:,element(1,i))*f3&
                +atnd(:,element(2,i))*f4
           nodepos(:,element(1,i))=xcnt(:) !要素iの両端の点をferguson曲線の中点に移動させる。
           nodepos(:,element(2,i))=xcnt(:) !要素iの両端の点をferguson曲線の中点に移動させる。
        end if
     end if
  end do


  ! 重複する節点の統合 ↑で2点を1点にmergeしたので、重複が現れる。これを取り除く。
  do i=1,nnode-1
     xi=nodepos(1,i)
     yi=nodepos(2,i)
     do j=i+1,nnode
        xj=nodepos(1,j)
        yj=nodepos(2,j)
        if(abs(xi-xj)<eps1.and.abs(yi-yj)<eps1) then ! iとjが一致したら
           do k=j,nnode-1
              nodepos(:,k)=nodepos(:,k+1) ! j+1 以降の点も一つずつ前にずらす
           end do
           do k=1,nelement
              do l=1,2
                 if(element(l,k).eq.j) then
                    element(l,k)=i ! 要素の節点jをiに置換し
                 else if(element(l,k).gt.j) then
                    element(l,k)=element(l,k)-1 ! j+1以降の点を一つずつ前にずらす
                 end if
              end do
           end do
           nnode=nnode-1 ! 節点が一つ減った
        end if
     end do
  end do
  ! do j=1,nelement
  !    write(44,*) element(:,j)
  ! end do

  ! 同じ点からなる要素を削除
101 continue
  do i=1,nelement
     if(element(1,i).eq.element(2,i)) then
        do j=i,nelement-1
           element(:,j)=element(:,j+1)
        end do
        nelement=nelement-1
        goto 101
     end if
  end do

  ! ! 直線を削除
112 continue
  do i=1,nelement
     do j=i+1,nelement
        if((element(1,i).eq.element(2,j)).and.(element(2,i).eq.element(1,j))) then

           k1=min(element(1,j),element(2,j))
           k2=max(element(1,j),element(2,j))
           do k=k1+1,nnode
              if((k.gt.k1).and.(k.lt.k2))then
                 nodepos(:,k-1)=nodepos(:,k)
              else if(k.gt.k2) then
                 nodepos(:,k-2)=nodepos(:,k)
              end if
           end do
           do k=1,nelement
              do l=1,2
                 if((element(l,k).gt.k1).and.(element(l,k).lt.k2)) then
                    element(l,k)=element(l,k)-1
                 else if(element(l,k).gt.k2) then
                    element(l,k)=element(l,k)-2
                 end if
              end do
           end do
           nnode=nnode-2

           do k=i+1,nelement
              if((k.gt.i).and.(k.lt.j)) then
                 element(:,k-1)=element(:,k)
              else if(k.gt.j) then
                 element(:,k-2)=element(:,k)
              end if
           end do
           nelement=nelement-2
           goto 112
        end if
     end do
  end do

  ! do i=1,nelement !要素改善後の境界要素の絵を描く
  !    do j=1,2
  !       write(83,*) nodepos(:,element(j,i))
  !    end do
  !    write(83,*) 
  ! end do

  allocate(at2(2,nelement))
  do i=1,nelement
     anorm=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))**2+(nodepos(2,element(2,i))-nodepos(2,element(1,i)))**2
     anorm=1d0/sqrt(anorm)
     at2(1,i)=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))*anorm
     at2(2,i)=(nodepos(2,element(2,i))-nodepos(2,element(1,i)))*anorm
  enddo
  deallocate(elm,atnd)

  deallocate(elml)
  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do

  shortestedge=minval(elml)
  longestedge=maxval(elml)
  open(99,file=file_log)
  write(99,*) "# Mesh with short edge is eliminated"
  write(99,*) '## nnode=',nnode !統合後の節点数
  write(99,*) '## nelement=',nelement !統合後の要素数
  write(99,*) "## (Longest edge)",longestedge
  write(99,*) "## (Shortest edge)=",shortestedge
  write(99,*) "## (Longest edge)/(Shortest edge)=",longestedge/shortestedge
  close(99)
  
#ifdef DETAIL
  do i=1,nelement !要素改善後の境界要素の絵を描く
     do j=1,2
        write(88+itmp,*) nodepos(:,element(j,i))
     end do
     write(88+itmp,*)
  end do
#endif

  if(shortestedge.le.preset_shortestedge) then
     deallocate(elml)
     at(:,:)=0
     do i=1,nelement
        at(:,i)=at2(:,i)
     end do
     deallocate(at2)
     goto 103
  end if
  deallocate(at)


  allocate(ndiv(nelement))
  do i=1,nelement
     ndiv(i)=int(dble(idiv)*elml(i)/shortestedge) !各要素を何等分するかを決定
  end do
  allocate(elementnew(2,sum(ndiv)))
  allocate(nodeposnew(2,nnode+sum(ndiv-1)))
  nodeposnew(:,1:nnode)=nodepos(:,1:nnode)

  allocate(elm(2,nnode)) !elm(i,j): j番節点のi番要素を返す配列を作る
  elm=0
  do i=1,nnode
     do j=1,nelement
        if(i.eq.element(1,j))then
           elm(1,i)=j
           goto 102
        elseif(i.eq.element(2,j))then
           elm(2,i)=j
           goto 102
        end if
102     continue
     end do
  end do

  if(symmetry.ge.1)then
     allocate(atnd(2,nnode)) !atnd(i,j): j番節点における接線のx_i成分
     do i=1,nnode
        if(elm(1,i).eq.0)then
           if((abs(nodepos(2,i)).le.eps1).or.&
                (abs(nodepos(2,i)-dble(ye)).le.eps1))then !\Gamma_{I_1} or \Gamma_{D_1}
              atnd(1,i)=0.d0
              atnd(2,i)=at2(2,elm(2,i))
           else if((abs(nodepos(1,i)).le.eps1).or.&
                (abs(nodepos(1,i)-dble(xe)).le.eps1))then !\Gamma_{I_2} or \Gamma_{D_2}
              atnd(1,i)=at2(1,elm(2,i))
              atnd(2,i)=0.d0
              ! write(*,*) i,atnd(:,i)
           end if
        else if(elm(2,i).eq.0)then
           if((abs(nodepos(2,i)).le.eps1).or.&
                (abs(nodepos(2,i)-dble(ye)).le.eps1))then
              atnd(1,i)=0.d0
              atnd(2,i)=at2(2,elm(1,i))
           else if((abs(nodepos(1,i)).le.eps1).or.&
                (abs(nodepos(1,i)-dble(xe)).le.eps1))then
              atnd(1,i)=at2(1,elm(1,i))
              atnd(2,i)=0.d0
           end if
        else
           atnd(:,i)=elml(elm(1,i))*at2(:,elm(1,i))&
                +elml(elm(2,i))*at2(:,elm(2,i))
        end if
        anorm=1.d0/sqrt(dot_product(atnd(:,i),atnd(:,i)))
        atnd(:,i)=atnd(:,i)*anorm
     end do
  else
     stop "Sorry, lsf2el2 cannot deal with assymmetric unit cells now."
  end if

  ! do i=1,nnode
  !    atnd(:,i)=elml(elm(1,i))*at2(:,elm(1,i))+elml(elm(2,i))*at2(:,elm(2,i))
  !    ! write(55,*) atnd(:,i)
  !    anorm=1.d0/sqrt(dot_product(atnd(:,i),atnd(:,i)))
  !    atnd(:,i)=atnd(:,i)*anorm
  ! end do

  deallocate(at2,elm)
#ifdef DETAIL
  do i=1,nnode !接線チェック
     !! gnuplot でplot "fort.104" w lp, "fort.88"w lpでチェックできる
     write(104,*) nodepos(:,i)
     write(104,*) nodepos(:,i)+atnd(:,i)
     write(104,*)
     write(104,*)
  end do
#endif
  ! itmp=0
  do i=1,nelement
     !     ferguson曲線を作成
     do j=1,ndiv(i)-1
        au=dble(j)/dble(ndiv(i))
        !        if(ndiv(i).le.2*idiv)then
        if(elml(i).le.dFerguson)then
           f1=1.d0-au
           f2=au
           f3=0.d0
           f4=0.d0
        else
           f1=2.d0*au**3-3.d0*au**2+1.d0
           f2=-2.d0*au**3+3.d0*au**2
           f3=au**3-2.d0*au**2+au
           f4=au**3-au**2
        end if
        xcnt(:)=nodepos(:,element(1,i))*f1&
             +nodepos(:,element(2,i))*f2&
             +atnd(:,element(1,i))*f3&
             +atnd(:,element(2,i))*f4
        !nodeを追加
        nnode=nnode+1
        ! itmp=itmp+1
        ! write(*,*) itmp,sum(ndiv(1:i-1))-i+1+j
        nodeposnew(:,nnode)=xcnt(:)
        elementnew(1,sum(ndiv(1:i-1))+j)=nnode-1
        elementnew(2,sum(ndiv(1:i-1))+j)=nnode
     end do
     !要素を追加
     elementnew(1,sum(ndiv(1:i-1))+1)=element(1,i)
     elementnew(1,sum(ndiv(1:i)))=nnode
     elementnew(2,sum(ndiv(1:i)))=element(2,i)
  end do

#ifdef DETAIL
  do i=1,sum(ndiv)
     write(105,*) nodeposnew(:,elementnew(1,i))
     write(105,*) nodeposnew(:,elementnew(2,i))
     write(105,*)
  end do
#endif

  deallocate(elml)
  allocate(elml(sum(ndiv)))
  do i=1,sum(ndiv) !要素長を計算
     elml(i)=sqrt(dot_product(&
          nodeposnew(:,elementnew(2,i))-nodeposnew(:,elementnew(1,i)),&
          nodeposnew(:,elementnew(2,i))-nodeposnew(:,elementnew(1,i))))
  end do


  shortestedge=minval(elml)
  longestedge=maxval(elml)
  open(99,file=file_log)
  write(99,*) "# Final mesh on physical boundary"
  write(99,*) '## nnode=',nnode
  write(99,*) '## nelement=',sum(ndiv)
  write(99,*) "## (Longest edge)",longestedge
  write(99,*) "## (Shortest edge)=",shortestedge
  write(99,*) "## (Longest edge)/(Shortest edge)=",longestedge/shortestedge
  close(99)
  
  call mesh_peri(shortestedge,ncross)

  deallocate(atnd)

  ! call output_mesh
  ! stop "stop at lsf2el2."

contains
  subroutine mesh_peri(el,ncross)
    use io
    real(8),intent(in)::el
    integer,intent(in)::ncross
    real(8)::ndpos,ua(2,2),xtmp(2)
    real(8),allocatable::ndposperi(:,:),ndposcross(:,:,:),length(:)
    integer,allocatable::ndcross(:,:),nnp(:),ndperi(:,:),elopps(:)
    integer::nn,tmp(4),icross,nb,jtmp,ktmp,nel,nnd

    ! unit vectors
    ua(:,1)=(/1.d0, 0.d0/)
    ua(:,2)=(/0.d0, 1.d0/)

    open(1,file=file_mesh)
    if(ncross.eq.0)then
       nn=int(xe/el)            !1辺の要素数
       nnd=4*nn
       nel=nnd
       allocate(ndposperi(2,nnd))
       ndposperi=0.d0;ndposperi(1,2)=dble(xe);ndposperi(2,3)=dble(ye);ndposperi(:,4)=dble(xe) !o
       do k=1,4              !I1->I2->D1->D2
          if(k.eq.1.or.k.eq.2) xtmp=ndposperi(:,1) !o
          if(k.eq.3) xtmp=ndposperi(:,3)           !o
          if(k.eq.4) xtmp=ndposperi(:,2)           !o
          do i=1,nn-1
             ndposperi(:,4+i+(k-1)*(nn-1))=xtmp& !o
                  +dble(xe)*0.5d0*(1.d0-cos(pi*i/dble(nn)))*ua(:,2-mod(k,2))
          end do
       end do

       allocate(ndperi(2,nel))
       allocate(elopps(nel))
       itmp=0
       do k=1,4
          itmp=itmp+1               !要素番号
          jtmp=nnode+4+(k-1)*(nn-1)+1 !節点番号
          ktmp=(-1)**(k/3)*2*nn+itmp !反対側の要素番号
          if(k.eq.1.or.k.eq.4)then
             ndperi(1,itmp)=nnode+k/4+1 !o
             ndperi(2,itmp)=jtmp             !o
             elopps(itmp)=ktmp
             do i=1,nn-2
                itmp=itmp+1
                ndperi(1,itmp)=jtmp+i-1 !o
                ndperi(2,itmp)=jtmp+i   !o
                elopps(itmp)=ktmp+i
             end do
             itmp=itmp+1
             ndperi(1,itmp)=jtmp+nn-2 !o
             ndperi(2,itmp)=nnode+2*(k+2)/3
             elopps(itmp)=ktmp+nn-1
          else
             ndperi(1,itmp)=jtmp             !o
             ndperi(2,itmp)=nnode+2*k-3 !o
             elopps(itmp)=ktmp
             do i=1,nn-2
                itmp=itmp+1
                ndperi(1,itmp)=jtmp+i   !o
                ndperi(2,itmp)=jtmp+i-1 !o
                elopps(itmp)=ktmp+i
             end do
             itmp=itmp+1
             ndperi(1,itmp)=nnode+k+1
             ndperi(2,itmp)=jtmp+nn-2 !o
             elopps(itmp)=ktmp+nn-1
          end if
       end do

    else if(ncross.gt.0)then
       allocate(ndcross(ncross,4))
       allocate(ndposcross(2,ncross,4))
!!!!!!save node numbers on periodic boundary!!!!!!!!
       tmp=0
       do i=1,nnode
          if(abs(nodeposnew(2,i)).le.eps1)then !底辺
             tmp(1)=tmp(1)+1
             ndcross(tmp(1),1)=i
          else if(abs(nodeposnew(1,i)).le.eps1)then !左辺
             tmp(2)=tmp(2)+1
             ndcross(tmp(2),2)=i
          else if(abs(nodeposnew(2,i)-ye).le.eps1)then !上辺
             tmp(3)=tmp(3)+1
             ndcross(tmp(3),3)=i
          else if(abs(nodeposnew(1,i)-xe).le.eps1)then !右辺
             tmp(4)=tmp(4)+1
             ndcross(tmp(4),4)=i
          end if
       end do
!!!!!!!save node numbers on periodic boundary!!!!!!!
       ! write(*,*) ndcross(:,1)
       ! write(*,*) ndcross(:,2)
       ! write(*,*) ndcross(:,3)
       ! write(*,*) ndcross(:,4)
       ! stop
!!!!!!!!!narabikae!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       do i=1,4
          do icross=2,ncross
             ndpos=nodeposnew(2-mod(i,2),ndcross(icross-1,i))
             if(nodeposnew(2-mod(i,2),ndcross(icross,i)).lt.ndpos)then !昇順
                itmp=ndcross(icross,i)
                ndcross(icross,i)=ndcross(icross-1,i)
                ndcross(icross-1,i)=itmp
             end if
          end do
       end do
!!!!!!!!!narabikae!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       if(atnd(2,ndcross(1,1)).lt.0.d0)then !下辺左端の節点の接線がセルの外を向いているとき
          nb=ncross/2           !設計領域の1辺の周期境界の数
          allocate(nnp(nb))     !各周期境界の要素数
          allocate(length(nb))  !各周期境界の長さ
          do j=1,nb
             length(j)=nodeposnew(1,ndcross(2*j,1))-nodeposnew(1,ndcross(2*j-1,1))
             if(length(j).le.2*el)then
                nnp(j)=1
             else
                nnp(j)=int(length(j)/el-1)
             end if
             if(nnp(j).le.0) stop "error:nn<=0"
          end do
          nnd=4*sum(nnp)
          allocate(ndposperi(2,nnd))
          do k=1,4              !I1->I2->D1->D2
             do j=1,nb
                do i=1,nnp(j)
                   ndposperi(:,i+sum(nnp(1:j-1))+(k-1)*sum(nnp))=nodeposnew(:,ndcross(2*j-1,k))&
                        +length(j)*0.5d0*(1.d0-cos(pi*i/dble(nnp(j)+1)))*ua(:,2-mod(k,2))
                end do
             end do
          end do
!!!!!!!!!connectivity!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          nn=sum(nnp+1)
          nel=4*nn
          allocate(ndperi(2,nel)) !各要素の両端の節点番号を返す
          allocate(elopps(nel))   !反対側の要素番号を返す
          itmp=0
          do k=1,4
             do j=1,nb
                itmp=itmp+1                  !j番目の境界の先頭の要素番号
                jtmp=nnode+(k-1)*sum(nnp)+sum(nnp(1:j-1))+1 !j番目の境界の先頭の節点番号 o
                ktmp=(-1)**(k/3)*2*nn+itmp       !j番目の境界の先頭の反対側の要素番号 o
                if(k.eq.1.or.k.eq.4)then
                   ndperi(1,itmp)=ndcross(2*j-1,k) !o
                   ndperi(2,itmp)=jtmp             !o
                   elopps(itmp)=ktmp
                   do i=1,nnp(j)-1
                      itmp=itmp+1
                      ndperi(1,itmp)=jtmp+i-1 !o
                      ndperi(2,itmp)=jtmp+i   !o
                      elopps(itmp)=ktmp+i
                   end do
                   itmp=itmp+1
                   ndperi(1,itmp)=jtmp+nnp(j)-1 !o
                   ndperi(2,itmp)=ndcross(2*j,k) !o
                   elopps(itmp)=ktmp+nnp(j)
                else
                   ndperi(1,itmp)=jtmp             !o
                   ndperi(2,itmp)=ndcross(2*j-1,k) !o
                   elopps(itmp)=ktmp
                   do i=1,nnp(j)-1
                      itmp=itmp+1
                      ndperi(1,itmp)=jtmp+i   !o
                      ndperi(2,itmp)=jtmp+i-1 !o
                      elopps(itmp)=ktmp+i
                   end do
                   itmp=itmp+1
                   ndperi(1,itmp)=ndcross(2*j,k) !o
                   ndperi(2,itmp)=jtmp+nnp(j)-1 !o
                   elopps(itmp)=ktmp+nnp(j)
                end if
             end do
          end do
!!!!!!!!!connectivity!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

       else if(atnd(2,ndcross(1,1)).gt.0.d0)then !下辺左端の節点の接線がセルの内を向いているとき
          nb=(ncross+2)/2           !設計領域の1辺の周期境界の数 o
          allocate(nnp(nb))     !各周期境界の要素数
          allocate(length(nb))  !各周期境界の長さ
          length(1)=nodeposnew(1,ndcross(1,1)) !o
          length(nb)=dble(xe)-nodeposnew(1,ndcross(ncross,1)) !o
          do j=1,nb
             if(j.ne.1.and.j.ne.nb) length(j)=nodeposnew(1,ndcross(2*j-1,1))-nodeposnew(1,ndcross(2*j-2,1)) !o
             if(length(j).le.2*el)then
                nnp(j)=1
             else
                nnp(j)=int(length(j)/el-1)
             end if
             if(nnp(j).le.0) stop "error:nn<=0"
          end do
          nn=sum(nnp+1)
          nnd=4*nn           !節点数 o
          allocate(ndposperi(2,nnd)) !o
          ndposperi=0.d0;ndposperi(1,2)=dble(xe);ndposperi(2,3)=dble(ye);ndposperi(:,4)=dble(xe) !o
          do k=1,4              !I1->I2->D1->D2
             if(k.eq.1.or.k.eq.2) xtmp=ndposperi(:,1) !o
             if(k.eq.3) xtmp=ndposperi(:,3)           !o
             if(k.eq.4) xtmp=ndposperi(:,2)           !o
             do j=1,nb
                if(j.ge.2)xtmp=nodeposnew(:,ndcross(2*j-2,k)) !o
                do i=1,nnp(j)
                   ndposperi(:,4+i+sum(nnp(1:j-1))+(k-1)*sum(nnp))=xtmp& !o
                        +length(j)/2.d0*(1.d0-cos(pi*i/dble(nnp(j)+1)))*ua(:,2-mod(k,2))
                end do
             end do
          end do
!!!!!!!!!connectivity!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          nel=nnd                 !要素数 o
          allocate(ndperi(2,nel)) !各要素の両端の節点番号を返す o
          allocate(elopps(4*nel))    !反対側の要素番号を返す o
          itmp=0
          do k=1,4
             do j=1,nb
                itmp=itmp+1                  !j番目の境界の先頭の要素番号
                jtmp=nnode+4+(k-1)*sum(nnp)+sum(nnp(1:j-1))+1 !j番目の境界の先頭の節点番号 o
                ktmp=(-1)**(k/3)*2*nn+itmp       !j番目の境界の先頭の反対側の要素番号 o
                if(k.eq.1.or.k.eq.4)then
                   if(j.eq.1) then
                      ndperi(1,itmp)=nnode+k/4+1 !o
                   else
                      ndperi(1,itmp)=ndcross(2*j-2,k) !o
                   end if
                   ndperi(2,itmp)=jtmp             !o
                   elopps(itmp)=ktmp
                   do i=1,nnp(j)-1
                      itmp=itmp+1
                      ndperi(1,itmp)=jtmp+i-1 !o
                      ndperi(2,itmp)=jtmp+i   !o
                      elopps(itmp)=ktmp+i
                   end do
                   itmp=itmp+1
                   ndperi(1,itmp)=jtmp+nnp(j)-1 !o
                   if(j.eq.nb)then
                      ndperi(2,itmp)=nnode+2*(k+2)/3
                   else
                      ndperi(2,itmp)=ndcross(2*j-1,k) !o
                   end if
                   elopps(itmp)=ktmp+nnp(j)
                else
                   ndperi(1,itmp)=jtmp             !o
                   if(j.eq.1)then
                      ndperi(2,itmp)=nnode+2*k-3 !o
                   else
                      ndperi(2,itmp)=ndcross(2*j-2,k) !o
                   end if
                   elopps(itmp)=ktmp
                   do i=1,nnp(j)-1
                      itmp=itmp+1
                      ndperi(1,itmp)=jtmp+i   !o
                      ndperi(2,itmp)=jtmp+i-1 !o
                      elopps(itmp)=ktmp+i
                   end do
                   itmp=itmp+1
                   if(j.eq.nb)then
                      ndperi(1,itmp)=nnode+k+1
                   else
                      ndperi(1,itmp)=ndcross(2*j-1,k) !o
                   end if
                   ndperi(2,itmp)=jtmp+nnp(j)-1 !o
                   elopps(itmp)=ktmp+nnp(j)
                end if
             end do
          end do
!!!!!!!!!connectivity!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

       end if
       deallocate(ndposcross,ndcross,length,nnp)

       ! do i=1,4
       !    do icross=1,ncross
       !       write(*,*) nodeposnew(:,ndcross(icross,i))
       !    end do
       !    write(*,*)
       ! end do
    end if
!!!!!!!!!output!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    write(1,*) nnode+nnd !節点数
    do i=1,nnode
       !          節点番号，座標
       write(1,*) i,nodeposnew(1,i)/dble(xe)*avec(:,1)+nodeposnew(2,i)/dble(ye)*avec(:,2) !長方形格子->平行四辺形格子
    end do
    do i=1,nnd
       write(1,*) i+nnode,ndposperi(1,i)/dble(xe)*avec(:,1)+ndposperi(2,i)/dble(ye)*avec(:,2)
    end do
    write(1,*) sum(ndiv)+nel !要素数
    !             要素番号            ,node番号      ,ibc,iperi(1,i)               ,iperi(2,i)
    do i=1,sum(ndiv)
       write(1,*) i                  ,elementnew(:,i),1  ,0                        ,0
    end do
    itmp=0
    do k=1,4
       do j=1,nn
          itmp=itmp+1
          write(1,*) sum(ndiv)+itmp          ,ndperi(:,itmp) ,10,(mod(k,2)+1)*(-1)**(k/3+1),sum(ndiv)+elopps(itmp)
       end do
    end do
!!!!!!!!!output!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    deallocate(ndposperi,ndperi,elopps)

    close(1)

  end subroutine mesh_peri

  subroutine output_mesh
    use io
    real(8)::x(2),unitx(2,2)


    ! BEMに渡すため、メッシュデータをfileに書き込み
    open(1,file=file_mesh)
    write(1,*) nnode+2*xe+2*ye !節点数
    unitx(:,1)=avec(:,1)/dble(xe)
    unitx(:,2)=avec(:,2)/dble(ye)
    do i=1,nnode
       !          節点番号，座標
       write(1,*) i,nodeposnew(1,i)/dble(xe)*avec(:,1)+nodeposnew(2,i)/dble(ye)*avec(:,2) !長方形格子->平行四辺形格子
    end do
    x=0.d0
    do i=1,xe !底辺
       ! x=x+unitx(:,1)
       x=(1.d0-cos(dble(i)/dble(xe)*pi))*0.5d0*avec(:,1)
       write(1,*) i+sum(ndiv),x
    end do
    do i=1,ye !右辺
       ! x=x+unitx(:,2)
       x=avec(:,1)+(1.d0-cos(dble(i)/dble(ye)*pi))*0.5d0*avec(:,2)
       write(1,*) i+sum(ndiv)+xe,x
    end do
    do i=1,xe !上辺
       ! x=x-unitx(:,1)
       x=avec(:,2)+(1.d0+cos(dble(i)/dble(xe)*pi))*0.5d0*avec(:,1)
       write(1,*) i+sum(ndiv)+xe+ye,x
    end do
    do i=1,ye !左辺
       ! x=x-unitx(:,2)
       x=(1.d0+cos(dble(i)/dble(ye)*pi))*0.5d0*avec(:,2)     
       write(1,*) i+sum(ndiv)+2*xe+ye,x
    end do
    write(1,*) sum(ndiv)+2*xe+2*ye !要素数
    !             要素番号            ,node番号              ,node番号                ,ibc,iperi(:,:)
    do i=1,sum(ndiv)
       write(1,*) i                  ,elementnew(:,i)                              ,1  ,0 ,0
    end do
    write(1,*)    sum(ndiv)+1        ,sum(ndiv)+2*xe+2*ye  ,sum(ndiv)+1            ,10 ,-2,sum(ndiv)+xe+ye+1
    !shita
    do i=2,xe 
       write(1,*) i+sum(ndiv)        ,sum(ndiv)+i-1        ,sum(ndiv)+i            ,10 ,-2,sum(ndiv)+xe+ye+i
    end do
    !hidari
    do i=1,ye
       write(1,*) i+sum(ndiv)+xe     ,sum(ndiv)+2*xe+2*ye-i,sum(ndiv)+2*xe+2*ye-i+1,10 ,-1,sum(ndiv)+2*xe+ye+i
    end do
    !ue
    do i=1,xe
       write(1,*) i+sum(ndiv)+xe+ye  ,sum(ndiv)+2*xe+ye-i  ,sum(ndiv)+2*xe+ye-i+1  ,10 ,2 ,sum(ndiv)+i
    end do
    !migi
    do i=1,ye
       write(1,*) i+sum(ndiv)+2*xe+ye,sum(ndiv)+xe+i-1     ,sum(ndiv)+xe+i         ,10 ,1 ,sum(ndiv)+xe+i
    end do
    close(1)

  end subroutine output_mesh

end subroutine improve_mesh


