/* ACAによりH-matrixを生成 */

#include "harith.h"
#include <stdio.h>
#include <complex.h>
#include <omp.h>

/* リーフブロックリスト構造体の定義 */
typedef struct _blist blist;
typedef blist* pblist;
struct _blist {
  uint bname; /* ブロック番号 */
  uint size;  /* 行数 x 列数 */
};

/* typedef struct _drct drct; */
/* typedef drct* pdrct; */
/* struct _drct { */
/*   pamatrix A_k; */
/*   pamatrix B_k; */
/*   pamatrix A; */
/*   pamatrix B; */
/* }; */

typedef bool (*admissible_peri)(pcluster rc, pcluster cc1, pcluster cc2, void *data);

extern void kernel_(uint *rows, uint *cols, uint *ridx, uint *cidx, field *drct, uint *bflag, uint *pflag, uint *dflag, field *wn);
/* void (*nearfield)(uint *rows, uint *cols, uint *ridx, uint *cidx, field *drct, uint *bflag, uint *pflag, uint *dflag, field *wn); */
phmatrix
new_rk_hmatrix2(pccluster rc, pccluster cc, uint k);
phmatrix
new_full_hmatrix2(pccluster rc, pccluster cc);


/* rows:行サイズ */
/* cols:列サイズ */
/* ridx:行節点番号配列 */
/* cidx:列節点番号配列 */
/* a:要素長さ */
/* drct:行列 */
/* dflag:核関数を区別 */

/* visualise structure of a block cluster tree */
void
block_write(uint rows, uint cols, uint *ridx, uint *cidx, uint flag, FILE *fp, uint *rridx, uint*ccidx)
{
  uint i,j;
  int rmax=0;
  int rmin=10000;
  int cmax=0;
  int cmin=10000;
  for (i=0;i<rows;i++) {
    if(rridx[ridx[i]] >= rmax) rmax=rridx[ridx[i]];
    if(rridx[ridx[i]] <= rmin) rmin=rridx[ridx[i]];
  }
  for (j=0;j<cols;j++) {
    if(ccidx[cidx[j]] >= cmax) cmax=ccidx[cidx[j]];
    if(ccidx[cidx[j]] <= cmin) cmin=ccidx[cidx[j]];
  }
  /* rmin=rmin+1; */
  /* rmax=rmax+1; */
  /* cmin=cmin+1; */
  /* cmax=cmax+1; */
  if (fp != NULL) {
    fprintf(fp,"%d,  %d,  %d\n",rmin,cmin,flag);
    fprintf(fp,"%d,  %d,  %d\n",rmax,cmin,flag);
    fprintf(fp,"%d,  %d,  %d\n",rmax,cmax,flag);
    fprintf(fp,"%d,  %d,  %d\n",rmin,cmax,flag);
    fprintf(fp,"%d,  %d,  %d\n",rmin,cmin,flag);
    fprintf(fp,"%d,  %d,  %d\n",-1,-1,flag);
  }
}

void
block_iterate(pcblock b, FILE *fp, uint *rridx, uint *ccidx)
{
  pcblock   b1;
  int       rsons, csons;
  int       i, j;
  
  if (b->son) {
    rsons = b->rsons;
    csons = b->csons;
    
    for (j = 0; j < csons; j++) {
      for (i = 0; i < rsons; i++) {
    	b1 = b->son[i + j * rsons];
	block_iterate(b1,fp,rridx,ccidx);
      }
    }
  }
  else if (b->a > 0) {		/* admissible */
    uint rsize = b->rc->size;
    uint csize = b->cc->size;
    block_write(rsize, csize,b->rc->idx,b->cc->idx,0,fp,rridx,ccidx);
  }
  else {			/* inadmissible */
    uint rsize = b->rc->size;
    uint csize = b->cc->size;
    block_write(rsize, csize,b->rc->idx,b->cc->idx,1,fp,rridx,ccidx);
  }
}

void
check_block(pcblock b, FILE *fp)
{
  uint rsize = b->rc->size;
  uint csize = b->cc->size;
  uint *ridx = b->rc->idx;
  uint *cidx = b->cc->idx;
  uint *rridx;
  uint *ccidx;
  uint i;

  rridx = malloc(rsize*sizeof(uint));
  ccidx = malloc(csize*sizeof(uint));

  for (i=0;i<rsize;i++) {
    rridx[ridx[i]] = i;
  }

  for (i=0;i<csize;i++) {
    ccidx[cidx[i]] = i;
  }

  fprintf(fp,"%d,  %d,  %d\n",-2,rsize,csize);  
  block_iterate(b, fp, rridx, ccidx);
}

/* ----------------------------------------------------------------------------------------------
 * assemble hmat 
 * ---------------------------------------------------------------------------------------------- */

void partaca(const uint *ridx, const uint rows, const uint *cidx, const uint cols,real accur, uint **rpivot, uint **cpivot, prkmatrix R, uint bflag, uint pflag, uint dflag, field wn){
  /* pdrct D; */
  uint *rperm, *cperm, *rpiv, *cpiv;
  uint i, j, mu, k, i_k, j_k;
  uint dlprows,dlpcols;
  real error, error2, starterror, M;
  field Aij;
  pfield aa, bb;

  struct drct {
    pamatrix A_k;
    pamatrix B_k;
    pamatrix A;
    pamatrix B;
  };

  /* Dのalloc */
  struct drct *D = (struct drct *) allocmem(sizeof(struct drct));
  D->A_k = allocmem(sizeof(amatrix));
  D->B_k = allocmem(sizeof(amatrix));

  k = 0;
  /* D = mk_drct(); */
  rperm = allocmem(rows * sizeof(uint));
  cperm = allocmem(cols * sizeof(uint));
  rpiv = allocmem(rows * sizeof(uint));
  cpiv = allocmem(cols * sizeof(uint));

  /* pivot初期化 */
  for (i = 0; i < rows; ++i) {  
    rpiv[i] = ridx[i];
  }

  /* pivot初期化 */
  for (j = 0; j < cols; ++j) {
    cpiv[j] = cidx[j];
  }

  D->A = &R->A; /* 行列R->AのアドレスをD->Aに代入(あだ名をつける) */
  D->B = &R->B; /* 行列R->BのアドレスをD->Bに代入(あだ名をつける) */

  error = 1.0;
  starterror = 1.0;

  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  /* ここからACA */
  /* D->AとD->Bを計算する (D->A x D->B^T が近似行列)*/
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  while (error > accur && k < rows && k < cols) { /* 許容誤差を下回るか、フルランクとなるまで繰り返す  */
    resizecopy_amatrix(D->A, rows, k + 1); /* D->Aのサイズ1up  */
    resizecopy_amatrix(D->B, cols, k + 1); /* D->Bのサイズ1up  */

    /* aa,bbはこれまでのステップで計算してきた近似行列構造体 */
    aa = D->A->a; 
    bb = D->B->a;

    (void) init_sub_amatrix(D->A_k, D->A, rows - k, k, 1, k);
    (void) init_sub_amatrix(D->B_k, D->B, cols - k, k, 1, k);
    /* D->A_k はこのステップで計算する D->A の部分行列構造体/ */
    /* D->B_k はこのステップで計算する D->B の部分行列構造体/ */
    /* ポインタで参照してるのでD->A_k,D->B_kに対する計算はそのままaa,bbとかD_A,D_Bに反映される */

    if(k>0){
      /* Compute next i_k. */
      M = cabs(aa[k + (k - 1) * rows]);
      i_k = k;
      for (i = k + 1; i < rows; ++i) {
     	if (cabs(aa[i + (k - 1) * rows]) > M) {
     	  M = cabs(aa[i + (k - 1) * rows]);
     	  i_k = i;
	}
      } 
    } 
    else { 
      i_k = rows / 2;
    }
    /* printf("@mkhmat>assemble_hmat>partaca  dflag=%d\n",*dflag); */


    /* Get next column for B.*/
    dlprows = 1;
    dlpcols = cols - k;
    kernel_(&dlprows, &dlpcols, rpiv + i_k, cpiv + k, D->B_k->a, &bflag, &pflag, &dflag, &wn);
    /* (*nearfield)(&dlprows, &dlpcols, rpiv + i_k, cpiv + k, D->B_k->a, bflag, pflag, dflag, wn); */


    /* Subtract current rank-k-approximation. */
    for (j = 0; j < k; ++j) {
      bb[j + k * cols] = 0.0;
    }
    for (mu = 0; mu < k; ++mu) {
      Aij = aa[i_k + mu * rows]; 
      for (j = k; j < cols; ++j) {
	bb[j + k * cols] = bb[j + k * cols] - bb[j + mu * cols] * Aij;
      } 
    }

    /* Compute next j_k. */
    M = cabs(bb[k + k * cols]);
    j_k = k;
    for (j = k + 1; j < cols; ++j) {
      if (cabs(bb[j + k * cols]) > M) {
    	M = cabs(bb[j + k * cols]);
	j_k = j;
      }
    }

    /* Get next column for A. */
    dlprows = rows - k;
    dlpcols = 1;
    kernel_(&dlprows, &dlpcols, rpiv + k, cpiv + j_k, D->A_k->a, &bflag, &pflag, &dflag, &wn);
    /* (*nearfield)(&dlprows, &dlpcols, rpiv + k, cpiv + j_k, D->A_k->a, bflag, pflag, dflag, wn); */


    /* Subtract current rank-k-approximation. */
    for (i = 0; i < k; ++i) {
      aa[i + k * rows] = 0.0;
    }
    for (mu = 0; mu < k; ++mu) {
      Aij = bb[j_k + mu * cols];
      for (i = k; i < rows; ++i) {
    	aa[i + k * rows] = aa[i + k * rows] - aa[i + mu * rows] * Aij;
      }
    }
    Aij = 1.0 / bb[j_k + k * cols];
    for (i = 0; i < rows; ++i) {
      aa[i + k * rows] = aa[i + k * rows] * Aij;
    }

    /* Update permutations.*/
    i = rpiv[k];
    rpiv[k] = rpiv[i_k];
    rpiv[i_k] = i;
    rperm[k] = i_k;

    j = cpiv[k];
    cpiv[k] = cpiv[j_k];
    cpiv[j_k] = j;
    cperm[k] = j_k;

    k++;

    /* Apply permutation to current approximation */
    for (i = 0; i < k; ++i) {
      Aij = aa[i_k + i * rows];
      aa[i_k + i * rows] = aa[k - 1 + i * rows];
      aa[k - 1 + i * rows] = Aij;
    }

    for (j = 0; j < k; ++j) {
      Aij = bb[j_k + j * cols];
      bb[j_k + j * cols] = bb[k - 1 + j * cols];
      bb[k - 1 + j * cols] = Aij;
    }

    /* 1ステップ目の誤差評価 */
    if (k == 1) {
      /* Computation of starterror. */
      starterror = 0.0; /* 行ベクトルの大きさ */
      for (i = k - 1; i < rows; ++i) {
    	starterror += ABSSQR(aa[i]);
      }
      error = 0.0; /* 列ベクトルの大きさ */
      for (j = k - 1; j < cols; ++j) {
    	error += ABSSQR(bb[j]);
      }
      starterror = 1.0 / REAL_SQRT(starterror * error); /* 1ステップ目の誤差 (= 行、列ベクトルのFrobeniusノルムの積) */
    }


    /* Computation of current relative error. */
    error = 0.0; /* 行ベクトルの大きさ */
    for (i = k - 1; i < rows; ++i) {
      error += ABSSQR(aa[i + (k - 1) * rows]);
    }
    error2 = 0.0; /* 列ベクトルの大きさ */
    for (j = k - 1; j < cols; ++j) {
      error2 += ABSSQR(bb[j + (k - 1) * cols]);
    }
    error = REAL_SQRT(error * error2) * starterror; /* kステップ目の相対誤差 */

    uninit_amatrix(D->A_k);
    uninit_amatrix(D->B_k);
  }
  /* if(k==rows&&bflag==8)printf("full-rank "); */
  /* if(k!=rows&&bflag==8)printf("compressed "); */
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  /* ここまでACA */
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */

  R->k = k;

  /* Reverse pivot permutations */
  aa = D->A->a;
  bb = D->B->a;
  for (i = k; i-- > 0;) {
    for (j = 0; j < k; j++) {
      Aij = aa[i + j * rows];
      aa[i + j * rows] = aa[rperm[i] + j * rows];
      aa[rperm[i] + j * rows] = Aij;

      Aij = bb[i + j * cols];
      bb[i + j * cols] = bb[cperm[i] + j * cols];
      bb[cperm[i] + j * cols] = Aij;
    }
  }

  conjugate_amatrix(D->B);

  if (rpivot != NULL) {
    *rpivot = allocuint(k);
    for (i = 0; i < k; ++i) {
      (*rpivot)[i] = rpiv[i];
    }
  }

  if (cpivot != NULL) {
    *cpivot = allocuint(k);
    for (i = 0; i < k; ++i) {
      (*cpivot)[i] = cpiv[i];
    }
  }

  freemem(D->A_k);
  freemem(D->B_k);
  freemem(D);
  freemem(rperm);
  freemem(cperm);
  freemem(rpiv);
  freemem(cpiv);

}

int
sort( const void * a , const void * b ) 
{
  return ((blist *)b)->size - ((blist *)a)->size;
}

void cnt_leaf(pcblock b, uint *nad, uint *ninad){
  pcblock b1;
  int i,j;
  int csons,rsons;
  if(b->son){
    rsons=b->rsons;
    csons=b->csons;
    for(j=0;j<csons;j++){
      for(i=0;i<rsons;i++){
	b1=b->son[i+j*rsons];
	cnt_leaf(b1,nad,ninad);
      }
    }
  }
  else
    if(b->a>0){/* admissible */
      *nad+=1;
    }
    else{ /* inadmissible */
      *ninad+=1;
    }
}

/* すべてのブロックに番号付けるための再帰関数              *
 * ブロック番号(bname)とHマトリクスの部分行列を対応付け    *
 * hn[bname]=hm: bname番ブロックと対応するHマトリクスはhm */
static void nrbr(pcblock b, uint bname, phmatrix hm, phmatrix *hn){
  uint bname1;
  uint i, j;

  assert(hm->rc==b->rc); /* hmのrow clusterとbのrow clusterは一致するはず */
  assert(hm->cc==b->cc); /* hmのcol clusterとbのcol clusterは一致するはず */
  hn[bname]=hm;
  bname1=bname+1;
  /* block bに対応するhmがNULL-->hn[bname]=NULL */
  /* block bの子供に対応するhmがNULL-->hn[bname1]=NULL */
  if (hm==NULL||hm->son==NULL) 
    for (j=0;j<b->csons;j++)
      for (i=0;i<b->rsons;i++){
	nrbr(b->son[i+j*b->rsons],bname1,NULL,hn);
	bname1+=b->son[i+j*b->rsons]->desc;
      }
  /* ブロックbに対応するHマトリクスhmがいる => hn[bname]=hm (nrbrの第3引数が非0) */
  else {
    assert(b->rsons==hm->rsons);
    assert(b->csons==hm->csons);
    for(j=0;j<b->csons;j++)
      for(i=0;i<b->rsons;i++){
	nrbr(b->son[i+j*b->rsons],bname1,hm->son[i+j*b->rsons],hn);
	bname1+=b->son[i+j*b->rsons]->desc;
      }
  }
  assert(bname1==bname+b->desc);
}

/* すべてのブロックに番号付け */
phmatrix *nrbr_hmatrix(pcblock b, phmatrix hm){
  phmatrix *hn;

  hn = (phmatrix *) allocmem((size_t) sizeof(phmatrix) * b->desc);
  nrbr(b, 0, hm, hn);

  return hn;
}

void assemble_hmatrix(pblock b, phmatrix G, real eps_aca, uint bflag, uint pflag, uint dflag ,field wn){
  uint i;
  phmatrix htmp,*hn;
  uint *ridx,*cidx;
  uint rows,cols;
  uint nad,ninad;

  /* printf("kflag=%d, bflag=%d, pflag=%d, dflag=%d\n",kflag,bflag,pflag,dflag); */
  /* 行列要素の計算プログラムを設定(partaca.cと下のassemble_block_hmatrixで呼び出す) */
  /* if(kflag == 1) nearfield=kernel_;      /\* 順・随伴の係数行列の計算 *\/ */
  /* if(kflag == 2) nearfield=kernelin_;    /\* 内点用係数行列の計算 *\/ */
  /* if(kflag == 3) nearfield=kernelobj_;   /\* 目的関数用係数行列の計算 *\/ */
  /* if(kflag == 4) nearfield=kerneladj_;   /\* 随伴問題の入射波用係数行列の計算(境界) *\/ */
  /* if(kflag == 5) nearfield=kerneladjin_; /\* 随伴問題の入射波用係数行列の計算(内点) *\/ */
  /* nearfieldはglobal*/

  /* printf("@mkhmat>assemble_hmat %d\n",pflag); */


  hn = nrbr_hmatrix(b, G);

  /* リーフブロックのリストを作成 */
  nad=0;
  ninad=0;
  cnt_leaf(b,&nad,&ninad);

  blist bl[nad+ninad];
  uint cnt=0;
  for (i=0;i<b->desc;i++){
    if((hn[i]->r) || (hn[i]->f)){ /* admissilbe(r) or inadmissible(f) */
      rows=hn[i]->rc->size;
      cols=hn[i]->cc->size;
      bl[cnt].size=rows*cols;
      bl[cnt].bname=i;
      cnt+=1;
     }
  }

  /* /\* サイズの大きい順に並び替え *\/ */
  /* qsort(bl,nad+ninad,sizeof(blist),sort); */

  /* ブロックサイズ順に行列要素を計算 */
/* #pragma omp parallel for private(rows, cols, ridx, cidx, htmp) schedule(dynamic,1) */
  for (i=0;i<(nad+ninad);i++) {
    htmp = hn[bl[i].bname];
    ridx = htmp->rc->idx;
    cidx = htmp->cc->idx;
    rows = htmp->rc->size;
    cols = htmp->cc->size;

    /* admissible leaf */
    /* ACA */
    if (htmp->r) {
      /* if(bflag==8)printf("ACA\n"); */
      partaca(ridx, rows, cidx, cols, eps_aca, NULL, NULL, htmp->r, bflag, pflag, dflag, wn);
    }
    /* inadmissible leaf */
    /* 直接計算 */
    else if (htmp->f) {
      //      printf("Direct\n");
      kernel_(&rows, &cols, ridx, cidx, htmp->f->a, &bflag, &pflag, &dflag, &wn);
      /* (*nearfield)(&rows, &cols, ridx, cidx, htmp->f->a, &bflag, &pflag, &dflag, &wn); */
    }
  }
    
  freemem(hn);
  hn = NULL;

}


phmatrix
mkhmat(pcluster root1, pcluster root2, real *eta, real eps_aca, uint bflag, uint pflag, uint dflag, field wn)
{
  pblock block;
  phmatrix hmat;

  /* ブロッククラスターツリーを生成 */
  block = build_nonstrict_block(root1, root2, eta, admissible_2_min_cluster);
  /* block = build_nonstrict_block(root1, root2, eta, admissible_max_cluster); */

  hmat = build_from_block_hmatrix(block,0);
  /* assemble_hmatrix(block, hmat[i], *eps_aca, dflag[i], *bflag, *kflag); */

  /* ACAで行列要素計算 */
  assemble_hmatrix(block, hmat, eps_aca, bflag, pflag, dflag, wn);

  del_block(block);

  return hmat;
}
  
pblock
build_nonstrict_block_peri(pcluster rc, pcluster cc1, pcluster cc2, void *eta, admissible_peri admis)
{
  pblock    b;

  bool      a;
  uint      rsons, csons;
  uint      i, j;

  /* printf("\nhello\n"); */

  a = admis(rc, cc1, cc2, eta);

  if (a == false) {
    /* inadmissible leaf */
    if (rc->sons * cc1->sons == 0) {
      rsons = 0;
      csons = 0;
    }
    /* no leaf */
    else {
      rsons = rc->sons;
      csons = cc1->sons;
    }
  }
  /* admissible leaf */
  else {
    assert(a == true);
    rsons = 0;
    csons = 0;
  }

  b = new_block(rc, cc1, a, rsons, csons);

  for (i = 0; i < rsons; i++) {
    for (j = 0; j < csons; j++) {
      /* printf("b->son[%d]\n",i+j*rsons); */
      /* printf("rc->sons=%d\n",rc->sons); */
      /* printf("cc1->sons=%d\n",cc1->sons); */
      /* printf("cc2->sons=%d\n",cc2->sons); */
      b->son[i + j * rsons] =
        build_nonstrict_block_peri(rc->son[i], cc1->son[j], cc2->son[j], eta, admis);
    }
  }

  update_block(b);

  return b;
}

bool
admissible_2_min_cluster_peri(pcluster rc, pcluster cc1, pcluster cc2, void *data)
{
  bool      i;
  real      eta = *(real *) data;;
  real      diamt, diams, dist, dist1, dist2, a;

  diamt = getdiam_2_cluster(rc);
  diams = getdiam_2_cluster(cc1);
  dist1 = getdist_2_cluster(rc, cc1);
  dist2 = getdist_2_cluster(rc, cc2);
  dist = REAL_MIN(dist1,dist2);

  a = REAL_MIN(diamt, diams);
  /* printf("%e %e\n",dist,a); */

  if (a <= eta * dist) {
    /* printf("true\n"); */
    i = true;
  }
  else {
    /* printf("false\n"); */
    i = false;
  }

  return i;
}

bool
admissible_max_cluster_peri(pcluster rc, pcluster cc1, pcluster cc2, void *data)
{
  bool      i;
  real      eta = *(real *) data;;
  real      diamt, diams, dist, dist1, dist2, a;

  diamt = getdiam_2_cluster(rc);
  diams = getdiam_2_cluster(cc1);
  dist1 = getdist_2_cluster(rc, cc1);
  dist2 = getdist_2_cluster(rc, cc2);
  dist = REAL_MIN(dist1,dist2);

  a = REAL_MAX(diamt, diams);

  if (a <= eta * dist) {
    /* printf("true\n"); */
    i = true;
  }
  else {
    /* printf("false\n"); */
    i = false;
  }

  return i;
}

/* ------------------------------------------------------------
 Build identity H-matrix based on block tree
 ------------------------------------------------------------ */
prkmatrix
init_identity_rkmatrix(prkmatrix r, uint rows, uint cols, uint k)
{
  init_identity_amatrix(&r->A, rows, k);
  init_identity_amatrix(&r->B, cols, k);
  r->k = k;

  return r;
}

prkmatrix
new_identity_rkmatrix(uint rows, uint cols, uint k)
{
  prkmatrix r;

  r = (prkmatrix) allocmem(sizeof(rkmatrix));

  return init_identity_rkmatrix(r, rows, cols, k);
}

phmatrix
new_identity_rk_hmatrix(pccluster rc, pccluster cc, uint k)
{
  phmatrix  hm;

  hm = new_hmatrix(rc, cc);

  hm->r = new_identity_rkmatrix(rc->size, cc->size, k);

  hm->desc = 1;

  return hm;
}

phmatrix
new_identity_full_hmatrix(pccluster rc, pccluster cc)
{
  phmatrix  hm;

  hm = new_hmatrix(rc, cc);

  hm->f = new_identity_amatrix(rc->size, cc->size);

  hm->desc = 1;

  return hm;
}

phmatrix
build_from_block_identity_hmatrix(pcblock b, uint k)
{
  phmatrix  h, h1;
  pcblock   b1;
  int       rsons, csons;
  int       i, j;

  h = NULL;

  if (b->son) {
    rsons = b->rsons;
    csons = b->csons;

    h = new_super_hmatrix(b->rc, b->cc, rsons, csons);

    for (j = 0; j < csons; j++) {
      for (i = 0; i < rsons; i++) {
	b1 = b->son[i + j * rsons];

	h1 = build_from_block_identity_hmatrix(b1, k);

	ref_hmatrix(h->son + i + j * rsons, h1);
      }
    }
  }
  else if (b->a > 0)
    h = new_identity_rk_hmatrix(b->rc, b->cc, k);
  else
    h = new_identity_full_hmatrix(b->rc, b->cc);

  update_hmatrix(h);

  return h;
}

phmatrix
build_from_block_zero_hmatrix(pcblock b, uint k)
{
  phmatrix  h, h1;
  pcblock   b1;
  int       rsons, csons;
  int       i, j;

  h = NULL;

  if (b->son) {
    rsons = b->rsons;
    csons = b->csons;

    h = new_super_hmatrix(b->rc, b->cc, rsons, csons);

    for (j = 0; j < csons; j++) {
      for (i = 0; i < rsons; i++) {
	b1 = b->son[i + j * rsons];

	h1 = build_from_block_zero_hmatrix(b1, k);

	ref_hmatrix(h->son + i + j * rsons, h1);
      }
    }
  }
  else if (b->a > 0){
    /* printf("rk\n"); */
    h = new_rk_hmatrix2(b->rc, b->cc, k);
  }
  else{
    /* printf("full\n"); */
    h = new_full_hmatrix2(b->rc, b->cc);
  }
 
  update_hmatrix(h);

  return h;
}

/*-----------------------------------------------------------------------------------------------*/
phmatrix
mkhmat_peri(pcluster root1, pcluster root2, pcluster root3, real *eta, real eps_aca,
	    uint bflag, uint pflag, uint dflag, field wn)
{
  pblock block;
  pamatrix a;
  phmatrix hmat;
  /* FILE *fp2; */

  /* ブロッククラスターツリーを生成 */
  block = build_nonstrict_block_peri(root1, root2, root3, eta, admissible_2_min_cluster_peri);
  /* block = build_nonstrict_block_peri(root1, root2, root3, eta, admissible_max_cluster_peri); */

  if((pflag==2 && dflag==1 && root1==root3) || (pflag==2 && dflag==2 && root1==root2)){
    hmat = build_from_block_zero_hmatrix(block, 0);
    a = new_identity_amatrix(root1->size, root1->size);
    add_amatrix_hmatrix(0.5, false, a, 0, eps_aca, hmat);
    del_amatrix(a);
  }else{
    /* printf("aca\n"); */
    hmat = build_from_block_hmatrix(block,0);

    /* ACAで行列要素計算 */
    assemble_hmatrix(block, hmat, eps_aca, bflag, pflag, dflag, wn);
  }

  /* fp2=fopen("block.res","w"); */
  /* check_block(block, fp2); */
  del_block(block);

  return hmat;
}

void
mkhmat_peri2(pcluster root1, pcluster root2, pcluster root3, real *eta, real eps_aca,
	     uint bflag, uint pflag, uint dflag, field wn, phmatrix *hmats)
{
  pblock block;
  pamatrix a;
  /* FILE *fp2; */

  /* ブロッククラスターツリーを生成 */
  block = build_nonstrict_block_peri(root1, root2, root3, eta, admissible_2_min_cluster_peri);
  /* block = build_nonstrict_block_peri(root1, root2, root3, eta, admissible_max_cluster_peri); */

  if((pflag==2 && dflag==1 && root1==root3) || (pflag==2 && dflag==2 && root1==root2)){
    hmats[0] = build_from_block_zero_hmatrix(block, 0);
    a = new_identity_amatrix(root1->size, root1->size);
    add_amatrix_hmatrix(0.5, false, a, 0, eps_aca, hmats[0]);
    del_amatrix(a);
  }else{
    /* printf("aca\n"); */
    hmats[0] = build_from_block_hmatrix(block,0);

    /* ACAで行列要素計算 */
    assemble_hmatrix(block, hmats[0], eps_aca, bflag, pflag, dflag, wn);
  }

  /* fp2=fopen("block.res","w"); */
  /* check_block(block, fp2); */
  del_block(block);

}

