#include "cluster.h"
#include <stdio.h>
#include "curve2d.h"
#include "bem2d.h"
#include "harith.h"
#include "partaca.h"


/* 要修正 */
pcluster integrate_cluster(uint n, pcluster clusters[n])
{
  int i,j,k,size,idt;
  pcluster integrated;
  uint q, *IDX;

  q=2;

  for (i=0;i<n;i++)
    /* printf("%d\n",clusters[i]->size); */

  /* 全体のH-matrix用のインデックスを生成 */
  size=0;			/*  */
  for (i=0;i<n;i++) {
    size+=clusters[i]->size;
  }

  // ???
  IDX = (uint *) allocmem(size * sizeof(uint));

  k=0;
  idt=0;
  for (i=0;i<n;i++) {
    for (j=0;j<clusters[i]->size;j++) {
      IDX[k]=clusters[i]->idx[j]+idt;
      k+=1;
    }
    idt+=clusters[i]->size;
  }

  /* 全体のH-matrix用のクラスターツリーを生成 */
  integrated = new_cluster(size,IDX,n,q);
  for (i=0;i<n;i++) {
    integrated->son[i] = clusters[i];
  }

  return integrated;
}

phmatrix integrate_hmatrix(pcluster rc, pcluster cc, phmatrix hmats[]){
  uint rsons=rc->sons;
  uint csons=cc->sons;
  phmatrix hmat;
  
  /* 全体のH-matrixを生成 */
  /* assert(rsons == rc->sons); */
  /* assert(csons == cc->sons); */

  hmat = new_super_hmatrix(rc, cc, rsons, csons);

  int i;
  for (i=0;i<rsons*csons;i++) {
    ref_hmatrix(hmat->son+i, hmats[i]);
  }

  update_hmatrix(hmat);

  return hmat;
}

phmatrix integrate_hmatrix2(pcluster rc, pcluster cc, uint nhmat, phmatrix hmats[nhmat]){
  uint rsons=rc->sons;
  uint csons=cc->sons;
  phmatrix hmat;
  
  /* 全体のH-matrixを生成 */
  /* assert(rsons == rc->sons); */
  /* assert(csons == cc->sons); */

  hmat = new_super_hmatrix(rc, cc, rsons, csons);

  int i;
  for (i=0;i<rsons*csons;i++) {
    ref_hmatrix(hmat->son+i, hmats[i]);
  }

  update_hmatrix(hmat);

  return hmat;
}


/* 前進 & 後退代入のwrapper */
void lusolve_hmatrix(bool atrans, phmatrix hmat, uint brows, uint bcols, field btmp[bcols][brows]){
  pavector bvec;
  pavector  v;
  uint i,j;

  /* printf("@lusolve :%d\n",hmat); */
  
  v = (pavector) allocmem(sizeof(avector));
  bvec = init_avector(v, brows);
  for(i=0;i<bcols;i++){
    for(j=0;j<brows;j++){
      bvec->v[j]=btmp[i][j];
    }
  /* printf("atrans:%d\n",atrans); */

  
    lrsolve_hmatrix_avector(atrans, hmat, bvec);

    for(j=0;j<brows;j++){
      btmp[i][j]=bvec->v[j];
    }
  }

  del_avector(bvec);
}

pcluster
w_new_cluster(uint *size, uint **idx, uint *sons, uint *dim)
{
  printf("%d,%d,%d\n",*size,*sons,*dim);
  return new_cluster(*size, *idx, *sons, *dim);
}

void show_vec(int n, double **vec){
  /* printf("%e,%e\n", *vec, *vec[1]); */
  //printf("%f\n",*vec[0]);
}

void show_mat(int n, double mat[2][n]){
  printf("%e\n",mat[1][1]);
}

/* invertion of hmatrixのwrapper */
phmatrix
invert_hmat(phmatrix hmat, double tol){
  phmatrix work;
  

  /* printf("@ludecomp:%d\n",hmat->rsons); */

  work=clonestructure_hmatrix(hmat);
  
  invert_hmatrix(hmat, work, 0, tol);

  del_hmatrix(work);
  return hmat;
}

/* Hmatとamatのかけ算のwrapper */
void addmul_hmat_amat_amat(field alpha, uint lda, uint ldb, uint rhb, bool atrans, pchmatrix a, bool btrans, field b[rhb][ldb], bool ctrans, field c[rhb][lda]){
  amatrix tmp1,tmp2;
  pamatrix bp,cp;
  uint i,j;
  const uint *ridx,*cidx;

  ridx = a->rc->idx;
  cidx = a->cc->idx;

  bp = init_amatrix(&tmp1,ldb,rhb);                 /* bmat構造体生成 */
  for (j = 0; j < rhb; j++)
    for (i = 0; i < ldb; i++)
      bp->a[i+ldb*j] = b[j][cidx[i]]; /* bmatの構造をhmatにあわせてbpにぶらさげる */

  cp = init_amatrix(&tmp2,lda,rhb);                 /* cmat構造体生成 */
  for (j = 0; j < rhb; j++)
    for (i = 0; i < lda; i++)
      cp->a[i+lda*j] = c[j][ridx[i]]; /* cmatの構造をhmatにをあわせてcpにぶらさげる */

  addmul_hmatrix_amatrix_amatrix(alpha, atrans, a, btrans, bp, ctrans, cp); /* C=C+AB */

  for (j = 0; j < rhb; j++)
    for (i = 0; i < lda; i++)
      c[j][ridx[i]] = cp->a[i+lda*j];

  uninit_amatrix(bp);
  uninit_amatrix(cp);
}

/* Hmatとamatのたし算のwrapper */
void add_hmat_amat(field alpha, uint ldb, uint rhb, bool atrans, pchmatrix a, field b[rhb][ldb]){
  amatrix tmp;
  pamatrix bp;
  uint i,j;
  const uint *ridx,*cidx;

  ridx = a->rc->idx;
  cidx = a->cc->idx;

  bp = init_amatrix(&tmp,ldb,rhb);                 /* 構造体生成 */
  for (j = 0; j < rhb; j++)
    for (i = 0; i < ldb; i++)
      bp->a[i+ldb*j] = b[cidx[j]][ridx[i]];

  add_hmatrix_amatrix(alpha, atrans, a, bp); /* B=B+alpha*A */

  for (j = 0; j < rhb; j++)
    for (i = 0; i < ldb; i++)
      b[cidx[j]][ridx[i]] = bp->a[i+ldb*j];

  uninit_amatrix(bp);
}

/* y=y+[hmat]x */
void mat_vec_(uint *m, uint *n, phmatrix hmat, field *xtmp, field *ytmp){
  pavector x;
  pavector y;

  x=new_avector(*n);
  y=new_avector(*m);

  x->v=xtmp;
  y->v=ytmp;

  addeval_hmatrix_avector(1.0+0.0*I, hmat, x, y);

  return;
}

pcurve2d
build_from_el2_curve2d(uint edges, uint vertices, double *x, uint *nd, uint nmin, double *an, double *lngth)
{
  pcurve2d gr; /* 要素情報 */
  
  /* gr(構造体)を生成 */
  gr = (pcurve2d) allocmem(sizeof(curve2d));
  /* 1次元配列をcastしてぶら下げる */
  gr->x = (real (*)[2]) x; /* 節点座標 */
  gr->e = (uint (*)[2]) nd; /* 節点番号 */
  gr->n = (real (*)[2]) an; /* 法線ベクトル */
  gr->g = (real *) lngth; /* 要素長  */
  gr->vertices = vertices;
  gr->edges = edges;

  /* printf("gr->e[2]=%u\n",*(gr->e[2])); */
  /* printf("gr->x[2]=%f\n",*(gr->x[*(gr->e[2])])); */
  
  return gr;

}

pcluster
new_constant_bem2d_cluster(pbem2d bem, uint nmin)
{
  return build_bem2d_cluster(bem,nmin,BASIS_CONSTANT_BEM2D);

}

pcluster
new_innerpoint_bem_cluster(pclustergeometry cf, uint nmin)
{
  uint i;
  /* 内点のindexを生成 */
  uint n = cf->nidx; // 内点数
  uint *idx = (uint *) allocmem(n * sizeof(uint));
  for(i=0; i<n; i++){
    idx[i] = i;
  }
  
  return build_adaptive_cluster(cf, n, idx, nmin);

}

/* tはroot clusterに限る */
void
del_idx_cluster(pcluster t)
{
  freemem(t->idx);
}

pclustergeometry
build_innerpoint_bem2d_clustergeometry(uint n, double *x)
{
  uint dim = 2;
  
  pclustergeometry cf = new_clustergeometry(dim, n);

  uint i, j;
  for(j=0; j<n; j++){
    for(i=0; i<dim; i++){
      cf->x[j][i] = x[i+j*dim];
      cf->smin[j][i] = x[i+j*dim];
      cf->smax[j][i] = x[i+j*dim];
    }
  }

  return cf;
}

void
coarsen_hmat(phmatrix G, ptruncmode tm, real eps)
{
  bool recursive = true;
  /* printf("%d ",recursive); */
  coarsen_hmatrix(G, tm, eps, recursive);
  /* coarsen_hmatrix(G, tm, eps, recursive); */
}
