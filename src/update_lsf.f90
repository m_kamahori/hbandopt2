subroutine update_lsf(sflg,step)
  use globals
  use io
  implicit none
  integer,intent(in)::sflg,step

  if(sflg.eq.1)then
     call update_lsf1(step)
  else
     call update_lsf2(step)
     ! stop "stop@update_lsf"
  end if

contains
  subroutine update_lsf2(step)
    integer i,j,ix,iy,maxpos(2)
    integer,intent(in)::step
    real(8) :: dx,dy
    real(8)::phi
    ! real(8),parameter::phi=1.d-1

    if(maxval(td(:,:)) <= 0.d0)stop "# Full bandgap width is Converged."

    maxpos(:)=maxloc(td(:,:))
    ix=maxpos(1)-1;iy=maxpos(2)-1 !td$B$,:GBgCM$r$H$k3J;RE@HV9f(B
    if(symmetry.eq.2)then         !$BBP>]$J3J;RE@A4$F$KHy>.$J7j$r$"$1$k(B
       phi=0.5d0*lsfunc(ix,iy)
       lsfunc(ix   ,   iy)=-phi
       lsfunc(xe-ix,   iy)=-phi
       lsfunc(ix   ,ye-iy)=-phi
       lsfunc(xe-ix,ye-iy)=-phi
       lsfunc(iy   ,   ix)=-phi
       lsfunc(iy   ,ye-ix)=-phi
       lsfunc(xe-iy,   ix)=-phi
       lsfunc(xe-iy,ye-ix)=-phi
    else
       write(*,*) "coming soon@updatelsf2"
    end if
    file_lsf=update_lsf_filename(step)
    open(100,file=file_lsf)
    do iy=0,ye
       do ix=0,xe
          dx=dble(ix)/dble(xe)*avec(1,1)+dble(iy)/dble(ye)*avec(1,2)
          dy=dble(ix)/dble(xe)*avec(2,1)+dble(iy)/dble(ye)*avec(2,2)
          write(100,fmt_d3) dx,dy,lsfunc( ix,iy ) !gfortran
       end do
    end do
    close(100)

    deallocate(lsfunc,td)
  end subroutine update_lsf2
  
  subroutine update_lsf1(step)
    integer i,j,ix,iy
    integer,intent(in)::step
    integer::xeg,yeg,nnodeg,bwidth,ns
    integer::vv(4)
    real(8),allocatable::kk_band(:,:),mm_band(:,:)
    real(8) a,b
    integer nn
    real(8) m
    real(8) tw,tt
    real(8),allocatable::xx(:),lsftmp(:)
    real(8) cc

    integer info
    real(8) ::prm_dt,volume1! ,prm_volc,prm_dt
    real(8) :: evol !:=\int_D |td| dV
    real(8) :: dx,dy
    real(8),parameter :: offset=1.d0
    ! character(len=32)::file_lsf2,stepname2

    volume1=0.d0
    evol=0.d0
    ns=0
    do iy=0,ye
       do ix=0,xe
          ! if(lsfunc(ix,iy).gt.0.d0)then !$BM6EEBNNN0h(B
          !    volume1=volume1+1.d0
          !    ! td(ix,iy)=-td(ix,iy) !maximisation
          ! end if
          if(sd(ix,iy).ne.0.d0)ns=ns+1
          evol=evol+abs(sd(ix,iy))
       end do
    end do


    ! $B%Q%i%a!<%?99?7(B ($B;~4V%9%F%C%W(B)
    tw=dble(step-tloop1)/dble(tloop2-tloop1)
    if(tw<0.0d0) tw=0.0d0
    if(tw>1.0d0) tw=1.0d0
    prm_dt=deltat1*(1.0d0-tw)+deltat2*tw !$B8=:_$N%9%F%C%W$K$*$1$k;~4V%9%F%C%W(B

    xeg=xe+1
    yeg=ye+1
    nnodeg=xeg*yeg

    ! K$B9TNs(B,M$B9TNs$N:n@.(B
    bwidth=xeg+1
    allocate(kk_band(bwidth+1,nnodeg),mm_band(bwidth+1,nnodeg))
    kk_band(:,:)=0.0d0
    mm_band(:,:)=0.0d0
    ! $B;M3Q7AMWAG(B
    do iy=0,ye-1
       do ix=0,xe-1! $B;M3Q7A$N:82<$N3J;RE@:BI8$r!J(Bix,iy$B!K$H$7$?;~$N@aE@HV9f(B
          vv(1)=xeg*iy    +ix    +1 ! $B:82<(B 4-----3
          vv(2)=xeg*iy    +(ix+1)+1 ! $B1&2<(B |     |
          vv(3)=xeg*(iy+1)+(ix+1)+1 ! $B1&>e(B |     |
          vv(4)=xeg*(iy+1)+ix    +1 ! $B:8>e(B 1-----2
          do i=1,4
             do j=1,4
                if(vv(i).ge.vv(j)) then
                   a=-1.0d0/6.0d0
                   b=1.0d0/18.0d0
                   if(mod(i+j,2).eq.0) then
                      a=-1.0d0/3.0d0
                      b=1.0d0/36.0d0
                   end if
                   if(i.eq.j)then
                      a=2.0d0/3.0d0
                      b=1.0d0/9.0d0
                   end if
                   nn=bwidth+vv(j)-vv(i)+1
                   kk_band(nn,vv(i))=kk_band(nn,vv(i))+a
                   mm_band(nn,vv(i))=mm_band(nn,vv(i))+b
                end if
             end do
          end do
       end do
    end do

    !($B:8JU(B)=[M]/dt+tau*[K]
    !$B$3$l$r(Bkk_band$B$K3JG<(B
    tt=xe*ye*prm_tau !prm_tau=$BJ#;(EY78?t(B
    do j=1,nnodeg
       do i=1,bwidth+1
          kk_band(i,j)=kk_band(i,j)*tt 
          mm_band(i,j)=mm_band(i,j)/prm_dt
       end do
    end do
    do j=1,nnodeg
       do i=1,bwidth+1
          kk_band(i,j)=kk_band(i,j)+mm_band(i,j)
       end do
    end do

    ! $B1&JU%Y%/%H%k(B
    allocate(xx(0:nnodeg-1))
    xx(:)=0.d0
    cc=prm_c*ns/evol
    ! cc=prm_c*volume1/evol   !1$BNN0h$@$H6u9&$N%H%]%m%8!<F34X?t$O7W;;$5$l$J$$$?$a(Bvolume1$B$rMQ$$$k(B
    ! cc=prm_c*xe*ye/evol     !xe*ye:=($B@_7WNN0h$NLL@Q(B)

    !$B1&JU%Y%/%H%k(Blsfunc+cc*dt*T$B$r7W;;(B
    allocate(lsftmp(0:nnodeg-1))
    lsftmp(:)=0
    do j=0,ye
       do i=0,xe
          lsftmp(xeg*j+i)=lsfunc(i,j)+sd(i,j)*prm_dt*cc
       end do
    end do
    deallocate(sd)

    ![M]/dt*lsftmp(i)$B$r7W;;$9$k!#(Bxx$B$K$$$l$k!#(B
    call dsbmv("U",nnodeg,bwidth,1.d0,mm_band,bwidth+1,lsftmp(0),1,0.d0,xx(0),1)

!!!!!!!!!!!!!!!!!!!!!!$B@)Ls>r7o(B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !$B6-3&>e$N(Bifix=1$B$NMWAG$G$O(Blsf$B$O>o$K(Boffset$B$K$J$k!#(B
    !$BH36bK!(B,m=1000000
    m=1000000.d0
    do j=1,ye+1     !lsf(0,j) and lsf(1,j)
       kk_band(bwidth+1,xeg*j-xe)=m*kk_band(bwidth+1,xeg*j-xe)
       kk_band(bwidth+1,xeg*j-xe+1)=m*kk_band(bwidth+1,xeg*j-xe+1)
       xx(xeg*(j-1))=offset*kk_band(bwidth+1,xeg*j-xe)
       xx(xeg*(j-1)+1)=offset*kk_band(bwidth+1,xeg*j-xe+1)
    end do
    do j=1,ye+1     !lsf(xe,j)
       kk_band(bwidth+1,xeg*j)=m*kk_band(bwidth+1,xeg*j)
       kk_band(bwidth+1,xeg*j-1)=m*kk_band(bwidth+1,xeg*j-1)
       xx(xeg*(j-1)+xe)=offset*kk_band(bwidth+1,xeg*j)
       xx(xeg*(j-1)+xe-1)=offset*kk_band(bwidth+1,xeg*j-1)
    end do
    do i=1,xe+1     !lsf(i,0)
       kk_band(bwidth+1,i )=m*kk_band(bwidth+1,i )
       kk_band(bwidth+1,i+xeg)=m*kk_band(bwidth+1,i+xeg)
       xx(i-1)=offset*kk_band(bwidth+1,i )
       xx(i-1+xeg)=offset*kk_band(bwidth+1,i+xeg)
    end do
    do i=1,xe+1     !lsf(i,ye)
       kk_band(bwidth+1,xeg*ye+i )=m*kk_band(bwidth+1,xeg*ye+i )
       kk_band(bwidth+1,xeg*(ye-1)+i )=m*kk_band(bwidth+1,xeg*(ye-1)+i )
       xx(xeg*ye+i-1)=offset*kk_band(bwidth+1,xeg*ye+i )
       xx(xeg*(ye-1)+i-1)=offset*kk_band(bwidth+1,xeg*(ye-1)+i )
    end do
!!!!!!!!!!!!!!!!!!!!!!$B@)Ls>r7o(B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    call dpbsv("U",nnodeg,bwidth,1,kk_band,bwidth+1,xx(0),nnodeg,info)
    if(info.ne.0) stop "@updatelsf info.ne.0"

    do j=0,ye
       do i=0,xe
          lsfunc(i,j)=xx(xeg*j+i)
          ! write(60,*) i/dble(xe),j/dble(ye),lsfunc(i,j)
       end do
    end do

    ! call impose_symmetry

    do j=0,ye
       do i=0,xe
!!!!!!!!!!!!!!!!!!!!!!$B@)Ls>r7o(B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if(lsfunc(i,j).gt. 1.d0) lsfunc(i,j)=1.d0  !lsf no jougen
          if(lsfunc(i,j).lt.-1.d0) lsfunc(i,j)=-1.d0 !lsf no kagen
!!!!!!!!!!!!!!!!!!!!!!$B@)Ls>r7o(B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       end do
    end do

    ! write(stepname2,'(i4.4)') step+1
    ! file_lsf2=trim("lsf/step"//trim(stepname2)//".ls2")
    file_lsf=update_lsf_filename(step)
    open(100,file=file_lsf)
    do iy=0,ye
       do ix=0,xe
          dx=dble(ix)/dble(xe)*avec(1,1)+dble(iy)/dble(xe)*avec(1,2)
          dy=dble(ix)/dble(xe)*avec(2,1)+dble(iy)/dble(xe)*avec(2,2)
          ! dx=xelm*dble(i)
          ! dy=yelm*dble(j)
          !        write(11,'(2i,f16.12)') i,j,lsfunc( i,j ) !ifort
          write(100,fmt_d3) dx,dy,lsfunc( ix,iy ) !gfortran
       end do
    end do
    close(100)

    deallocate(kk_band,xx,mm_band,lsftmp,lsfunc)


  end subroutine update_lsf1
end subroutine update_lsf
