subroutine SSM(step)
  use omp_lib
  use class_hbem
  use io
  implicit none
  integer::ssinfo,evflg(2),nfail
  integer,parameter::nfailmax=2
  integer::iskbn,idomp,nperi,ihank
  integer::ibril,iir,nir
  complex(8)::zc,ztmp

  type integral_route
     integer::n                   !number of integral points
     complex(8),allocatable::p(:) !integral points
     complex(8)::c                !center of integral route
     real(8)::r                   !radius of integral route
     real(8)::l                   !valid ev range
  end type integral_route
  type(integral_route),allocatable::ir(:) !integral routes
  type(eigenpairs),allocatable::e(:,:)

  complex(8),allocatable::vmat(:,:)
  complex(8),allocatable::ymat(:,:,:)
  real(8)::ins,int
  ! real(8),allocatable::ttmp(:,:)
  integer,intent(inout)::step
  complex(8),allocatable::shat(:,:,:,:) !=1/N sum_{j=0}^{N-1} ((omega_j-cent)/rho)^{k+1} A(z)^{-1} V
  complex(8),allocatable::mhat(:,:,:) !=Hankel行列
  complex(8),allocatable::h(:,:),sh(:,:) !=Hankel行列

  interface
     subroutine solver(idomp,ibril,nirep,nperi,vmat,ymat,islv)
       integer,intent(in)::ibril,nirep,nperi,idomp,islv
       complex(8),intent(in)::vmat(:,:)
       complex(8),intent(out)::ymat(:,:)
     end subroutine solver
  end interface

  write(*,*) "# Mesh generation"
  call checktime(tins,tint)

  call load(step)                     !BEMのmesh, SS法のparameterを読み込み
  call gauss                    !Gauss求積の積分点、重みを設定
  call generate_cluster         !境界及び内点クラスターを生成


  nperi=sum(edges(2:5))         !周期境界要素数


  write(*,*) "numomp=",numomp
  write(*,*) "    ne=",ne
  write(*,*) " nskbn=",nskbn
  write(*,*) " nbril=",nbril

100 continue
  if(evslctr.eq.0)then
     nir=init_ncent
     evflg(:)=mode(:)
  else
     nir=2
  endif

  call alloc_ssm_arrays
  call random_matrix(ne,nblck,vmat) !乱数を作成し、vmatに格納
  
  allocate(ir(nir))
  do iir=1,nir
     if(evslctr.eq.0)then
        ir(iir)%l=init_rad*(1.d0-mrate)
        ir(iir)%r=init_rad
        ir(iir)%c=cmplx(ir(iir)%l,0.d0,kind=8)+(iir-1)*2*cmplx(ir(iir)%l,0.d0,kind=8)
     else
        ir(iir)%l=(maxval(real(ev_slc(iir,:)))-minval(real(ev_slc(iir,:))))/2.d0+deltair !bandgap/2+deltair
        ir(iir)%r=ir(iir)%l/(1.d0-mrate)
        ir(iir)%c=(maxval(real(ev_slc(iir,:)))+minval(real(ev_slc(iir,:))))/2.d0
     end if
     allocate(ir(iir)%p(0:nskbn-1))
     do iskbn=0,nskbn-1         !計算に時間のかかる積分点から順にomgj(nskbn)に格納
        if(mod(iskbn,2).eq.0)then
           ir(iir)%p(iskbn)=ir(iir)%c+ir(iir)%r*exp((iskbn+1.d0)/dble(nskbn)*pi*ione)
        else
           ir(iir)%p(iskbn)=ir(iir)%c+ir(iir)%r*exp(-iskbn/dble(nskbn)*pi*ione)
        end if
        write(101,*) real(ir(iir)%p(iskbn)),aimag(ir(iir)%p(iskbn)) !積分路の座標
     end do
  end do

  ! allocate(ttmp(2,0:numomp-1))
  do iir=1,nir              !積分円のループ
     write(*,*) "[",real(ir(iir)%c)-ir(iir)%l,":",real(ir(iir)%c)+ir(iir)%l,"]"
     write(*,'(a,$)') "iskbn"
     !---(ここから)----A(z, K) ymat = vmatを解いて、ymatを作成し、モーメントshat=\int z^k ymat dz を計算-----
     shat(:,:,:,:)=zero
     !$omp parallel do private(idomp,ibril,zc,ihank),schedule(auto)
     do iskbn=0,nskbn-1
        write(*,'(a,$)') "."
        idomp=omp_get_thread_num()
        call build_bem_matrices_precom(ir(iir)%p(iskbn),idomp)

        do ibril=1,nbril    !波数ベクトルのループ
           ! A(z) ymat = vmatを解く
           call solver(idomp,ibril,edges(1),nperi,vmat,ymat(:,:,idomp),islv)
           !$omp critical
           zc=(ir(iir)%p(iskbn)-ir(iir)%c)/ir(iir)%r
           do ihank=0,2*nhank-1
              shat(:,:,ihank,ibril)=shat(:,:,ihank,ibril)+zc*ymat(:,:,idomp)
              zc=zc*(ir(iir)%p(iskbn)-ir(iir)%c)/ir(iir)%r
           end do
           !$omp end critical
        end do !ibril
        call delete_hmatrices(idomp)
     end do !iskbn
     !$omp end parallel do
     write(*,*)
     shat(:,:,:,:)=shat(:,:,:,:)/dble(nskbn)
     !---(ここまで)----A(z, K) ymat = vmatを解いて、ymatを作成し、モーメントshat=\int z^k ymat dz を計算-----
     do ibril=1,nbril
        call build_hankel_matrices
        ssinfo=0
        call sssolve(ssinfo)
        if(ssinfo.eq.1)then  !バンド解析に失敗した場合
           nfail=nfail+1
           write(*,*) "nfail=",nfail
           ! if(evslctr.eq.0)then
           if(nfail.eq.nfailmax)then
              evslctr=0;nfail=0
              deallocate(ir,e,hstry(step)%e)    !nirを変更するために開放しておく
              goto 100
           end if
           exit
        end if
     end do
     if(ssinfo.eq.1)cycle
     nfail=0
  end do !iir
  hstry(step)%e(:,:)=e(:,:)


  call output

  call set_evv_ref
  ! if(hstry(step)%f.lt.0.1d0)then
  !    evslctr=0
  ! else
     evslctr=1
  ! end if

     
  ! if(mflag.eq.1)then
     call innerpoints           !内点計算
  ! else
  !    ! do idomp=0,numomp-1
  !    !    write(111,*) idomp,ttmp(:,idomp)
  !    ! end do
  !    ! open(1,file='time_fwd.dat',access='append') 
  !    ! write(1,'(4e25.16)') edges(1)/dble(nperi),sum(ttmp(1,:))/numomp,sum(ttmp(2,:))/numomp
  !    ! close(1)
  !    stop "@output"
  ! end if

  ! write(*,*)"# Innerpoints"
  ! call checktime(tins,tint)
  ! time(2)=tint


99 continue

  call dealloc_bem_matrices
  call delete_clusters(6,bc)
  call dealloc_bem_arrays

contains
  subroutine set_evv_ref
    integer::i,j,k,jtmp
    real(8)::rtmp
    real(8),parameter::delta=1.d-3

    open(1,file=file_ref)
    do ibril=1,nbril
       do k=1,2
          do i=1,4
             rtmp=0.d0
             do j=1,nref/4
                ! 最も距離の小さい選点の要素番号jtmpをsearch
                jtmp=neighbor_elm(xref(:,j+nref/4*(i-1)),c(:,:),sum(edges(1:i))+1,sum(edges(1:i+1)))
                if(jtmp.eq.0)then
                   write(*,*) "physical boundary crosses periodic boundary"
                   evv_ref(j+nref/4*(i-1),k,ibril)=zero !周期境界ではないのでMACの計算には用いない
                else
                   evv_ref(j+nref/4*(i-1),k,ibril)=evv_slc(jtmp,k,ibril)
                end if
                rtmp=rtmp+abs(evv_ref(j+nref/4*(i-1),k,ibril))
             end do
             rtmp=rtmp/nref*4
             !固有ベクトルの絶対値の総和平均がほぼゼロならゼロに近似
             if(rtmp.lt.delta.and.i.le.2)then !qの値のみ近似
                evv_ref(nref/4*(i-1)+1:nref/4*i,k,ibril)=zero
             end if
             do j=1,nref/4
                write(1,*) evv_ref(j+nref/4*(i-1),k,ibril)
             end do
          end do
       end do
    end do
    close(1)
    
  end subroutine set_evv_ref

  !要素番号minからmaxの間の要素の中で、中心が点xに最も近い要素の要素番号を返す
  function neighbor_elm(x,c,min,max)
    real(8),intent(in)::x(:),c(:,:)
    integer,intent(in)::min,max
    integer::neighbor_elm
    integer::i,n
    real(8)::r,rtmp

    r=dot_product(x-c(:,min),x-c(:,min))
    n=0
    do i=min+1,max
       rtmp=dot_product(x-c(:,i),x-c(:,i))
       if(r > rtmp .and. sqrt(rtmp) <= lngth(i)/2)then
          r=rtmp
          n=i
       end if
    end do
    neighbor_elm=n

  end function neighbor_elm

  subroutine alloc_ssm_arrays
    ! 次のstepまでに必ずdeallocateすること
    if(allocated(shat).eqv..false.)         allocate(shat(ne,nblck,0:2*nhank-1,nbril))
    if(allocated(mhat).eqv..false.)         allocate(mhat(nblck,nblck,0:2*nhank-1))
    if(allocated(h).eqv..false.)            allocate(h(nblck*nhank,nblck*nhank))
    if(allocated(sh).eqv..false.)           allocate(sh(nblck*nhank,nblck*nhank))
    if(allocated(ymat).eqv..false.)         allocate(ymat(ne,nblck,0:numomp-1)) !ymat=A(z)^{-1} vmat
    if(allocated(vmat).eqv..false.)         allocate(vmat(ne,nblck))           !random vector
    if(allocated(hstry(step)%e).eqv..false.)allocate(hstry(step)%e(nir,nbril))
    if(allocated(e).eqv..false.)            allocate(e(nir,nbril))
    if(allocated(evv_slc).eqv..false.)      allocate(evv_slc(ne,2,nbril))
  end subroutine alloc_ssm_arrays

  subroutine build_hankel_matrices
    integer::jtmp,itmp,i,j,k

    !---(ここから)----u * shat を計算して、Hankel行列の要素(サブ行列)を作成----------------
    mhat(:,:,:)=zero
    do k=0,2*nhank-1
       ! mhat=mhat+vmat^T*shat
       call zgemm("T","N",nblck,nblck,nblck,one,vmat,ne,shat(:,:,k,ibril),ne,one,mhat(:,:,k),nblck)
    end do
    !---(ここまで)----u * shat を計算して、Hankel行列の要素(サブ行列)を作成----------------
    !---(ここから)----モーメントをHankel行列に格納---------------------------------------
    jtmp=0
    do j=1,nhank
       itmp=0
       do i=1,nhank
          h(itmp+1:itmp+nblck,jtmp+1:jtmp+nblck)=mhat(:,:,i+j-2)
          sh(itmp+1:itmp+nblck,jtmp+1:jtmp+nblck)=mhat(:,:,i+j-1)
          itmp=itmp+nblck
       end do
       jtmp=jtmp+nblck
    end do
    !---(ここまで)----モーメントをHankel行列に格納---------------------------------------

  end subroutine build_hankel_matrices

  subroutine sssolve(ssinfo)
    integer::m,itmp,i,j
    integer,intent(out)::ssinfo
    integer,allocatable::ichk(:)  !使う固有値のフラグ
    !---lapack---------------------
    integer::lwork,lrwork,info
    real(8),allocatable::rwork(:)
    real(8),allocatable::sgm(:)
    complex(8),allocatable::alp(:),bet(:)
    complex(8),allocatable::vr(:,:)
    complex(8),allocatable::zwork(:,:)
    complex(8),allocatable::work(:)
    !---lapack---------------------
    complex(8),allocatable::shattmp(:,:)
    real(8)::delta_img,sgm_p

    !---(ここから)----Hankel行列を特異値分解して固有値の数を決める--------------------------
    m=nhank*nblck
    allocate(sgm(m))
    lwork=3*m
    allocate(work(lwork))
    lrwork=5*m
    allocate(rwork(lrwork))
    allocate(zwork(m,m))
    zwork(:,:)=h(:,:)
    ! call write_mat(m,m,zwork,1111)
    ! write(*,*)"m=",m
    ! stop
    sgm=0.d0;;work=0.d0;
    call zgesvd("N","N",m,m,zwork,m,sgm,ztmp,1,ztmp,1,work,lwork,rwork,info)

    deallocate(work,rwork,zwork)

#ifdef DETAIL
    do i=1,m
       write(300+iir,*) ibril,sgm(i)/sgm(1)
       write(400+iir,*) ibril,sgm(i)
    end do
#endif

    if(evslctr.eq.0)then
       delta_img=1.d-3
       sgm_p=1.d-8
       ! sgm_p=1.d-10
    else
       delta_img=1.d-3
       ! sgm_p=5.d-7
       sgm_p=1.d-10
    endif

    do i=1,m
       if(sgm(i)/sgm(1).le.sgm_p) goto 101
    end do
    ! if(sgm(i).le.1.d-6) goto 101
101 continue
    m=i-1 !固有値の数
    if(allocated(e(iir,ibril)%flag).eqv..true.)deallocate(e(iir,ibril)%flag)
    if(allocated(e(iir,ibril)%stmp).eqv..true.)deallocate(e(iir,ibril)%stmp)
    allocate(e(iir,ibril)%flag(m))
    allocate(e(iir,ibril)%stmp(m))
    !---(ここまで)----Hankel行列を特異値分解して固有値の数を決める--------------------------

    !---(ここから)----一般化固有値問題を解くと、もとの問題の固有値が求まる---------------------
    allocate(alp(m),bet(m))
    allocate(vr(m,m))
    lwork=2*m
    allocate(work(lwork))
    lrwork=8*m
    allocate(rwork(lrwork))
    call zggev("N","V",m,sh(1:m,1:m),m,h(1:m,1:m),m,alp,bet,ztmp,1,vr,m,work,lwork,rwork,info)
    ! write(*,*) "zggev_info=",info
    !---(ここまで)----一般化固有値問題を解くと、もとの問題の固有値が求まる---------------------
    !---(ここから)----いらない固有値をplot捨てる----------------------------------------
    allocate(ichk(m))
    ichk(:)=0
    do j=1,m
       ztmp=alp(j)/bet(j)*ir(iir)%r+ir(iir)%c
       e(iir,ibril)%stmp(j)=ztmp
       if(abs(aimag(ztmp))<=delta_img.and.real(ztmp)>=(real(ir(iir)%c)-ir(iir)%l)&
            .and.real(ztmp)<(real(ir(iir)%c)+ir(iir)%l).and.real(ztmp)>0.d0) then
          !積分円の端の固有値と虚部の大きい固有値を排除。
          ichk(j)=1
       end if
    end do
    !---(ここまで)----いらない固有値を捨てる-------------------------------------------
    e(iir,ibril)%n=sum(ichk)
    if(e(iir,ibril)%n.eq.0.and.evslctr.eq.1)then
       write(*,*) "NO EIGENVALUE",iir,ibril
       ssinfo=1
       goto 98
    end if
    if(allocated(e(iir,ibril)%s).eqv..true.)deallocate(e(iir,ibril)%s)
    if(allocated(e(iir,ibril)%v).eqv..true.)deallocate(e(iir,ibril)%v)
    allocate(e(iir,ibril)%s(e(iir,ibril)%n))
    allocate(e(iir,ibril)%v(ne,e(iir,ibril)%n))
    !---(ここから)----固有ベクトル-----------------------------------------------------
    itmp=m/nblck
    allocate(shattmp(ne,m))
    do j=1,itmp
       do i=1,nblck
          shattmp(1:ne,(j-1)*nblck+i)=shat(1:ne,i,j-1,ibril)
       end do
    end do
    do i=nblck*itmp+1,m
       shattmp(1:ne,i)=shat(1:ne,i-nblck*itmp,itmp,ibril)
    end do
    do i=1,m
       if(ichk(i).eq.1)then
          e(iir,ibril)%s(sum(ichk(1:i)))=e(iir,ibril)%stmp(i)
          e(iir,ibril)%v(:,sum(ichk(1:i)))=matmul(shattmp(:,:),vr(:,i))
       end if
    end do
    deallocate(e(iir,ibril)%stmp)
    deallocate(sgm,alp,bet,vr,work,rwork,shattmp,ichk)
    !---(ここまで)----固有ベクトル-----------------------------------------------------
    ! 固有値・固有ベクトルを昇順にソート
    do i=1,e(iir,ibril)%n-1
       do j=i+1,e(iir,ibril)%n
          if(real(e(iir,ibril)%s(i)) > real(e(iir,ibril)%s(j)))then
             call cswap(1 ,e(iir,ibril)%s(i)  ,e(iir,ibril)%s(j)  )
             call cswap(ne,e(iir,ibril)%v(:,i),e(iir,ibril)%v(:,j))
          end if
       end do
    end do

    if(evslctr.eq.1)then !MACで固有値を選別
       call select_ev(ssinfo) !固有値の選別
    elseif(evslctr.eq.0)then
       if(iir.eq.nir)then
          call select_ev2
             ! if(step.eq.step1)then
             !    do ibril=1,nbril
             !       itmp=0
             !       do iir=1,nir
             !          do i=1,e(iir,ibril)%n
             !             itmp=itmp+1
             !             if(itmp.eq.mode(1))then
             !                ev_slc(1,ibril)=e(iir,ibril)%s(i)
             !                evv_slc(:,1,ibril)=e(iir,ibril)%v(:,i)
             !             else if(itmp.eq.mode(2))then
             !                ev_slc(2,ibril)=e(iir,ibril)%s(i)
             !                evv_slc(:,2,ibril)=e(iir,ibril)%v(:,i)
             !             end if
             !          end do
             !       end do
             !    end do
          ! end if
       end if
    end if
    if(real(ev_slc(2,ibril)) < real(ev_slc(1,ibril))) stop "error@sssolve"

98  continue

    if(ssinfo.eq.1)then    !同じモードの固有値がなかった時
       ir(iir)%l=ir(iir)%l+deltair
       ir(iir)%r=ir(iir)%r+deltair
       iir=iir-1       
       return
    end if
    ssinfo=0
  end subroutine sssolve

  subroutine select_ev2
    integer::ii,jj,k,l,pos(1,2),jtmp
    real(8)::lngth,b
    real(8),allocatable::de(:),v(:,:)
    complex(8),allocatable::etmp(:),vtmp(:,:)
          
    if(step.eq.0)then
       jtmp=0
       do k=1,nir
          do ii=1,e(k,ibril)%n
             jtmp=jtmp+1
             if(jtmp.eq.mode(1))then
                ev_slc(1,ibril)=e(k,ibril)%s(ii)
                evv_slc(:,1,ibril)=e(k,ibril)%v(:,ii)
             else if(jtmp.eq.mode(2))then
                ev_slc(2,ibril)=e(k,ibril)%s(ii)
                evv_slc(:,2,ibril)=e(k,ibril)%v(:,ii)
             end if
          end do
       end do
    else
       ! 固有値の数を数える
       l=0
       do k=1,nir
          l=l+e(k,ibril)%n
       end do
       ! 固有値・固有ベクトルを一つの配列に格納
       allocate(etmp(l),vtmp(ne,l))
       jtmp=0
       do k=1,nir
          do jj=1,e(k,ibril)%n
             jtmp=jtmp+1
             etmp(jtmp)=e(k,ibril)%s(jj)
             vtmp(:,jtmp)=e(k,ibril)%v(:,jj)
          end do
       end do

       if(hstry(step)%f < 0.1d0)then
          ! ############固有値の変化で取りこぼしを感知###########
          allocate(v(l,2))
          v(:,1)=abs(etmp(:)-real(ev_slc(1,ibril)))
          v(:,2)=abs(etmp(:)-real(ev_slc(2,ibril)))
          ! target固有値の変化が小さいか変化の大きい固有値しか見つからなかった時
          if((v(mode(1),1) < deltair.and.v(mode(2),2) < deltair)&
               .or.minval(v(:,1)) > deltair.or.minval(v(:,2)) > deltair)then
             pos(1,1)=mode(1)
             pos(1,2)=mode(2)
          else
             allocate(de(l))
             b=(real(ev_slc(1,ibril))+real(ev_slc(2,ibril)))*0.5d0
             de(:)=abs(etmp(:)-b)
             pos(:,1)=minloc(de(:))
             pos(:,2)=minloc(de(:),mask=de(:)>de(pos(1,1)))
             if(real(etmp(pos(1,1))) > real(etmp(pos(1,2))))call iswap(1,pos(:,1),pos(:,2))
             write(*,*) ibril,pos(1,1),pos(1,2)
             ! if(pos(1,1).eq.pos(1,2))then
             !    if(v(pos(1,1),1) > v(pos(1,2),2))then
             !       pos(:,1)=minloc(v(:,1),mask=v(:,1)>v(pos(1,1),1))
             !       write(*,*) "collision!",ibril,1,pos(1,1)
             !       do ii=1,l
             !          write(*,*) ii,v(ii,1)
             !       end do
             !    else
             !       pos(:,2)=minloc(v(:,2),mask=v(:,2)>v(pos(1,2),2))
             !       write(*,*) "collision!",ibril,2,pos(1,2)
             !       do ii=1,l
             !          write(*,*) ii,v(ii,2)
             !       end do
             !    end if
             ! end if
          end if
          deallocate(v)
          ! ############固有値の変化で取りこぼしを感知###########

          ! ############固有値間の幅の変化で取りこぼしを感知###########
          ! allocate(de(l-1))
          ! lngth=real(ev_slc(2,ibril))-real(ev_slc(1,ibril))
          ! do ii=1,l-1
          !    de(ii)=real(etmp(ii+1))-real(etmp(ii))
          ! end do
          ! if(abs(de(mode(1))-lngth) > 3*deltair)then
          !    ! write(*,*)ibril,de(mode(1)),lngth
          !    ! write(*,*)minloc(abs(de(:)-lngth)),l
          !    pos(:,1)=minloc(abs(de(:)-lngth))
          !    pos(1,2)=pos(1,1)+1
          ! else
          !    pos(1,1)=mode(1)
          !    pos(1,2)=mode(2)
          ! end if
          ! deallocate(de)
          ! ############固有値間の幅の変化で取りこぼしを感知###########
       else
          b=hstry(step)%cof     !center of the full bandgap
          allocate(de(l))
          de(:)=real(etmp(:))-b !relative position of ev from the center of the full bandgap
          pos(:,1)=maxloc(de(:),mask=de(:) < 0)
          pos(:,2)=minloc(de(:),mask=de(:) > 0)
          deallocate(de)
       end if
       ev_slc(1,ibril)=etmp(pos(1,1))
       ev_slc(2,ibril)=etmp(pos(1,2))
       evv_slc(:,1,ibril)=vtmp(:,pos(1,1))
       evv_slc(:,2,ibril)=vtmp(:,pos(1,2))
       deallocate(etmp,vtmp)
    end if

  end subroutine select_ev2

  subroutine select_ev(info)
    use io
    integer,intent(out)::info
    integer::i,j,k,l,itmp,jtmp,m
    integer,allocatable::tmp(:)
    real(8)::x,y,dx,dy,r,rtmp,z(e(iir,ibril)%n,4),zavg(e(iir,ibril)%n)
    real(8),parameter::min_mac=0.6d0
    real(8),parameter::delta=1.d-3
    complex(8)::v(nref/4,4,e(iir,ibril)%n)
    complex(8),allocatable::phi_ref(:),phi_cmp(:),cnd(:)

    interface
       real(8) function MAC(n,r,c)
         integer,intent(in)::n
         complex(8),intent(in)::r(n),c(n)
       end function MAC
    end interface

    m=e(iir,ibril)%n            !固有値の候補の数

    ! MACの計算につかう固有ベクトルを配列vに格納
    do i=1,4               !loop of periodic boundary
       do j=1,nref/4
          ! 最も近い選点の要素番号を探索
          jtmp=neighbor_elm(xref(:,j+nref/4*(i-1)),c(:,:),sum(edges(1:i))+1,sum(edges(1:i+1)))
          if(jtmp.eq.0)then
             v(j,i,:)=zero !PECなのでMACの計算には用いない
          else
             do k=1,m
                v(j,i,k)=e(iir,ibril)%v(jtmp,k)
#ifdef EV_DATA
                write(step*10000+iir*1000+ibril*10+k,fmt_d4) xref(:,j+nref/4*(i-1)),real(v(j,i,k)),aimag(v(j,i,k))
#endif
             end do
#ifdef EV_DATA
             write(step*10000+iir*1000+ibril*10,fmt_d3) xref(:,j+nref/4*(i-1)),real(evv_ref(j+nref/4*(i-1),iir,ibril))
#endif
          end if
       end do
    end do

    do k=1,m
       do i=1,2                 !qの値について
          rtmp=0.d0
          do j=1,nref/4
             rtmp=rtmp+abs(v(j,i,k))
          end do
          rtmp=rtmp/nref*4
          if(rtmp.lt.delta)v(:,i,k)=zero !固有ベクトルの絶対値の総和平均がほぼゼロならゼロに近似
       end do
    end do
    
    ! 1辺ずつMACを計算して判定
    itmp=nref/4            !周期境界の1辺のMACを計算する点の数
    allocate(phi_ref(itmp),phi_cmp(itmp))
    do i=1,4
       do k=1,m
          do j=1,nref/4
                phi_ref(j)=evv_ref(j+nref/4*(i-1),iir,ibril)
                phi_cmp(j)=v(j,i,k)
          end do
          z(k,i)=MAC(itmp,phi_ref,phi_cmp) !MACを計算
          ! write(*,*) k,i,z(k,i)
#ifdef MAC_DATA
          write(step*10+iir,*) ibril,z(k,i),i
#endif
       end do
       if(maxval(z(:,i)).le.min_mac)then
          write(*,*) "max(MAC)=",maxval(z(:,i)),i
          write(*,*) "ibril=",ibril,"iir=",iir
          info=1
          return
       end if
    end do
    deallocate(phi_ref,phi_cmp)

    e(iir,ibril)%flag(:)=0
    if(real(ir(1)%c)+ir(1)%l.lt.real(ir(2)%c)-ir(2)%l)then !探索範囲が被っていなければ
       allocate(tmp(1))
       if(iir.eq.1)then
          tmp(:)=maxloc(real(e(iir,ibril)%s(:))) !一番大きい固有値
       elseif(iir.eq.2)then
          tmp(:)=minloc(real(e(iir,ibril)%s(:))) !一番小さい固有値
       end if
    else                     !被っていれば
       !境界ごとのMACの値の総和平均を計算
       zavg(:)=0.d0
       do k=1,m
          do i=1,4
             zavg(k)=zavg(k)+z(k,i)
          end do
       end do
       zavg=zavg/4

       if(maxval(zavg).le.min_mac)then
          write(*,*) "zavg=",zavg
          info=1
          return
       end if

       allocate(tmp(1))
       tmp(:)=maxloc(zavg)
    end if
    e(iir,ibril)%flag(tmp(1))=1 !tmp(1)番固有値を選択
    info=0

    deallocate(tmp)
  end subroutine select_ev

  subroutine output
    integer::i,itmp,minpos(1)

    !---(ここから)----バンド構造の書き出し-----------------------------
    open(999,file=file_band)      !探索した全ての固有値をplot
    do ibril=1,nbril
       do iir=1,nir
          do i=1,e(iir,ibril)%n
             if(symmetry.eq.2)then
                if(ibril.le.nb)then
                   write(999,fmt_d3) -sqrt(dot_product(wv(:,ibril),wv(:,ibril))),real(e(iir,ibril)%s(i)),aimag(e(iir,ibril)%s(i))
                elseif(ibril.le.2*nb)then
                   write(999,fmt_d3) sqrt(dot_product(wv(:,ibril),wv(:,ibril))),real(e(iir,ibril)%s(i)),aimag(e(iir,ibril)%s(i))
                else
                   write(999,fmt_d3) 0.5d0*sqrt(dot_product(gvec(:,1),gvec(:,1)))+&
                        sqrt(dot_product(wv(:,ibril)-0.5d0*gvec(:,1),wv(:,ibril)-0.5d0*gvec(:,1)))&
                        ,real(e(iir,ibril)%s(i)),aimag(e(iir,ibril)%s(i))
                end if
             elseif(symmetry.eq.1)then
                write(999,fmt_d3) dble(ibril),real(e(iir,ibril)%s(i)),aimag(e(iir,ibril)%s(i))
             elseif(symmetry.eq.0)then
                write(999,fmt_d3) wv(:,ibril),real(e(iir,ibril)%s(i)),aimag(e(iir,ibril)%s(i))
             else
                stop "coming soon..."
             end if
          end do
       end do
    end do !ibril
    close(999)        
    !---(ここまで)----バンド構造の書き出し-----------------------------

    ! list target eigenvalue
    if(evslctr.eq.1)then
       do ibril=1,nbril
          do iir=1,nir
             do i=1,e(iir,ibril)%n
                if(e(iir,ibril)%flag(i).eq.1)then
                   ev_slc(iir,ibril)=e(iir,ibril)%s(i)
                   evv_slc(:,iir,ibril)=e(iir,ibril)%v(:,i)
                end if
             end do
          end do
       end do
    end if

    do ibril=1,nbril
       if(real(ev_slc(2,ibril)) < real(ev_slc(1,ibril))) then
          call cswap( 1,ev_slc(1,ibril),ev_slc(2,ibril))
          call cswap(ne,evv_slc(:,1,ibril),evv_slc(:,2,ibril))
       end if
    end do

    ibril2=1;ibril1=1
    do ibril=2,nbril
       if(real(ev_slc(2,ibril2)).gt.real(ev_slc(2,ibril))) ibril2=ibril
       if(real(ev_slc(1,ibril1)).lt.real(ev_slc(1,ibril))) ibril1=ibril
    end do
    write(*,*) 
    write(*,*) "upperev_min",ibril2,real(ev_slc(2,ibril2))
    write(*,*) "lowerev_max",ibril1,real(ev_slc(1,ibril1))



    open(111,file=file_evslct)         !トポロジー導関数を計算する固有値をplot
    do ibril=1,nbril
       if(real(ev_slc(2,ibril)) < real(ev_slc(1,ibril))) stop "error@output"
       do i=1,2
          if(symmetry.eq.2)then
             if(ibril.le.nb)then
                write(111,fmt_d3) -sqrt(dot_product(wv(:,ibril),wv(:,ibril))),real(ev_slc(i,ibril)),aimag(ev_slc(i,ibril))
             elseif(ibril.le.2*nb)then
                write(111,fmt_d3) sqrt(dot_product(wv(:,ibril),wv(:,ibril))),real(ev_slc(i,ibril)),aimag(ev_slc(i,ibril))
             else
                write(111,fmt_d3) 0.5d0*sqrt(dot_product(gvec(:,1),gvec(:,1)))+&
                     sqrt(dot_product(wv(:,ibril)-0.5d0*gvec(:,1),wv(:,ibril)-0.5d0*gvec(:,1)))&
                     ,real(ev_slc(i,ibril)),aimag(ev_slc(i,ibril))
             end if
          elseif(symmetry.eq.1)then
             write(111,fmt_d3) dble(ibril),real(ev_slc(i,ibril)),aimag(ev_slc(i,ibril))
          elseif(symmetry.eq.0)then
             write(111,fmt_d3) wv(:,ibril),real(ev_slc(1,ibril)),aimag(ev_slc(i,ibril))
          else
             write(*,*)'coming soon...'
          end if
       end do
    end do
    close(111)
    
    hstry(step)%f=real(ev_slc(2,ibril2))-real(ev_slc(1,ibril1)) !full bandgap width
    hstry(step)%cof=real(ev_slc(1,ibril1))+hstry(step)%f*0.5d0   !center of full bandgap
    write(*,*) "FULL BANDGAP WIDTH:",hstry(step)%f

    write(*,*) "# Forward analysis"
    call checktime(tins,tint)
    time(1)=tint
    ! en=tins
    ! open(69,file="time_rate.dat",access='append')
    ! write(69,*) edges(1)/dble(nperi),time(1),step,islv
    ! close(69)
    ! stop "@output"
    ! open(69,file="log.dat",access="append")
    ! write(69,*) ne,en-st
    ! close(69)
    ! open(69,file="time_ne.dat")
    ! write(69,*) ne,en-st
    ! close(69)

  end subroutine output

  subroutine innerpoints
    integer::i,k
    complex(8),allocatable::utmp(:),qtmp(:)

    allocate(utmp(ne),qtmp(ne))

    if(islv.ge.1)then
       ic=new_cluster_in(nip,grid,nminin)      !内点clusterを生成
       write(*,'(a,$)') "# Computing innerpoints (with the ACA)"
    else
       write(*,'(a,$)') "# Computing innerpoints (with conv. method)"
    end if

    loop1:do ibril=1,nbril
       write(*,'(a,$)') "."
       loop2:do k=1,2

          do i=1,ne
                
             if(ibc(i).eq.0)then     !Dirichlet
                utmp(i)=zero
                qtmp(i)=evv_slc(i,k,ibril)
             elseif(ibc(i).eq.1)then !Neumann
                utmp(i)=evv_slc(i,k,ibril)
                qtmp(i)=zero
             elseif(ibc(i).eq.10.and.iperi(1,i).gt.0)then !dependent
                utmp(i)=evv_slc(i,k,ibril)*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,i))))
                utmp(iperi(2,i))=evv_slc(i,k,ibril)
             elseif(ibc(i).eq.10.and.iperi(1,i).lt.0)then !independent
                qtmp(i)=evv_slc(i,k,ibril)
                qtmp(iperi(2,i))=-evv_slc(i,k,ibril)*exp(ione*dot_product(wv(:,ibril),avec(:,-iperi(1,i))))
             end if
          end do

          u(:,k,ibril)=utmp(:)
          un(:,k,ibril)=qtmp(:)

          wn_g=ev_slc(k,ibril)
          if(islv.ge.1)then
             call hinnerpoints(utmp,qtmp,k,ibril,ssinfo) !calculate innerpoints with the H-matrix method
          else
             call inner_points(utmp,qtmp,k,ibril)
          end if

          if(ssinfo.eq.1)then
             stop "@innerpoints"
          end if

       end do loop2
    end do loop1
    write(*,*)

    deallocate(utmp,qtmp)
    if(islv.ge.1)call delete_cluster_in(ic)

  end subroutine innerpoints

  subroutine dealloc_bem_arrays
    ! deallocate(an)
    ! deallocate(at)
    ! deallocate(p)
    ! deallocate(nd)
    ! deallocate(c)
    deallocate(ibc)
    deallocate(iperi)
    deallocate(shat)
    deallocate(mhat)
    deallocate(h)
    deallocate(sh)
    ! deallocate(lngth)
    deallocate(ridt,cidt)
    deallocate(ir)
    deallocate(e)
    deallocate(evv_slc)
    ! deallocate(ttmp)

  end subroutine dealloc_bem_arrays

end subroutine SSM

! subc(:,:,idomp)=dmat(edges(1)+1:ne,1:edges(1),idomp)
! workmat(:,:,idomp)=zero
! write(*,*) workmat(1,1,idomp),idomp
! call add_hmat_amat(one,nperi,edges(1),false,hcmat(idomp),workmat(:,:,idomp))
! call error_mat(nperi,edges(1),workmat(:,:,idomp),subc(:,:,idomp))
! stop

! subb(:,:,idomp)=dmat(1:edges(1),1:edges(1),idomp)
! workmat(:,:,idomp)=zero
! call add_hmat_amat(one,edges(1),edges(1),false,hbmat(idomp),workmat(:,:,idomp))
! call error_mat(edges(1),edges(1),workmat(:,:,idomp),subb(:,:,idomp))
! stop
    ! if(sum(zflag).gt.1)then                                   !候補が複数の場合
    !    if(real(ir(1)%c)+ir(1)%l.lt.real(ir(2)%c)-ir(2)%l)then !探索範囲が被っていなければ
    !       ! allocate(cnd(sum(zflag)))                           !candidate eigenvalues
    !       ! itmp=0
    !       ! do k=1,m              !候補の固有値を集める
    !       !    if(zflag(k).eq.1)then
    !       !       itmp=itmp+1
    !       !       cnd(itmp)=e(iir,ibril)%s(k)
    !       !    end if
    !       ! end do
    !       if(iir.eq.1)then
    !          tmp(:)=maxloc(real(e(iir,ibril)%s(:))) !一番大きい固有値
    !       elseif(iir.eq.2)then
    !          tmp(:)=minloc(real(e(iir,ibril)%s(:))) !一番小さい固有値
    !       end if
    !       if(zflag(tmp(1)).ne.1)tmp(:)=maxloc(z) !偽固有値の場合は
    !    else                     !被っていれば
    !       tmp(:)=maxloc(z)      !MACが最大の固有値
    !    end if
    !    e(iir,ibril)%flag(tmp(1))=1
    !    info=0
    ! elseif(sum(zflag).eq.1)then !候補が一つの場合
    !    tmp(:)=maxloc(z)      !MACが最大の固有値
    !    e(iir,ibril)%flag(tmp(1))=1
    ! else
    !    write(*,*) "MAC is smaller than",min_mac
    !    write(*,*) "ibril=",ibril,"iir=",iir
    !    ! write(*,*) maxloc(z)
    !    ! stop "@select_ev2"
    !    deltair=deltair+1.d-1
    !    info=1
    ! end if
