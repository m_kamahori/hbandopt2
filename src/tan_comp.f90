subroutine tan_comp
  use globals
  implicit none
  integer idom,ig,nz,ierr,ibril,k
  integer nj1,nj2,kode,nnt,mmt
  real(8) x1,x2,n1,n2,a
  real(8) r,dotx,doty,dotn,zr,zi,fnu
  real(8) cyr(2),cyi(2),rr1,rr2,theta1,theta2
  complex(8) zcy(2),stmp,dtmp,dstmp,ddtmp,htmp(0:1),stmp2,dtmp2,dstmp2,ddtmp2,zcy2(2),wn
  integer i,j

  ! call gauss
  ut(:,:,:)=cmplx(0.d0,0.d0,kind=8)
  do ibril=1,nbril
     do k=1,2
        wn=ev_slc(k,ibril)
        !$omp parallel do private(stmp,dtmp,dstmp,ddtmp,htmp,nj1,nj2,x1,x2,n1,n2,a,r,dotx,doty,dotn,zr,zi,zcy,rr1,rr2,theta1,theta2)
        do i=1,edges(1)
           do j=1,ne
              dstmp=zero
              ddtmp=zero

              nj1=nd(1,j)
              nj2=nd(2,j)

              x1=-(c(1,i)-c(1,j))*an(2,j)+(c(2,i)-c(2,j))*an(1,j)
              x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j)
              n1=an(1,i)*an(2,j)-an(2,i)*an(1,j)
              n2=an(1,i)*an(1,j)+an(2,i)*an(2,j)

              a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2)

              do ig=1,ng
                 r=sqrt((x1-a*gzai(ig))**2+x2**2)
                 dotx=(x1-gzai(ig)*a)*n2+x2*n1
                 ! dotx=(x1-gzai(ig)*a)*(-n2)+x2*n1
                 dotn=n1
                 zr=real(wn*r)
                 zi=aimag(wn*r)
                 call hankel(1,zr,zi,1,2,htmp)
                 zcy(1)=htmp(0)
                 zcy(2)=htmp(1)

                 if(i.eq.j)then
                    zcy(2)=zcy(2)+ione*2.d0/pi/wn/r !有限部分:G^R
                 end if
                 dstmp=dstmp+wei(ig)*a*(-wn*zcy(2))*dotx/r 
                 ddtmp=ddtmp+wn**2*dotn*wei(ig)*a*zcy(1)
              end do

              if(i.eq.j)then
                 theta1=atan2(x1-a,x2)
                 theta2=atan2(x1+a,x2)
                 dstmp=dstmp+(theta1-theta2)*2.d0*ione/pi*n1&
                      +log(((x1-a)**2.d0+x2**2.d0)/((x1+a)**2.d0+x2**2.d0))*ione/pi*n2
              end if

              rr1=sqrt((c(1,i)-p(1,nj1))**2+(c(2,i)-p(2,nj1))**2)
              zr=real(wn*rr1)
              zi=aimag(wn*rr1)

              call hankel(1,zr,zi,1,2,htmp)
              zcy(1)=htmp(1)

              rr2=sqrt((c(1,i)-p(1,nj2))**2+(c(2,i)-p(2,nj2))**2)
              zr=real(wn*rr2)
              zi=aimag(wn*rr2)

              call hankel(1,zr,zi,1,2,htmp)
              zcy(2)=htmp(1)

              dotx=-an(1,i)*(c(1,i)-p(1,nj1))-an(2,i)*(c(2,i)-p(2,nj1))
              doty=-an(1,i)*(c(1,i)-p(1,nj2))-an(2,i)*(c(2,i)-p(2,nj2))

              ddtmp=ddtmp+wn*(zcy(2)*doty/rr2-zcy(1)*dotx/rr1)

              ut(i,k,ibril)=ut(i,k,ibril)+dstmp*un(j,k,ibril)-ddtmp*u(j,k,ibril)
           enddo
        enddo
        !$omp end parallel do
     end do
  end do

  ut(:,:,:)=ut(:,:,:)*0.5d0*ione

end subroutine tan_comp
