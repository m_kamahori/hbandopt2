function ltc(s,g) result(out)
  implicit none
  real(8) :: s,g
  integer :: out

  if(mod(s,1.d0).eq.0.d0) then
     if(s.ge.g) out=int(s)
     if(s.le.g) out=int(s)+1
  else
     out=int(s)+1
  end if
  
end function ltc


subroutine elm2ltc_check(nnode,x,nelm,elm2nd)
  use globals
  implicit none

  integer :: nnode,nelm,elm2nd(2,nelm),i,j
  real(8) :: x(2,nnode),xtmp(2,nnode)

  do i=1,nnode
     xtmp(1,i)=x(1,i)
     xtmp(2,i)=x(2,i)
  end do  

  open(1,file='elm2ltc_check')
  do i=1,nelm
     do j=1,e2l(i)%n
        write(1,*) e2l(i)%ltc(1,j)  ,e2l(i)%ltc(2,j)
        write(1,*) e2l(i)%ltc(1,j)-1,e2l(i)%ltc(2,j)
        write(1,*)
        write(1,*) e2l(i)%ltc(1,j)-1,e2l(i)%ltc(2,j)           
        write(1,*) e2l(i)%ltc(1,j)-1,e2l(i)%ltc(2,j)-1
        write(1,*)
        write(1,*) e2l(i)%ltc(1,j)-1,e2l(i)%ltc(2,j)-1           
        write(1,*) e2l(i)%ltc(1,j)  ,e2l(i)%ltc(2,j)-1
        write(1,*)
        write(1,*) e2l(i)%ltc(1,j)  ,e2l(i)%ltc(2,j)-1
        write(1,*) e2l(i)%ltc(1,j)  ,e2l(i)%ltc(2,j)
        write(1,*)
     end do
     write(1,*) xtmp(:,elm2nd(1,i))
     write(1,*) xtmp(:,elm2nd(2,i))
     write(1,*)
  end do
  close(1)

end subroutine elm2ltc_check

subroutine ltc2elm_check(nnode,x,nelm,elm2nd,l2etmp)
  use globals
  implicit none

  integer :: nnode,nelm,elm2nd(2,nelm),i,ix,iy
  integer :: ielm
  real(8) :: x(2,nnode),xtmp(2,nnode)
  type(ltc2elm) :: l2etmp(0:xe,0:ye)

  ! 格子番号=格子点座標になるようにスケーリング
  do i=1,nnode
     xtmp(1,i)=x(1,i)*xe/l1
     xtmp(2,i)=x(2,i)*ye/l2
  end do    

  open(1,file='ltc2elm_check')
  open(2,file='ltc_nelm')
  do ix=1,xe
     do iy=1,ye
        if(l2etmp(ix,iy)%n.ne.0) then
           do i=1,l2etmp(ix,iy)%n
              ielm=l2etmp(ix,iy)%elm(i)
              write(1,*) xtmp(:,elm2nd(1,ielm))
              write(1,*) xtmp(:,elm2nd(2,ielm))
              write(1,*)
              if(l2etmp(ix,iy)%edge(0,i).eq.1) write(1,*) ix-0.5d0,iy-1
              if(l2etmp(ix,iy)%edge(1,i).eq.1) write(1,*) ix,iy-0.5d0
              if(l2etmp(ix,iy)%edge(2,i).eq.1) write(1,*) ix-0.5d0,iy
              if(l2etmp(ix,iy)%edge(3,i).eq.1) write(1,*) ix-1,iy-0.5d0
              write(1,*)
           end do
           write(1,*) ix,iy
           write(1,*) ix-1,iy
           write(1,*)
           write(1,*) ix-1,iy
           write(1,*) ix-1,iy-1
           write(1,*)
           write(1,*) ix-1,iy-1
           write(1,*) ix,iy-1
           write(1,*)
           write(1,*) ix,iy-1
           write(1,*) ix,iy
           write(1,*)
        end if
     end do
  end do
  close(1)
  close(2)
  
end subroutine ltc2elm_check


function cross(x1,x2,x3,x4) result(res)
  implicit none
  real(8)::x1(2),x2(2),x3(2),x4(2)
  real(8)::p(2),q(2),pl,ql,pt(2),qt(2),det,u,v,op,vec1(2),vec2(2)
  integer::res

  p(:)=x1(:)
  pt(:)=x2(:)-x1(:)
  pl=sqrt(dot_product(pt,pt))
  pt(:)=pt(:)/pl
  
  q(:)=x3(:)
  qt(:)=x4(:)-x3(:)
  ql=sqrt(dot_product(qt,qt))
  qt(:)=qt(:)/ql

  res=3
  det=pl*ql*(pt(2)*qt(1)-pt(1)*qt(2))
  if(abs(det).lt.1.d-6) then   !2直線が平行
     vec1(:)=pt(:)
     vec2(:)=q(:)-p(:)
     op=abs(vec1(1)*vec2(2)-vec1(2)*vec2(1))
     if(op.lt.1.d-6) then       !2直線が同一直線上
        if(abs(pt(1)).lt.1.d-6) then
           u=(q(2)-p(2))/(pl*pt(2))
           v=(q(2)-p(2)+ql*qt(2))/(pl*pt(2))
        else
           u=(q(1)-p(1))/(pl*pt(1))
           v=(q(1)-p(1)+ql*qt(1))/(pl*pt(1))
        end if
        if((u.ge.0.d0).and.(u.le.1.d0).or.&
             (v.ge.0.d0).and.(v.le.1.d0)) then
           res=1
        else
           res=0
        end if
     else                       !2直線が別の直線上
        res=0
     end if
  else                          !2直線が交点を持つ
     u=ql*(qt(1)*(q(2)-p(2))-qt(2)*(q(1)-p(1)))/det
     v=pl*(pt(1)*(q(2)-p(2))-pt(2)*(q(1)-p(1)))/det
     if(((u+1.d-6).ge.0.d0).and.((u-1.d-6).le.1.d0).and.&
          ((v+1.d-6).ge.0.d0).and.((v-1.d-6).le.1.d0)) then
        res=1
     else
        res=0
     end if
  end if
  
end function cross

! subroutine update_ltcelm
!   use globals
!   implicit none
  
  
! end subroutine update_ltcelm
