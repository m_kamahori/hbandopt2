subroutine near_lattice(ix,iy,i,j,x1,x2,v1,v2,start,goal,outdir,ltc,ltcl,connect,evnum,ibril,&
     dt1,dt2,selm,gelm,vreg,res)
  use globals
  use io
  implicit none
  integer :: ix,iy,i,j,k,start,goal,igrid,outdir,iltc,jltc,inbr,jnbr,itmp
  integer :: mx(0:3),my(0:3),idx(0:6),selm,gelm,nelm,ielm
  integer :: connect(2,edges(1))
  real(8) :: v(2),vreg,x1(2),x2(2),v1(2),v2(2),at1(2),at2(2)
  real(8) :: x11(2),x12(2),x21(2),x22(2),au,ltcl
  real(8) :: w1(2),w2(2),lw1,lw2,tmp,norm,co,tmp1(2),tmp2(2)
  real(8) :: tt1(2,2),tt2(2,2),dt1(2),dt2(2),vt1(2),vt2(2)
  type(lattice) :: ltc(2,2)
  integer,allocatable :: conelm(:)
  integer,intent(in)::evnum,ibril
  real(8) :: a1,a2,a3,c1,c2,vec(2),vnorm,f1,f2,f3,f4
  real(8) :: elmv1(2),elmv2(2),sdtmp,res
  ! complex(8) ufwd(2*el%ne),uadj(2*el%ne)

  idx=(/ 0, 1, 2, 3, 0, 1, 2/)
  mx=(/ 0, 1, 0, -1/)
  my=(/ -1, 0, 1, 0/)
  iltc=i-ix+1
  jltc=j-iy+1

  tt1(:,:)=0.d0
  tt2(:,:)=0.d0
  itmp=0

  igrid=i-1+(xe+1)*(j-1)+1
  
  x1(1)=x1(1)+xlsf(1,igrid) !第１節点グローバル座標
  x1(2)=x1(2)+xlsf(2,igrid) !第１節点グローバル座標
  x2(1)=x2(1)+xlsf(1,igrid) !第２節点グローバル座標
  x2(2)=x2(2)+xlsf(2,igrid) !第２節点グローバル座標
  ! x1(1)=x1(1)+grid(1,igrid) !第１節点グローバル座標
  ! x1(2)=x1(2)+grid(2,igrid) !第１節点グローバル座標
  ! x2(1)=x2(1)+grid(1,igrid) !第２節点グローバル座標
  ! x2(2)=x2(2)+grid(2,igrid) !第２節点グローバル座標

  ! 接線ベクトル
  at1(:)=orgat(:,ltc2nd(start,i,j)) ! 始点における接線ベクトル(lsf2elm.f90で計算)
  at2(:)=orgat(:,ltc2nd(goal ,i,j)) ! 終点における接線ベクトル(lsf2elm.f90で計算)

  ! 第１節点における接線ベクトルの微分計算
  x11(:)=nd4at(1:2,ltc2nd(start,i,j))
  x12(:)=nd4at(3:4,ltc2nd(start,i,j))
  w1(:)=x12(:)-x11(:)
  lw1=sqrt(dot_product(w1,w1)) ! (第１節点の接線ベクトルはw1(:)/lw1で与えられる)
  tmp=1.d0/lw1**3.d0
  if(outdir.eq.1) then      !x11動かない、x12動く
     tt1(1,1)= w1(2)**2.d0*tmp
     tt1(1,2)=-w1(1)*w1(2)*tmp
     tt1(2,1)=tt1(1,2)
     tt1(2,2)= w1(1)**2.d0*tmp
     vt1(:)=v2(:)
  else if(outdir.eq.2) then !x11不明、x12動かない
     inbr=iltc+mx(start)
     jnbr=jltc+my(start)
     if(ltc(inbr,jnbr)%goal(1).eq.idx(start+2)) then
        itmp=1
     else if(ltc(inbr,jnbr)%goal(2).eq.idx(start+2)) then
        itmp=2
     end if
     if(ltc(inbr,jnbr)%outdir(itmp).eq.0) then !x11動く、x12動かない
        tt1(1,1)=-w1(2)**2.d0*tmp !符号変わっただけ
        tt1(1,2)= w1(1)*w1(2)*tmp
        tt1(2,1)=tt1(1,2)
        tt1(2,2)=-w1(1)**2.d0*tmp
        vt1(:)=ltc(iltc+mx(start),jltc+my(start))%v1(:,itmp)
     else if(ltc(inbr,jnbr)%outdir(itmp).eq.1) then !x11動かない、x12動かない
        tt1(:,:)=0.d0
        vt1(:)=0.d0
     end if
  else if(outdir.eq.0) then !x12動く、x11不明
     inbr=iltc+mx(start)
     jnbr=jltc+my(start)
     if(ltc(inbr,jnbr)%goal(1).eq.idx(start+2)) then
        itmp=1
     else if(ltc(inbr,jnbr)%goal(2).eq.idx(start+2)) then
        itmp=2
     end if
     if(ltc(inbr,jnbr)%outdir(itmp).eq.0) then !x11動く、x12動く
        tt1(:,:)=0.d0
        vt1(:)=0.d0
     else if(ltc(inbr,jnbr)%outdir(itmp).eq.1) then !x11動かない、x12動く
        tt1(1,1)= w1(2)**2.d0*tmp
        tt1(1,2)=-w1(1)*w1(2)*tmp
        tt1(2,1)=tt1(1,2)
        tt1(2,2)= w1(1)**2.d0*tmp
        vt1(:)=v2(:)
     end if
  end if
  dt1(1)=tt1(1,1)*vt1(1)+tt1(1,2)*vt1(2)
  dt1(2)=tt1(2,1)*vt1(1)+tt1(2,2)*vt1(2)
  
  ! 第２節点における接線ベクトルの微分計算
  x21(:)=nd4at(1:2,ltc2nd(goal ,i,j))
  x22(:)=nd4at(3:4,ltc2nd(goal ,i,j))
  w2(:)=x22(:)-x21(:)
  lw2=sqrt(dot_product(w2,w2)) ! (第２節点の接線ベクトルはw2(:)/lw2で与えられる)
  tmp=1.d0/lw2**3.d0
  if(outdir.eq.2) then      !x21動く、x22動かない
     tt2(1,1)=-w2(2)**2.d0*tmp
     tt2(1,2)= w2(1)*w2(2)*tmp
     tt2(2,1)=tt2(1,2)
     tt2(2,2)=-w2(1)**2.d0*tmp
     vt2(:)=v1(:)
  else if(outdir.eq.1) then !x21動かない、x22不明
     inbr=iltc+mx(goal)
     jnbr=jltc+my(goal)
     if(ltc(inbr,jnbr)%start(1).eq.idx(goal+2)) then
        itmp=1
     else if(ltc(inbr,jnbr)%start(2).eq.idx(goal+2)) then
        itmp=2
     end if
     if(ltc(inbr,jnbr)%outdir(itmp).eq.0) then !x21動かない、x22動く
        tt2(1,1)= w2(2)**2.d0*tmp
        tt2(1,2)=-w2(1)*w2(2)*tmp
        tt2(2,1)=tt2(1,2)
        tt2(2,2)= w2(1)**2.d0*tmp
        vt2(:)=ltc(inbr,jnbr)%v2(:,itmp)
     else if(ltc(inbr,jnbr)%outdir(itmp).eq.2) then !x21動かない、x22動かない
        tt2(:,:)=0.d0
        vt2(:)=0.d0
     end if
  else if(outdir.eq.0) then !x21動く、x22不明
     inbr=iltc+mx(goal)
     jnbr=jltc+my(goal)
     if(ltc(inbr,jnbr)%start(1).eq.idx(goal+2)) then
        itmp=1
     else if(ltc(inbr,jnbr)%start(2).eq.idx(goal+2)) then
        itmp=2
     end if
     if(ltc(inbr,jnbr)%outdir(itmp).eq.0) then !x21動く、x22動く
        tt2(:,:)=0.d0
        vt2(:)=0.d0
     else if(ltc(inbr,jnbr)%outdir(itmp).eq.2) then !x21動く、x22動かない
        tt2(1,1)=-w2(2)**2.d0*tmp
        tt2(1,2)= w2(1)*w2(2)*tmp
        tt2(2,1)=tt2(1,2)
        tt2(2,2)=-w2(1)**2.d0*tmp
        vt2(:)=v1(:)
     end if
  end if
  dt2(1)=tt2(1,1)*vt2(1)+tt2(1,2)*vt2(2)
  dt2(2)=tt2(2,1)*vt2(1)+tt2(2,2)*vt2(2)
  
  ! theta計算
  tmp1(:)=x2(:)-x1(:)
  norm=sqrt(dot_product(tmp1,tmp1))
  tmp1(:)=tmp1(:)/norm
  tmp2(:)=v2(:)-v1(:)
  norm=sqrt(dot_product(tmp2,tmp2))
  tmp2(:)=tmp2(:)/norm
  co=dot_product(tmp1,tmp2)

  !始点をさがす
  selm=-1
  do k=1,l2e(i,j)%n !格子内の境界要素ループ
     !境界要素がstart辺と交差するか
     if(l2e(i,j)%edge(start,k).eq.1) selm=k
  end do
  if(selm.eq.-1) return         !格子内の要素は微小要素として削除されてるので存在しない

  !繋がった要素の数を数える
  nelm=1
  ielm=selm
1111 continue
  do k=1,l2e(i,j)%n
     if(l2e(i,j)%elm(k).eq.connect(2,l2e(i,j)%elm(ielm))) then
        nelm=nelm+1
        ielm=k
        if(l2e(i,j)%edge(goal,k).eq.1) then
           gelm=k
           goto 2222
        else
           goto 1111
        end if
     end if
  end do
2222 continue
  if(nelm.eq.1) then
     gelm=selm
  end if

  ! 繋がった要素を並べる
  allocate(conelm(nelm))
  nelm=1
  ielm=selm
  conelm(nelm)=ielm
3333 continue
  do k=1,l2e(i,j)%n
     if(l2e(i,j)%elm(k).eq.connect(2,l2e(i,j)%elm(ielm))) then
        nelm=nelm+1
        ielm=k
        conelm(nelm)=ielm
        if(l2e(i,j)%edge(goal,k).eq.1) then
           goto 4444
        else
           goto 3333
        end if
     end if
  end do
4444 continue

  ! (i,j)格子内の要素ループ
  vnorm=sqrt(dot_product(v2-v1,v2-v1))
  vec(:)=x2(:)-x1(:)
  norm=sqrt(dot_product(vec,vec))
  vec(:)=vec(:)/norm !始点-->終点
  c1=dot_product(at1,vec)
  c2=dot_product(at2,vec)
  a1=c1
  a2=-2.d0*c1-c2+3.d0
  a3=c1+c2-2.d0
  do k=1,nelm
     ielm=l2e(i,j)%elm(conelm(k))

     ! 要素始点の速度ベクトル計算
     au=l2e(i,j)%au(1,conelm(k))

     if((au.ge.0.d0).and.(au.le.1.d0)) then
        f1=2.d0*au**3.d0-3.d0*au**2.d0+1.d0
        f2=-2.d0*au**3.d0+3.d0*au**2.d0
        f3=au**3.d0-2.d0*au**2.d0+au
        f4=au**3.d0-au**2.d0

        elmv1(:)=f1*v1(:)+f2*v2(:)+norm*f3*dt1(:)+norm*f4*dt2(:)+&
             co*vnorm*(f3*at1(:)+f4*at2(:))
     else
        elmv1(:)=0.d0
     end if

     ! 要素終点の速度ベクトル計算
     au=l2e(i,j)%au(2,conelm(k))
     
     if((au.ge.0.d0).and.(au.le.1.d0)) then
        f1=2.d0*au**3.d0-3.d0*au**2.d0+1.d0
        f2=-2.d0*au**3.d0+3.d0*au**2.d0
        f3=au**3.d0-2.d0*au**2.d0+au
        f4=au**3.d0-au**2.d0

        elmv2(:)=f1*v1(:)+f2*v2(:)+norm*f3*dt1(:)+norm*f4*dt2(:)+&
             co*vnorm*(f3*at1(:)+f4*at2(:))
     else
        elmv2(:)=0.d0
     end if

     v(:)=(elmv1(:)+elmv2(:))*0.5d0*lngth(ielm)
     ! if(ix.eq.4.and.iy.eq.4)write(111,fmt_d4) c(:,ielm),v(:)
     sdtmp=dot_product(v(:),an(:,ielm))
     vreg=vreg+abs(sdtmp)/ltcl
     res=res+sdtmp*(real(ut(ielm,evnum,ibril))**2+aimag(ut(ielm,evnum,ibril))**2&    
            +real(un(ielm,evnum,ibril))**2+aimag(un(ielm,evnum,ibril))**2&          
            -real(ev_slc(evnum,ibril))**2*(real(u(ielm,evnum,ibril))**2+aimag(u(ielm,evnum,ibril))**2))*ltcl
     ! ltclはv1とv2にかかっている
     ! res=res+sdtmp*real(ufwd(ielm)*uadj(ielm))*ltcl
  end do
  deallocate(conelm)
  
end subroutine near_lattice
