subroutine build_hmat_ibril(ibril,idomp,hdmat,hemat)
  use class_hbem
  implicit none

  integer,intent(in)::ibril,idomp
  type(c_ptr),intent(out)::hdmat,hemat
  type(c_ptr)::hdmat_tmp(4)
  type(c_ptr)::hemat_tmp(16)
  integer::i,j

  ! -----------------hdmat---------------------------------
  do j=1,4
     hdmat_tmp(j)=clone_hmatrix(hdmat_i(j  ,idomp))
     call add_hmatrix(phase(mod(j,2)+1,ibril),hdmat_d(j  ,idomp),c_null_ptr,tol,hdmat_tmp(j))
  end do
  hdmat = integrate_hmatrix2(irep_cluster,peri_cluster,4,hdmat_tmp)
  ! -----------------hdmat---------------------------------

  ! -----------------hemat---------------------------------
  do j=1,4
     do i=1,4
        hemat_tmp(i+4*(j-1))=clone_hmatrix(hemat_i(i+4*(j-1),idomp))
        call add_hmatrix(phase(mod(j,2)+1,ibril),hemat_d(i+4*(j-1),idomp),c_null_ptr,tol,hemat_tmp(i+4*(j-1)))
     end do
  end do
  hemat = integrate_hmatrix2(peri_cluster,peri_cluster,16,hemat_tmp)
  ! -----------------hemat---------------------------------

  ! call coarsen_hmat(hdmat, c_null_ptr, tol)
  ! call coarsen_hmat(hemat, c_null_ptr, tol)

end subroutine build_hmat_ibril

subroutine bem_matrix_lu(ibril,idomp)
  use globals
  implicit none
  integer,intent(in)::ibril,idomp
  integer::j

  amat(:,1:edges(1),idomp)=dmat(:,1:edges(1),idomp)
  
  do j=edges(1)+1,ne 
     if(iperi(1,j).gt.0)then !dependent
        amat(:,j,idomp)=exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))*dmat(:,j,idomp)&
             +dmat(:,iperi(2,j),idomp)
     elseif(iperi(1,j).lt.0)then !independent
        amat(:,j,idomp)=exp(ione*dot_product(wv(:,ibril),avec(:,-iperi(1,j))))*smat(:,iperi(2,j),idomp)&
             -smat(:,j,idomp)
     end if
  end do

end subroutine bem_matrix_lu

subroutine bem_matrix(subd,sube,ibril,s,d)
  use globals
  implicit none
  integer,intent(in)::ibril
  complex(8),intent(out)::subd(:,:),sube(:,:)
  complex(8),intent(in)::s(:,:),d(:,:)
  integer::j,offset

  offset=edges(1)
  do j=offset+1,ne 
     if(iperi(1,j).gt.0)then !dependent
        subd(:,j-offset)=exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))*d(1:offset,j)&
             +d(1:offset,iperi(2,j))
        sube(:,j-offset)=exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))*d(offset+1:ne,j)&
             +d(offset+1:ne,iperi(2,j))
     elseif(iperi(1,j).lt.0)then !independent
        subd(:,j-offset)=exp(ione*dot_product(wv(:,ibril),avec(:,-iperi(1,j))))*s(1:offset,iperi(2,j)-offset)&
             -s(1:offset,j-offset)
        sube(:,j-offset)=exp(ione*dot_product(wv(:,ibril),avec(:,-iperi(1,j))))*s(offset+1:ne,iperi(2,j)-offset)&
             -s(offset+1:ne,j-offset)
     end if
  end do

end subroutine bem_matrix

