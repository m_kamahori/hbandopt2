subroutine random_matrix(ld,rh,rmat)
  implicit none
  real(8),allocatable::ransu(:)
  integer,intent(in)::ld,rh
  complex(8),intent(out)::rmat(ld,rh)
  integer::seedsize,i,j,k
  integer,allocatable::seed(:)
  integer::zikoku

  allocate(ransu(2*ld*rh))
  call system_clock(count=zikoku)
  call random_seed(size=seedsize)
  allocate(seed(seedsize))
  seed=zikoku
  call random_seed(put=seed)
  call random_number(ransu)
  ! open(90,file="rvector.dat")
  k=1
  do j=1,rh
     do i=1,ld
        ! read(90,*) rmat(i,j) !random vector を固定
        rmat(i,j)=cmplx(ransu(k),ransu(k+1),kind=8)
        ! write(90,*) rmat(i,j)
        k=k+2
     end do
  end do
  ! close(90)
  deallocate(ransu,seed)


end subroutine random_matrix
