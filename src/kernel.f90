subroutine kernel(rows,cols,ridx,cidx,drct,bflag,pflag,dflag,wn)
  use globals
  implicit none

  integer,intent(in)::rows,cols,ridx(rows),cidx(cols),bflag,pflag,dflag
  complex(8),intent(in)::wn
  complex(8),intent(out)::drct(rows,cols)

  integer::ii,jj,i,j,nj1,nj2,ig
  ! integer::idomp
  real(8)::x1,x2,a,r,doty
  complex(8)::stmp,dtmp,zcy,z
  complex(8)::CBJ0,CBJ1,CBY0,CBY1

  ! idomp=omp_get_thread_num()
  drct(:,:)=zero
  ! write(*,*) "bflag=",bflag
  ! write(*,*) "pflag=",pflag
  ! write(*,*) "dflag=",dflag
  ! write(*,*) "ridt=",ridt(bflag)
  ! write(*,*) "cidt=",cidt(bflag)
  ! ii,jj:local節点番号
  ! i,j:global節点番号
  if(pflag.eq.2)then     !double layler
     do jj=1,cols               !source
        do ii=1,rows                  !observe
           i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
           j=cidx(jj)+cidt(bflag)+1
           if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
           nj1=nd(1,j)
           nj2=nd(2,j)
           x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
           x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
           a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
           dtmp=zero
           do ig=1,ng
              r=sqrt((x1-a*gzai(ig))**2+x2**2)
              doty=-x2
              z=wn*r
              call CJY1_R(Z,CBJ1,CBY1)
              zcy=CBJ1+ione*CBY1
              dtmp=dtmp+wei(ig)*a*(-wn*zcy)*doty/r
           end do
           if(i.eq.j)dtmp=cmplx(0.d0,-2.d0,kind=8)
           drct(ii,jj)=dtmp
           ! drct(ii,jj)=dtmp*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))
           ! if(i.eq.j)drct(ii,jj)=cmplx(0.d0,-2.d0)
        enddo
     enddo
  else                    !single layer
     if(dflag.eq.1)then   !independent
        do jj=1,cols               !source
           do ii=1,rows                  !observe
              i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
              j=cidx(jj)+cidt(bflag)+1
              if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
              nj1=nd(1,j)
              nj2=nd(2,j)
              x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
              x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
              a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
              stmp=zero
              do ig=1,ng
                 r=sqrt((x1-a*gzai(ig))**2+x2**2)
                 z=wn*r               !wnは現在load.f90で定義している
                 call CJY0_R(Z,CBJ0,CBY0)
                 zcy=CBJ0+ione*CBY0
                 stmp=stmp+wei(ig)*a*zcy
              end do
              drct(ii,jj)=-stmp
           enddo
        enddo
     else                 !dependent
        do jj=1,cols               !source
           do ii=1,rows                  !observe
              i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
              j=cidx(jj)+cidt(bflag)+1
              if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
              nj1=nd(1,j)
              nj2=nd(2,j)
              x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
              x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
              a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
              stmp=zero
              do ig=1,ng
                 r=sqrt((x1-a*gzai(ig))**2+x2**2)
                 z=wn*r               !wnは現在load.f90で定義している
                 call CJY0_R(Z,CBJ0,CBY0)
                 zcy=CBJ0+ione*CBY0
                 stmp=stmp+wei(ig)*a*zcy
              end do
              drct(ii,jj)=stmp
              ! drct(ii,jj)=stmp*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))
           enddo
        enddo
     endif
  end if

  drct(:,:)=drct(:,:)*0.25d0*ione

  ! do ii=1,rows
  !    do jj=1,cols
  !       i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
  !       j=cidx(jj)+cidt(bflag)+1
  !       if(abs(drct(ii,jj)-dmat(i,j,idomp)).gt.1.d-16)then
  !          write(*,*)drct(ii,jj),dmat(i,j,idomp)
  !          stop "kernel"
  !       end if
  !    end do
  ! end do


end subroutine kernel

! subroutine kernel2(rows,cols,ridx,cidx,sm,dm,bflag,pflag,dflag,wn)
!   use globals
!   implicit none

!   integer,intent(in)::rows,cols,ridx(rows),cidx(cols),bflag,pflag,dflag
!   complex(8),intent(in)::wn
!   complex(8),intent(out)::sm(rows,cols),dm(rows,cols)

!   integer::ii,jj,i,j,nj1,nj2,ig
!   ! integer::idomp
!   real(8)::x1,x2,a,r,doty
!   complex(8)::stmp,dtmp,zcy,z
!   complex(8)::CBJ0,CBJ1,CBY0,CBY1

!   ! idomp=omp_get_thread_num()
!   sm=zero
!   dm=zero
!   ! write(*,*) "bflag=",bflag
!   ! write(*,*) "pflag=",pflag
!   ! write(*,*) "dflag=",dflag
!   ! write(*,*) "ridt=",ridt(bflag)
!   ! write(*,*) "cidt=",cidt(bflag)
!   ! ii,jj:local節点番号
!   ! i,j:global節点番号
!      do jj=1,cols               !source
!         do ii=1,rows                  !observe
!            i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
!            j=cidx(jj)+cidt(bflag)+1
!            if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
!            nj1=nd(1,j)
!            nj2=nd(2,j)
!            x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
!            x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
!            a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
!            dtmp=zero
!            do ig=1,ng
!               r=sqrt((x1-a*gzai(ig))**2+x2**2)
!               doty=-x2
!               z=wn*r
!               call CJY1_R(Z,CBJ1,CBY1)
!               zcy=CBJ1+ione*CBY1
!               dtmp=dtmp+wei(ig)*a*(-wn*zcy)*doty/r
!            end do
!            if(i.eq.j)dtmp=cmplx(0.d0,-2.d0,kind=8)
!            drct(ii,jj)=dtmp
!            ! drct(ii,jj)=dtmp*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))
!            ! if(i.eq.j)drct(ii,jj)=cmplx(0.d0,-2.d0)
!         enddo
!      enddo


  
!   if(pflag.eq.2)then     !double layler
!      do jj=1,cols               !source
!         do ii=1,rows                  !observe
!            i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
!            j=cidx(jj)+cidt(bflag)+1
!            if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
!            nj1=nd(1,j)
!            nj2=nd(2,j)
!            x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
!            x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
!            a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
!            dtmp=zero
!            do ig=1,ng
!               r=sqrt((x1-a*gzai(ig))**2+x2**2)
!               doty=-x2
!               z=wn*r
!               call CJY1_R(Z,CBJ1,CBY1)
!               zcy=CBJ1+ione*CBY1
!               dtmp=dtmp+wei(ig)*a*(-wn*zcy)*doty/r
!            end do
!            if(i.eq.j)dtmp=cmplx(0.d0,-2.d0,kind=8)
!            drct(ii,jj)=dtmp
!            ! drct(ii,jj)=dtmp*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))
!            ! if(i.eq.j)drct(ii,jj)=cmplx(0.d0,-2.d0)
!         enddo
!      enddo
!   else                    !single layer
!      if(dflag.eq.1)then   !independent
!         do jj=1,cols               !source
!            do ii=1,rows                  !observe
!               i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
!               j=cidx(jj)+cidt(bflag)+1
!               if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
!               nj1=nd(1,j)
!               nj2=nd(2,j)
!               x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
!               x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
!               a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
!               stmp=zero
!               do ig=1,ng
!                  r=sqrt((x1-a*gzai(ig))**2+x2**2)
!                  z=wn*r               !wnは現在load.f90で定義している
!                  call CJY0_R(Z,CBJ0,CBY0)
!                  zcy=CBJ0+ione*CBY0
!                  stmp=stmp+wei(ig)*a*zcy
!               end do
!               drct(ii,jj)=-stmp
!            enddo
!         enddo
!      else                 !dependent
!         do jj=1,cols               !source
!            do ii=1,rows                  !observe
!               i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
!               j=cidx(jj)+cidt(bflag)+1
!               if(pflag*dflag.eq.2.and.bflag.gt.5) j=iperi(2,j) !ソース点を反対側の境界要素に設定
!               nj1=nd(1,j)
!               nj2=nd(2,j)
!               x1=(c(1,i)-c(1,j))*an(2,j)-(c(2,i)-c(2,j))*an(1,j) !垂線の足から要素中心までの距離(符号付き)
!               x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
!               a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
!               stmp=zero
!               do ig=1,ng
!                  r=sqrt((x1-a*gzai(ig))**2+x2**2)
!                  z=wn*r               !wnは現在load.f90で定義している
!                  call CJY0_R(Z,CBJ0,CBY0)
!                  zcy=CBJ0+ione*CBY0
!                  stmp=stmp+wei(ig)*a*zcy
!               end do
!               drct(ii,jj)=stmp
!               ! drct(ii,jj)=stmp*exp(ione*dot_product(wv(:,ibril),avec(:,iperi(1,j))))
!            enddo
!         enddo
!      endif
!   end if

!   drct(:,:)=drct(:,:)*0.25d0*ione

!   ! do ii=1,rows
!   !    do jj=1,cols
!   !       i=ridx(ii)+ridt(bflag)+1 !各cluster内のlocal節点番号+global節点番号への変換項
!   !       j=cidx(jj)+cidt(bflag)+1
!   !       if(abs(drct(ii,jj)-dmat(i,j,idomp)).gt.1.d-16)then
!   !          write(*,*)drct(ii,jj),dmat(i,j,idomp)
!   !          stop "kernel"
!   !       end if
!   !    end do
!   ! end do


! end subroutine kernel2
