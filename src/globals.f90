module globals
  use iso_c_binding
  implicit none
  real(8),parameter::pi=acos(-1.d0)           !=円周率
  complex(8),parameter::ione=cmplx(0.d0,1.d0,kind=8) !=虚数単位
  complex(8),parameter::zero=cmplx(0.d0,0.d0,kind=8) !=零
  complex(8),parameter::one =cmplx(1.d0,0.d0,kind=8) !=1
  logical(c_bool),parameter::true=.true.,false=.false.

  character(len=32)::meshfile !BEMのメッシュファイル
  character(len=32)::naitenfile !BEMのnaitenファイル

  integer::np !節点数
  integer::ne !要素数 (二次元問題ではnpに等しい)
  integer::edges(5)             !edges(i):i番境界要素数
  integer::nip !naiten no kazu
  integer,allocatable,target::nd(:,:) !要素コネクティビティ
  integer,allocatable::ibc(:)
  integer,allocatable::ipiv_b(:,:)
  real(8),allocatable,target::p(:,:)  !p(i,j):j番節点のx_i座標
  real(8),allocatable::c(:,:)         !c(i,j):j番要素のx_i座標
  real(8),allocatable::at(:,:) !at(i,j):j番要素の単位接線ベクトルのx_i成分
  real(8),allocatable,target::an(:,:) !an(i,j):j番要素の単位法線ベクトルのx_i成分
  real(8),allocatable::grid(:,:)      !naiten
  real(8),allocatable,target::lngth(:)    !lngth(j): j番要素の長さ
  complex(8),allocatable::u(:,:,:)     !eigenvector
  complex(8),allocatable::un(:,:,:)   !eigenvector
  complex(8),allocatable::ut(:,:,:)   !eigenvector
  complex(8),allocatable::ugrid(:,:,:)    !naiten
  complex(8),allocatable::dugrid(:,:,:,:) !naiten
  complex(8),allocatable::smat(:,:,:) !precomputed single layer
  complex(8),allocatable::dmat(:,:,:) !precomputed double layer
  complex(8),allocatable::subb(:,:,:),subc(:,:,:),cbinv(:,:,:),amat(:,:,:)

  integer,allocatable::iperi(:,:)
  real(8),allocatable::wv(:,:) !wave vector
  complex(8),allocatable::phase(:,:)
  complex(8)::wn_g             !grobal wave number
  real(8),parameter::br=0.7d0        !switch islv by this parameter
  
  type eigenpairs
     integer::n                 !number of eigenpairs
     ! integer,allocatable::t(:)                 !number of target eigenvalue
     integer,allocatable::flag(:) !flag of target eigenvalue
     complex(8),allocatable::s(:) !eigenvalues
     complex(8),allocatable::stmp(:) !eigenvalues
     complex(8),allocatable::v(:,:) !eigenvectors
     ! complex(8),allocatable::v_slc(:,:) !eigenvectors to be used in select_ev
  end type eigenpairs
  type history
     type(eigenpairs),allocatable::e(:,:)
     ! type(eigenpairs),pointer::e(:,:)
     real(8)::f                   !Full bandgap width
     real(8)::cof                 !center of full bandgap
     integer::flag                !1:f has minimum value
  end type history
  type(history),allocatable::hstry(:)
     

  integer::nskbn     !=積分点の数
  integer::nblck     !=L(ブロックの数)
  integer::nhank     !=Hankel行列のサイズ
  integer::numomp
  integer::mode(2)
  real(8)::init_rad             !積分円の半径の初期値
 
  real(8)::ds         !設計領域で積分する時に使う微小面積
  real(8)::menseki
!!!!!!!!!!! H-matrix !!!!!!!!!!!!!!!!!!
  integer::nmin     !ACAのn_min: clusterに含まれる要素数の最大値
  integer::nminin   !ACAのn_min: 内点clusterに含まれる要素数の最大値
  real(8)::eta      !ACA のadmissible condition の eta(境界)
  real(8)::eta_in   !ACA のadmissible condition の eta(内点)
  real(8)::eps_aca  !eps_aca: ACAの許容誤差		   
  ! real(8)::tol_lu   !tol_lu: LU分解の許容誤差		   
  real(8)::tol   !tol: H行列計算の許容誤差		   
  integer::cflag    !ACA agglomeration: 0:しない 1:する 
  real(8)::ceps     !agglomeration の許容誤差
  real(8)::eps_trunc!truncation の許容誤差
  integer,allocatable::ridt(:)     !各行clusterの先頭のglobal節点番号
  integer,allocatable::cidt(:)     !各列clusterの先頭のglobal節点番号

  type(c_ptr),target :: rootin,irep_cluster,peri_cluster,all_cluster
  type(c_ptr),pointer:: irep_gcluster(:),peri_gcluster(:),all_gcluster(:)

  type,bind(c) :: hmatrix
     type(c_ptr) :: rc
     type(c_ptr) :: cc
     type(c_ptr) :: r
     type(c_ptr) :: f
     type(c_ptr) :: son
     integer(c_int) :: rsons
     integer(c_int) :: csons
     integer(c_int) :: refs
     integer(c_int) :: desc
  end type hmatrix
  type(c_ptr),pointer::hbmat(:),hcmat(:),hcbinvmat(:)
  type(c_ptr),pointer::hdmat_i(:,:),hdmat_d(:,:)
  type(c_ptr),pointer::hemat_i(:,:),hemat_d(:,:)

  type kernels
     complex(8),allocatable :: s(:,:)
     complex(8),allocatable :: d(:,:)
     complex(8),allocatable :: ds(:,:)
     complex(8),allocatable :: dd(:,:)
     complex(8),allocatable :: ds1(:,:)
     complex(8),allocatable :: ds2(:,:)
     complex(8),allocatable :: dd1(:,:)
     complex(8),allocatable :: dd2(:,:)
     integer,allocatable :: flag(:,:)
  end type kernels
  type(kernels),allocatable :: krnl(:)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 
!!$ Gauus求積
  integer,parameter::ng=10
  real(8)::wei(ng),gzai(ng)

 !!$ memory management
  integer::imem=0
  integer,parameter::iii=4,iid=8,iiz=16

  ! !!!!!!!!!!!!!!!!!!kamahori
  integer::xe,ye,nxhole,nyhole,idiv,mflag,nlsf
  integer::step1,step2,ibril1,ibril2
  real(8)::l1,l2,holerad,offset1,offset2,deltat1,deltat2,tloop1,tloop2,prm_c,prm_tau,mrate
  ! real(8),allocatable::td_sum(:)! ,td_ev(:,:,:)
  real(8),allocatable::td(:,:)
  real(8),allocatable::sd(:,:)
  integer,parameter::nb=6       !Brillouin zone の１辺の分割数
  integer::nbril                !Brillouin zone の分割数
  integer::symmetry
  integer::nevmax
  integer::init_ncent
  integer::evslctr !determine ev selecter
  integer::islv    !0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat
  integer::nref    !the number of points at which we calculate MAC
  real(8),allocatable::xref(:,:) !the position of points at which we calculate MAC
  real(8),parameter::deltair=0.5d0
  ! real(8)::deltair
  real(8),parameter::deltalsf=1.d-4
  complex(8),allocatable::ev_slc(:,:) !トポロジー導関数を計算する固有値
  complex(8),allocatable::evv_slc(:,:,:) !トポロジー導関数を計算する固有値に対する固有ベクトル
  complex(8),allocatable::evv_ref(:,:,:) !Mode accuracy criterionの基準固有ベクトル
  real(8)::avec(2,2)              !kosi vector
  real(8)::time(3)            !時間計測用
  real(8)::tins               !time instance
  real(8)::tint               !time interval
  real(8),allocatable::lsfunc(:,:)
  real(8),allocatable::xlsf(:,:)
  real(8)::gvec(2,2)            !逆格子ベクトル

!!!!!!!!!!!!!!for sabun
  integer::nsd
  character(len=128)::file_dir
  

! !!!!!!!!!!!!!!singular
!   integer,parameter::ngl=8
!   real(8)::weil(ngl),gzail(ngl)

  real(8),allocatable::orgat(:,:),nd4at(:,:)
  integer,allocatable::ltc2nd(:,:,:)  
  integer,parameter::gridtype=1 !※原点(0,0)を端にとる→1,中心にとる→2

  type :: ltc2elm
     integer :: n
     integer,allocatable :: elm(:)
     integer,allocatable :: edge(:,:)
     integer,allocatable :: start(:)
     integer,allocatable :: goal(:)
     real(8),allocatable :: au(:,:)
  end type ltc2elm
  type(ltc2elm),allocatable :: l2e(:,:),l2epre(:,:)

  type :: elm2ltc
     integer :: n
     integer,allocatable :: ltc(:,:)
  end type elm2ltc
  type(elm2ltc),allocatable :: e2l(:),e2lpre(:)


  type :: lattice
     integer :: n
     real(8) :: x1(2,2)
     real(8) :: x2(2,2)
     real(8) :: v1(2,2)
     real(8) :: v2(2,2)
     integer :: start(2)
     integer :: goal(2)
     integer :: flag
     integer :: outdir(2)
  end type lattice

  ! mesh
  integer :: ndof(0:2) !ndof(0)=#total dof, ndof(1)=#dof on physical boundary, ndof(2)=#dof on periodic boundary
  type :: boundary_element
     integer :: np ! #nodes
     integer :: ne
     integer :: n(0:4) ! n(i): #elements on Gamma_i
     integer :: nsm(-1:4) ! nsm(i): nsm(-1)=0, nsm(0)=ndof(1), nsm(j)=nsm(j-1)+n(j) for j=1..4
     real(8), allocatable :: x(:,:) ! x(j,i): j-th comp. of coordinate of i-th nodal point
     integer, allocatable :: nd(:,:) ! nd(j,i,k): global nodal number for j-th node of i-th elem on the k-th boundary.
     real(8), allocatable :: an(:,:)! an(j,i): j-th comp. of normal vec. on i-th elem.
     real(8), allocatable :: at(:,:)! at(j,i): j-th comp. of tangent vec. on i-th elem.
     real(8), allocatable :: le(:)  ! le(j): length of j-th elem.
     real(8), allocatable :: c(:,:) ! c(j,i): j-th comp. of coordinate of collocation point of i-th elem.
     integer, allocatable :: iperi(:,:) ! see README.md
  end type boundary_element
  type(boundary_element) :: el

contains
  subroutine gauss
    implicit none
    gzai(1)= 0.9739065285171717d0
    gzai(2)= 0.8650633666889845d0
    gzai(3)= 0.6794095682990244d0
    gzai(4)= 0.4333953941292471d0
    gzai(5)= 0.1488743389816312d0
    gzai(6)=-0.1488743389816312d0
    gzai(7)=-0.4333953941292471d0
    gzai(8)=-0.6794095682990244d0
    gzai(9)=-0.8650633666889845d0
    gzai(10)=-0.9739065285171717d0
    wei(1)= 0.6667134430868813d-01
    wei(2)= 0.1494513491505805d0
    wei(3)= 0.2190863625159820d0
    wei(4)= 0.2692667193099963d0
    wei(5)= 0.2955242247147528d0
    wei(6)= 0.2955242247147528d0
    wei(7)= 0.2692667193099963d0
    wei(8)= 0.2190863625159820d0
    wei(9)= 0.1494513491505805d0
    wei(10)= 0.6667134430868813d-01
  end subroutine gauss
  
  ! subroutine loggauss
  !   implicit none
  !   if(ngl.eq.2)then
  !      gzail(1) = 0.1120088061669761
  !      gzail(2) = 0.6022769081187381
  !      weil(1) = -0.7185393190303845
  !      weil(2) = -0.2814606809696154
  !   elseif(ngl.eq.3)then
  !      gzail(1) = 0.06389079308732544
  !      gzail(2) = 0.3689970637156184
  !      gzail(3) = 0.766880303938942
  !      weil(1) = -0.5134045522323634
  !      weil(2) = -0.3919800412014877
  !      weil(3) = -0.0946154065661483
  !   elseif(ngl.eq.4)then
  !      gzail(1) = 0.04144848019938324
  !      gzail(2) = 0.2452749143206022
  !      gzail(3) = 0.5561654535602751
  !      gzail(4) = 0.848982394532986
  !      weil(1) = -0.3834640681451353
  !      weil(2) = -0.3868753177747627
  !      weil(3) = -0.1904351269501432
  !      weil(4) = -0.03922548712995894
  !   elseif(ngl.eq.5)then
  !      gzail(1) = 0.02913447215197205
  !      gzail(2) = 0.1739772133208974
  !      gzail(3) = 0.4117025202849029
  !      gzail(4) = 0.6773141745828183
  !      gzail(5) = 0.89477136103101
  !      weil(1) = -0.2978934717828955
  !      weil(2) = -0.3497762265132236
  !      weil(3) = -0.234488290044052
  !      weil(4) = -0.0989304595166356
  !      weil(5) = -0.01891155214319462
  !   elseif(ngl.eq.6)then
  !      gzail(1) = 0.02163400584411693
  !      gzail(2) = 0.1295833911549506
  !      gzail(3) = 0.3140204499147661
  !      gzail(4) = 0.5386572173517997
  !      gzail(5) = 0.7569153373774084
  !      gzail(6) = 0.922668851372116
  !      weil(1) = -0.2387636625785478
  !      weil(2) = -0.3082865732739458
  !      weil(3) = -0.2453174265632108
  !      weil(4) = -0.1420087565664786
  !      weil(5) = -0.05545462232488041
  !      weil(6) = -0.01016895869293513
  !   elseif(ngl.eq.7)then
  !      gzail(1) = 0.016719355408258515941667360932
  !      gzail(2) = 0.100185677915675121586885031757
  !      gzail(3) = 0.246294246207930599046668547239
  !      gzail(4) = 0.433463493257033105832882482601
  !      gzail(5) = 0.632350988047766088461805812245
  !      gzail(6) = 0.811118626740105576526226796782
  !      gzail(7) = 0.940848166743347721760134113379
  !      weil(1) = -1.961693894252482075254273775846d-1
  !      weil(2) = -2.703026442472729821452717195335d-1
  !      weil(3) = -2.396818730076909483080727852520d-1
  !      weil(4) = -1.657757748104329065608696877360d-1
  !      weil(5) = -8.894322713765796443572384034578d-2
  !      weil(6) = -3.319430435657106702544941110339d-2
  !      weil(7) = -5.932787015125923999185178444685d-3
  !   elseif(ngl.eq.8)then
  !      gzail(1) = 0.013320244160892465012252672524
  !      gzail(2) = 0.079750429013894938409827729142
  !      gzail(3) = 0.197871029326188053794476159516
  !      gzail(4) = 0.354153994351909419671463603538
  !      gzail(5) = 0.529458575234917277706149699996
  !      gzail(6) = 0.701814529939099963837152670310
  !      gzail(7) = 0.849379320441106676048309202301
  !      gzail(8) = 0.953326450056359788767379678514
  !      weil(1) = -1.644166047280028868314725683259d-1
  !      weil(2) = -2.375256100233060205013485619605d-1
  !      weil(3) = -2.268419844319191263687804029360d-1
  !      weil(4) = -1.757540790060702449880562120059d-1
  !      weil(5) = -1.129240302467590518550004420863d-1
  !      weil(6) = -5.787221071778207239852796729398d-2
  !      weil(7) = -2.097907374213297804346152411499d-2
  !      weil(8) = -3.686407104027619013352321276470d-3
  !   end if

  ! end subroutine loggauss

end module globals
