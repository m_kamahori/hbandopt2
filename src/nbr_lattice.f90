subroutine nbr_lattice(ix,iy,i,j,lsf,start,goal,outdir,ltc,ltcl,connect,evnum,ibril,dt1,dt2,&
     selm,gelm,vreg,res)
  use globals
  implicit none

  integer :: ix,iy,i,j,k,start,goal,igrid,outdir
  integer :: mx(0:3),my(0:3),idx(0:6),selm,gelm,nelm,ielm,ielm0
  integer :: connect(2,edges(1)),sg,inbr,jnbr
  real(8) :: v(2),vreg,x1(2),x2(2),v1(2),v2(2),at1(2),at2(2)
  real(8) :: x11(2),x12(2),x21(2),x22(2),s,au,ltcl
  real(8) :: w1(2),w2(2),lw1,lw2,tmp,theta,norm,co,tmp1(2),tmp2(2)
  real(8) :: tt1(2,2),tt2(2,2),dt1(2),dt2(2),vt1(2),vt2(2),phi(0:3)
  real(8) lsf(0:xe,0:ye)
  type(lattice) :: ltc(2,2)
  integer,allocatable :: conelm(:)
  integer,intent(in)::evnum,ibril
  real(8) :: a0,a1,a2,a3,c1,c2,vec(2),vnorm,x(2),lx(2),f1,f2,f3,f4
  real(8) :: elmv1(2),elmv2(2),sdtmp,res
  ! complex(8) ufwd(2*el%ne),uadj(2*el%ne)

  idx=(/ 0, 1, 2, 3, 0, 1, 2/)
  mx=(/ 0, 1, 0, -1/)
  my=(/ -1, 0, 1, 0/)

  if(outdir.eq.1) then !始点が4格子の外と繋がってる
     ielm0=selm
     sg=start
     goal=idx(start+2)
     start=-1
  else if(outdir.eq.2) then !終点が4格子の外と繋がってる
     ielm0=gelm
     sg=goal
     start=idx(goal+2)
     goal=-1
  else
     return
  end if

  if(sg.eq.0) then !下
     inbr=i
     jnbr=j-1
  end if
  if(sg.eq.1) then !右
     inbr=i+1
     jnbr=j
  end if
  if(sg.eq.2) then !上
     inbr=i
     jnbr=j+1
  end if
  if(sg.eq.3) then !左
     inbr=i-1
     jnbr=j
  end if

  !始点/終点をさがす
  ielm0=-1
  do k=1,l2e(inbr,jnbr)%n
     if(outdir.eq.1) then
        if(l2e(inbr,jnbr)%edge(goal,k).eq.1) ielm0=k
     else if(outdir.eq.2) then
        if(l2e(inbr,jnbr)%edge(start,k).eq.1) ielm0=k
     end if
  end do
  if(ielm0.eq.-1) return        !格子内の要素は微小要素として削除されてるので存在しない

  ! 繋がった要素の数を数える
  nelm=1
  ielm=ielm0
5555 continue
  do k=1,l2e(inbr,jnbr)%n
     if(l2e(inbr,jnbr)%elm(k).eq.connect(outdir,l2e(inbr,jnbr)%elm(ielm))) then
        nelm=nelm+1
        ielm=k
        if(l2e(inbr,jnbr)%edge(0,k).eq.1) then !下
           if(outdir.eq.1) start=0            !始点は下
           if(outdir.eq.2) goal =0            !終点は下
           goto 6666
        else if(l2e(inbr,jnbr)%edge(1,k).eq.1) then !右
           if(outdir.eq.1) start=1
           if(outdir.eq.2) goal =1
           goto 6666                          
        else if(l2e(inbr,jnbr)%edge(2,k).eq.1) then !上
           if(outdir.eq.1) start=2
           if(outdir.eq.2) goal =2
           goto 6666                          
        else if(l2e(inbr,jnbr)%edge(3,k).eq.1) then !左
           if(outdir.eq.1) start=3
           if(outdir.eq.2) goal =3
           goto 6666
        else
           goto 5555
        end if
     end if
  end do
6666 continue
  if(nelm.eq.1) then !始点と終点が同一境界要素の節点
     if(outdir.eq.1) then
        do k=1,3
           if(l2e(inbr,jnbr)%edge(idx(goal+k),1).eq.1) then
              start=idx(goal+k)
           end if
        end do
     else if(outdir.eq.2) then
        do k=1,3
           if(l2e(inbr,jnbr)%edge(idx(start+k),1).eq.1) then
              goal=idx(start+k)
           end if
        end do
     end if
  end if  

  ! 繋がった要素を並べる
  allocate(conelm(nelm))
  nelm=1
  ielm=ielm0
  conelm(nelm)=ielm
7777 continue
  do k=1,l2e(inbr,jnbr)%n
     if(l2e(inbr,jnbr)%elm(k).eq.connect(outdir,l2e(inbr,jnbr)%elm(ielm))) then
        nelm=nelm+1
        ielm=k
        conelm(nelm)=ielm
        if((outdir.eq.1).and.(l2e(inbr,jnbr)%edge(start,k).eq.1)) goto 8888
        if((outdir.eq.2).and.(l2e(inbr,jnbr)%edge(goal ,k).eq.1)) goto 8888
        goto 7777
     end if
  end do
8888 continue

  ! 接線ベクトル
  at1(:)=orgat(:,ltc2nd(start,inbr,jnbr)) ! 始点における接線ベクトル
  at2(:)=orgat(:,ltc2nd(goal ,inbr,jnbr)) ! 終点における接線ベクトル

  ! 始点、終点座標計算
  phi(0)=lsf(inbr-1,jnbr-1)
  phi(1)=lsf(inbr,jnbr-1)
  phi(2)=lsf(inbr,jnbr)
  phi(3)=lsf(inbr-1,jnbr)
  if(start.eq.0) then
     x1(1)=ltcl*phi(0)/(phi(0)-phi(1))
     x1(2)=0.d0
  else if(start.eq.1) then
     x1(1)=ltcl
     x1(2)=ltcl*phi(1)/(phi(1)-phi(2))
  else if(start.eq.2) then
     x1(1)=ltcl*phi(3)/(phi(3)-phi(2))
     x1(2)=ltcl
  else if(start.eq.3) then
     x1(1)=0.d0
     x1(2)=ltcl*phi(0)/(phi(0)-phi(3))
  end if
  if(goal.eq.0) then
     x2(1)=ltcl*phi(0)/(phi(0)-phi(1))
     x2(2)=0.d0
  else if(goal.eq.1) then
     x2(1)=ltcl
     x2(2)=ltcl*phi(1)/(phi(1)-phi(2))
  else if(goal.eq.2) then
     x2(1)=ltcl*phi(3)/(phi(3)-phi(2))
     x2(2)=ltcl
  else if(goal.eq.3) then
     x2(1)=0.d0
     x2(2)=ltcl*phi(0)/(phi(0)-phi(3))
  end if
  igrid=inbr-1+(xe+1)*(jnbr-1)+1
  x1(1)=x1(1)+xlsf(1,igrid) !第１節点グローバル座標
  x1(2)=x1(2)+xlsf(2,igrid) !第１節点グローバル座標
  x2(1)=x2(1)+xlsf(1,igrid) !第２節点グローバル座標
  x2(2)=x2(2)+xlsf(2,igrid) !第２節点グローバル座標
  ! x1(1)=x1(1)+grid(1,igrid) !第１節点グローバル座標
  ! x1(2)=x1(2)+grid(2,igrid) !第１節点グローバル座標
  ! x2(1)=x2(1)+grid(1,igrid) !第２節点グローバル座標
  ! x2(2)=x2(2)+grid(2,igrid) !第２節点グローバル座標  

  ! (i,j)格子内の要素ループ
  vec(:)=x2(:)-x1(:)
  norm=sqrt(dot_product(vec,vec))
  vec(:)=vec(:)/norm !始点-->終点
  c1=dot_product(at1,vec)
  c2=dot_product(at2,vec)
  a1=c1
  a2=-2.d0*c1-c2+3.d0
  a3=c1+c2-2.d0
  do k=1,nelm
     ielm=l2e(inbr,jnbr)%elm(conelm(k))

     ! 要素始点の速度ベクトル計算
     au=l2e(inbr,jnbr)%au(1,conelm(k))
     if((au.ge.0.d0).and.(au.le.1.d0)) then
        if(outdir.eq.1) then
           f1=au**3.d0-au**2.d0
           elmv1(:)=norm*f1*dt1(:)
        else if(outdir.eq.2) then
           f1=au**3.d0-2.d0*au**2.d0+au
           elmv1(:)=norm*f1*dt2(:)
        end if
     else
        elmv1(:)=0.d0
     end if

     ! 要素終点の速度ベクトル計算
     au=l2e(inbr,jnbr)%au(2,conelm(k))
     if((au.ge.0.d0).and.(au.le.1.d0)) then
        if(outdir.eq.1) then
           f1=au**3.d0-au**2.d0
           elmv2(:)=norm*f1*dt1(:)
        else if(outdir.eq.2) then
           f1=au**3.d0-2.d0*au**2.d0+au
           elmv2(:)=norm*f1*dt2(:)
        end if
     else
        elmv2(:)=0.d0
     end if

     v(:)=(elmv1(:)+elmv2(:))*0.5d0*lngth(ielm)
     sdtmp=dot_product(v(:),an(:,ielm))
     vreg=vreg+abs(sdtmp)/ltcl
     res=res+sdtmp*(real(ut(ielm,evnum,ibril))**2+aimag(ut(ielm,evnum,ibril))**2&    
            +real(un(ielm,evnum,ibril))**2+aimag(un(ielm,evnum,ibril))**2&          
            -real(ev_slc(evnum,ibril))**2*(real(u(ielm,evnum,ibril))**2+aimag(u(ielm,evnum,ibril))**2))*ltcl
     ! res=res+sdtmp*real(ufwd(ielm)*uadj(ielm))*ltcl
  end do
  deallocate(conelm)
  
end subroutine nbr_lattice
