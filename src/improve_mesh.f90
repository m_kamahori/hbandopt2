subroutine improve_mesh(nnode,nelement,nodepos,element,eps1,iflag_bounded)
  use globals
  implicit none

  ! real(8),parameter::preset_shortestedge=0.2d0
  real(8),parameter::ps=0.35d0
  real(8),parameter::dFerguson=0.5d0
  real(8)::preset_shortestedge

  integer nnode,nelement,iflag_bounded,flag,nds,ndg
  integer element(2,nelement)
  real(8) nodepos(2,nnode)
  real(8) eps1
  character(len=32) file_elm,file_lsf,file_elm_gp

  real(8),allocatable::elml(:) !elml(i): 要素iの長さ
  integer,allocatable::elm(:,:) !elm(i,j): j番節点のi番要素
  real(8),allocatable::atnd(:,:) !atnd(i,j): j番節点における接線のx_i成分

  integer i,j,k,l,m,k1,k2,ii,jj,ix,iy,ix2,iy2,itmp

  real(8) xcnt(2)

  real(8) au,aupre
  real(8) f1,f2,f3,f4
  real(8) anorm
  real(8) xi,xj,yi,yj

  integer,allocatable::ndiv(:)
  real(8) shortestedge,longestedge

  real(8),allocatable::nodeposnew(:,:)
  integer,allocatable::elementnew(:,:)

  integer :: scaleflag,imax,imin,jmax,jmin
  real(8) :: tnorm,x1(2),x2(2),x3(2),x4(2),res,fergscale,leng,leng0
  real(8),allocatable :: tvec(:,:),tmp(:,:),checkt(:,:)
  integer,allocatable :: connect(:,:),divflag(:),melm(:)

  integer,allocatable :: celm(:,:),elmflag(:)
  real(8),allocatable :: xc(:,:)
  integer :: ntinyelm,is,ig,ielm,iltc,jltc,iiltc(3),jjltc(3),start,goal,nmax
  real(8) :: theta,xs,xg,ys,yg,rtmp,edge(0:3),a0,a1,a2,a3,c1,c2,vec(2),vnorm

  integer :: selm,gelm,elm1,elm2

  type :: element_set
     integer :: n
     integer,allocatable :: elm(:)
     integer,allocatable :: nd(:)
     integer :: selm
     integer :: gelm
     real(8) :: leng
  end type element_set
  type(element_set),allocatable :: es(:)

  interface
     function cross(x1,x2,x3,x4) result(out)
       real(8) :: x1(2),x2(2),x3(2),x4(2)
       integer :: out
     end function cross

     function ltc(s,g) result(out)
       real(8) :: s,g
       integer :: out
     end function ltc

     function sol(a0,a1,a2,a3) result(out)
       real(8) :: a0,a1,a2,a3,out
     end function sol
  end interface

  preset_shortestedge=ps*min(l1/dble(xe),l2/dble(ye))

  allocate(elmflag(nelement))

  ! 隣接関係を作成
  allocate(connect(2,nelement))
  do i=1,nelement ! 各要素
     do j=i+1,nelement
        if(element(1,i).eq.element(2,j)) then
           connect(1,i)=j
           connect(2,j)=i
        else if(element(2,i).eq.element(1,j)) then
           connect(2,i)=j
           connect(1,j)=i
        end if
     end do
  end do

  !短い辺を削除
  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do

  allocate(elm(2,nnode)) !elm(i,j): j番節点のi番要素を返す配列を作る
  do i=1,nnode
     do j=1,nelement
        if(i.eq.element(1,j))then
           elm(1,i)=j
           goto 100
        elseif(i.eq.element(2,j))then
           elm(2,i)=j
           goto 100
        end if
100     continue
     end do
  end do

  allocate(atnd(2,nnode)) !atnd(i,j): j番節点における接線のx_i成分
  do i=1,nnode
     atnd(:,i)=elml(elm(1,i))*el%at(:,elm(1,i))+elml(elm(2,i))*el%at(:,elm(2,i))
     anorm=1.d0/sqrt(dot_product(atnd(:,i),atnd(:,i)))
     atnd(:,i)=atnd(:,i)*anorm
  end do

  allocate(es(nelement))

  do i=1,nelement
     allocate(es(i)%elm(5))
     es(i)%n=0
     es(i)%selm=i
     es(i)%gelm=i
  end do

  au=0.5d0
  f1=2.d0*au**3-3.d0*au**2+1.d0
  f2=-2.d0*au**3+3.d0*au**2
  f3=au**3-2.d0*au**2+au
  f4=au**3-au**2
  elmflag(:)=0
  itmp=1
  do i=1,nelement
     if(elmflag(i).eq.1) cycle
     if(elml(i).gt.preset_shortestedge)then
        es(i)%n=es(i)%n+1
        es(i)%elm(es(i)%n)=i
     else if(elml(i).le.preset_shortestedge)then
        ! 連続する微小要素の数カウント
        is=i;ig=i
        ntinyelm=1;ielm=i
        do j=1,3
           if(elml(connect(1,ielm)).le.preset_shortestedge) then
              ielm=connect(1,ielm)
              ntinyelm=ntinyelm+1
              is=ielm
           else
              exit
           end if
        end do
        if(ntinyelm.lt.4) then
           ielm=i
           do j=1,3
              if(elml(connect(2,ielm)).le.preset_shortestedge) then
                 ielm=connect(2,ielm)
                 ntinyelm=ntinyelm+1
                 ig=ielm
              else
                 exit
              end if
           end do
        end if
        if(ntinyelm.eq.1) then
           elmflag(i)=1
           xcnt(:)=nodepos(:,element(1,i))*f1&
                +nodepos(:,element(2,i))*f2&
                +elml(i)*atnd(:,element(1,i))*f3&
                +elml(i)*atnd(:,element(2,i))*f4

           es(connect(1,i))%n=es(connect(1,i))%n+1
           es(connect(1,i))%elm(es(connect(1,i))%n)=i
           es(connect(1,i))%gelm=i
        else if(ntinyelm.eq.2) then
           elmflag(is)=1
           elmflag(ig)=1

           es(connect(1,is))%n=es(connect(1,is))%n+1
           es(connect(1,is))%elm(es(connect(1,is))%n)=is
           es(connect(1,is))%gelm=is

           es(connect(2,ig))%n=es(connect(2,ig))%n+1
           es(connect(2,ig))%elm(es(connect(2,ig))%n)=ig
           es(connect(2,ig))%selm=ig
        else if(ntinyelm.eq.3) then
           elmflag(is)=1
           elmflag(connect(2,is))=1
           elmflag(ig)=1

           xcnt(:)=nodepos(:,element(1,connect(2,is)))*f1&
                +nodepos(:,element(2,connect(2,is)))*f2&
                +elml(i)*atnd(:,element(1,connect(2,is)))*f3&
                +elml(i)*atnd(:,element(2,connect(2,is)))*f4

           es(connect(1,is))%n=es(connect(1,is))%n+1
           es(connect(1,is))%elm(es(connect(1,is))%n)=is
           es(connect(1,is))%n=es(connect(1,is))%n+1
           es(connect(1,is))%elm(es(connect(1,is))%n)=connect(2,is)
           es(connect(1,is))%gelm=connect(2,is)

           es(connect(2,ig))%n=es(connect(2,ig))%n+1
           es(connect(2,ig))%elm(es(connect(2,ig))%n)=ig
           es(connect(2,ig))%selm=ig
        else if(ntinyelm.eq.4) then
           elmflag(is)=1
           elmflag(connect(2,is))=1
           elmflag(connect(1,ig))=1
           elmflag(ig)=1
        end if
     end if
  end do
  ! es(i)%n=0     : 微小要素
  ! es(i)%n=1     : 微小要素と隣り合わない微小でない要素
  ! es(i)%n.ge.2  : 微小要素と隣り合う微小でない要素

  deallocate(el%at)
  allocate(el%at(2,nelement))
  do i=1,nelement
     anorm=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))**2+&
          (nodepos(2,element(2,i))-nodepos(2,element(1,i)))**2
     anorm=1d0/sqrt(anorm)
     el%at(1,i)=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))*anorm
     el%at(2,i)=(nodepos(2,element(2,i))-nodepos(2,element(1,i)))*anorm
  enddo
  deallocate(elm,atnd)

  deallocate(elml)
  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do

  allocate(ndiv(nelement))
  ndiv(:)=0
  do i=1,nelement
     if(es(i)%n.eq.1) then
        ndiv(i)=idiv*int(2.d0*elml(i)/preset_shortestedge) !各要素を何等分するかを決定
     else if (es(i)%n.ge.2) then
        es(i)%leng=0.d0
        do j=1,es(i)%n
           x1(:)=nodepos(:,element(1,es(i)%elm(j)))
           x2(:)=nodepos(:,element(2,es(i)%elm(j)))
           es(i)%leng=es(i)%leng+sqrt(dot_product(x1(:)-x2(:),x1(:)-x2(:)))
        end do
        ndiv(i)=idiv*int(2.d0*es(i)%leng/preset_shortestedge) !各要素を何等分するかを決定
     end if
     allocate(es(i)%nd(ndiv(i)+1))
  end do
  allocate(elementnew(2,sum(ndiv)))
  allocate(nodeposnew(2,2*sum(ndiv)))

  allocate(atnd(2,nnode)) !atnd(i,j): j番節点の接線ベクトルのx_i成分
  allocate(elm(2,nnode)) !elm(i,j): j番節点のi番要素を返す配列を作る
  do i=1,nnode
     do j=1,nelement
        if(i.eq.element(1,j))then
           elm(1,i)=j
           goto 102
        elseif(i.eq.element(2,j))then
           elm(2,i)=j
           goto 102
        end if
102     continue
     end do
  end do
  do i=1,nnode
     atnd(:,i)=elml(elm(1,i))*el%at(:,elm(1,i))+elml(elm(2,i))*el%at(:,elm(2,i))
     anorm=1.d0/sqrt(dot_product(atnd(:,i),atnd(:,i)))
     atnd(:,i)=atnd(:,i)*anorm
  end do

  nmax=maxval(ndiv)*4
  allocate(l2e(0:xe,0:ye))
  do iy=0,ye
     do ix=0,xe
        allocate(l2e(ix,iy)%elm(nmax))
        allocate(l2e(ix,iy)%edge(0:3,nmax))
        allocate(l2e(ix,iy)%start(nmax))
        allocate(l2e(ix,iy)%goal(nmax))
        allocate(l2e(ix,iy)%au(2,nmax))
        l2e(ix,iy)%n=0
        l2e(ix,iy)%edge(:,:)=0
     end do
  end do
  allocate(e2l(sum(ndiv)))
  do i=1,sum(ndiv)
     e2l(i)%n=0
     allocate(e2l(i)%ltc(2,5))
  end do

  fergscale=1.d0
  allocate(divflag(nelement),melm(sum(ndiv)))
  divflag(:)=1
  nnode=0
3333 continue
  do i=1,nelement
     if(divflag(i).eq.1) then   !未分割
        if(es(i)%n.eq.1) then   !微小要素と隣り合わない非微小要素
           iltc=e2lpre(i)%ltc(1,1)
           jltc=e2lpre(i)%ltc(2,1)
           do k=1,l2epre(iltc,jltc)%n
              if(l2epre(iltc,jltc)%elm(k).eq.i) then
                 start=l2epre(iltc,jltc)%start(k)
                 goal =l2epre(iltc,jltc)%goal(k)
              end if
           end do

           ! 始点
           nnode=nnode+1
           nds=nnode
           nodeposnew(:,nnode)=nodepos(:,element(1,i))
           es(i)%nd(1)=nnode
           do j=1,ndiv(i)-1
              au=dble(j)/dble(ndiv(i))
              f1=2.d0*au**3-3.d0*au**2+1.d0
              f2=-2.d0*au**3+3.d0*au**2
              f3=au**3-2.d0*au**2+au
              f4=au**3-au**2
              xcnt(:)=nodepos(:,element(1,i))*f1&
                   +nodepos(:,element(2,i))*f2&
                   +fergscale*elml(i)*atnd(:,element(1,i))*f3&
                   +fergscale*elml(i)*atnd(:,element(2,i))*f4
              !nodeを追加
              nnode=nnode+1
              nodeposnew(:,nnode)=xcnt(:)
              es(i)%nd(j+1)=nnode
              elementnew(1,sum(ndiv(1:i-1))+j)=nnode-1
              elementnew(2,sum(ndiv(1:i-1))+j)=nnode

              melm(sum(ndiv(1:i-1))+j)=i
              e2l(sum(ndiv(1:i-1))+j)%n=e2l(sum(ndiv(1:i-1))+j)%n+1
              e2l(sum(ndiv(1:i-1))+j)%ltc(1,e2l(sum(ndiv(1:i-1))+j)%n)=iltc
              e2l(sum(ndiv(1:i-1))+j)%ltc(2,e2l(sum(ndiv(1:i-1))+j)%n)=jltc

              l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
              l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i-1))+j
              l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=dble(j-1)/dble(ndiv(i)) !始点のau
              l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=au !終点のau

              if(j.eq.1) then
                 l2e(iltc,jltc)%edge(0:3,l2e(iltc,jltc)%n)=0
                 l2e(iltc,jltc)%edge(start,l2e(iltc,jltc)%n)=1
              end if
           end do

           ! 終点
           nnode=nnode+1
           ndg=nnode
           nodeposnew(:,nnode)=nodepos(:,element(2,i))
           es(i)%nd(ndiv(i)+1)=nnode

           !要素を追加
           elementnew(1,sum(ndiv(1:i-1))+1)=nds
           elementnew(1,sum(ndiv(1:i)))=nnode-1
           elementnew(2,sum(ndiv(1:i)))=ndg
           melm(sum(ndiv(1:i)))=i
           do k=1,e2lpre(i)%n
              e2l(sum(ndiv(1:i)))%n=e2l(sum(ndiv(1:i)))%n+1
              e2l(sum(ndiv(1:i)))%ltc(1,e2l(sum(ndiv(1:i)))%n)=iltc
              e2l(sum(ndiv(1:i)))%ltc(2,e2l(sum(ndiv(1:i)))%n)=jltc

              l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
              l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i))
              l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1
              l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=dble(ndiv(i)-1)/dble(ndiv(i))
              l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=1
           end do
        else if(es(i)%n.ge.2) then !微小要素と隣り合う非微小要素
           ! 始点
           nnode=nnode+1
           nds=nnode
           nodeposnew(:,nnode)=nodepos(:,element(1,es(i)%selm))
           es(i)%nd(1)=nnode

           ! selm番要素のstart,goal
           iltc=e2lpre(es(i)%selm)%ltc(1,1)
           jltc=e2lpre(es(i)%selm)%ltc(2,1)
           do k=1,l2epre(iltc,jltc)%n
              if(l2epre(iltc,jltc)%elm(k).eq.es(i)%selm) then
                 start=l2epre(iltc,jltc)%start(k) ! 始辺
                 goal =l2epre(iltc,jltc)%goal(k) ! 始辺
              end if
           end do

           aupre=0
           elm1=es(i)%selm
           do j=1,ndiv(i)-1
              au=dble(j)/dble(ndiv(i))
              elm2=es(i)%selm
              leng=0.d0
              leng0=0.d0
              ! auに対応する点がk=1〜es(i)%nのどの要素上に乗ってるか
              do k=1,es(i)%n
                 leng=leng+elml(elm2)
                 if(au*es(i)%leng.lt.leng) then
                    exit
                 else
                    leng0=leng
                    elm2=connect(2,elm2)
                 end if
              end do
              au=(au*es(i)%leng-leng0)/elml(elm2) ! elm2上での局所座標
              f1=2.d0*au**3-3.d0*au**2+1.d0
              f2=-2.d0*au**3+3.d0*au**2
              f3=au**3-2.d0*au**2+au
              f4=au**3-au**2
              xcnt(:)=nodepos(:,element(1,elm2))*f1&
                   +nodepos(:,element(2,elm2))*f2&
                   +fergscale*elml(elm2)*atnd(:,element(1,elm2))*f3&
                   +fergscale*elml(elm2)*atnd(:,element(2,elm2))*f4 !elm2上での座標

              ! nodeを追加
              nnode=nnode+1
              nodeposnew(:,nnode)=xcnt(:)
              es(i)%nd(j+1)=nnode

              elementnew(1,sum(ndiv(1:i-1))+j)=nnode-1
              elementnew(2,sum(ndiv(1:i-1))+j)=nnode
              melm(sum(ndiv(1:i-1))+j)=i

              ! 要素番号-->格子情報、格子番号-->要素情報
              if(elm1.eq.elm2) then !格子境界をまたがないとき
                 e2l(sum(ndiv(1:i-1))+j)%n=e2l(sum(ndiv(1:i-1))+j)%n+1
                 e2l(sum(ndiv(1:i-1))+j)%ltc(1,e2l(sum(ndiv(1:i-1))+j)%n)=iltc
                 e2l(sum(ndiv(1:i-1))+j)%ltc(2,e2l(sum(ndiv(1:i-1))+j)%n)=jltc
                 l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
                 l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i-1))+j
                 l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=aupre
                 l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=au
                 if(j.eq.1) then
                    l2e(iltc,jltc)%edge(0:3,l2e(iltc,jltc)%n)=0
                    l2e(iltc,jltc)%edge(start,l2e(iltc,jltc)%n)=1
                 end if
              else if(elm1.ne.elm2) then !格子境界をまたぐとき
                 e2l(sum(ndiv(1:i-1))+j)%n=e2l(sum(ndiv(1:i-1))+j)%n+1
                 e2l(sum(ndiv(1:i-1))+j)%ltc(1,e2l(sum(ndiv(1:i-1))+j)%n)=iltc
                 e2l(sum(ndiv(1:i-1))+j)%ltc(2,e2l(sum(ndiv(1:i-1))+j)%n)=jltc
                 l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
                 l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i-1))+j
                 l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=aupre
                 l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=-1
                 l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1
                 if(j.eq.1) then
                    l2e(iltc,jltc)%edge(0:3,l2e(iltc,jltc)%n)=0
                    l2e(iltc,jltc)%edge(start,l2e(iltc,jltc)%n)=1
                    l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1
                 end if

                 iltc=e2lpre(elm2)%ltc(1,1)
                 jltc=e2lpre(elm2)%ltc(2,1)
                 do k=1,l2epre(iltc,jltc)%n
                    if(l2epre(iltc,jltc)%elm(k).eq.elm2) then
                       start=l2epre(iltc,jltc)%start(k)
                       goal =l2epre(iltc,jltc)%goal(k)
                    end if
                 end do
                 e2l(sum(ndiv(1:i-1))+j)%n=e2l(sum(ndiv(1:i-1))+j)%n+1
                 e2l(sum(ndiv(1:i-1))+j)%ltc(1,e2l(sum(ndiv(1:i-1))+j)%n)=iltc
                 e2l(sum(ndiv(1:i-1))+j)%ltc(2,e2l(sum(ndiv(1:i-1))+j)%n)=jltc
                 l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
                 l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i-1))+j
                 l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=-1
                 l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=au
                 l2e(iltc,jltc)%edge(start,l2e(iltc,jltc)%n)=1

                 elm1=elm2
              end if
              aupre=au
           end do
           ! 終点
           nnode=nnode+1
           ndg=nnode
           nodeposnew(:,nnode)=nodepos(:,element(2,es(i)%gelm))
           es(i)%nd(ndiv(i)+1)=nnode

           !要素を追加
           elementnew(1,sum(ndiv(1:i-1))+1)=nds
           elementnew(1,sum(ndiv(1:i)))=nnode-1
           elementnew(2,sum(ndiv(1:i)))=ndg
           melm(sum(ndiv(1:i)))=i

           if(elm1.eq.es(i)%gelm) then !格子境界をまたがないとき
              e2l(sum(ndiv(1:i)))%n=e2l(sum(ndiv(1:i)))%n+1
              e2l(sum(ndiv(1:i)))%ltc(1,e2l(sum(ndiv(1:i)))%n)=iltc
              e2l(sum(ndiv(1:i)))%ltc(2,e2l(sum(ndiv(1:i)))%n)=jltc
              l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
              l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i))
              l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=aupre
              l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=1
              l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1
           else if(elm1.ne.es(i)%gelm) then !格子境界またぐとき
              e2l(sum(ndiv(1:i)))%n=e2l(sum(ndiv(1:i)))%n+1
              e2l(sum(ndiv(1:i)))%ltc(1,e2l(sum(ndiv(1:i)))%n)=iltc
              e2l(sum(ndiv(1:i)))%ltc(2,e2l(sum(ndiv(1:i)))%n)=jltc
              l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
              l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i))
              l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=aupre
              l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=-1
              l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1

              iltc=e2lpre(es(i)%gelm)%ltc(1,1)
              jltc=e2lpre(es(i)%gelm)%ltc(2,1)
              do k=1,l2epre(iltc,jltc)%n
                 if(l2epre(iltc,jltc)%elm(k).eq.es(i)%gelm) then
                    start=l2epre(iltc,jltc)%start(k)
                    goal =l2epre(iltc,jltc)%goal(k)
                 end if
              end do
              e2l(sum(ndiv(1:i)))%n=e2l(sum(ndiv(1:i)))%n+1
              e2l(sum(ndiv(1:i)))%ltc(1,e2l(sum(ndiv(1:i)))%n)=iltc
              e2l(sum(ndiv(1:i)))%ltc(2,e2l(sum(ndiv(1:i)))%n)=jltc
              l2e(iltc,jltc)%n=l2e(iltc,jltc)%n+1
              l2e(iltc,jltc)%elm(l2e(iltc,jltc)%n)=sum(ndiv(1:i))
              l2e(iltc,jltc)%au(1,l2e(iltc,jltc)%n)=-1
              l2e(iltc,jltc)%au(2,l2e(iltc,jltc)%n)=1
              l2e(iltc,jltc)%edge(start,l2e(iltc,jltc)%n)=1
              l2e(iltc,jltc)%edge(goal,l2e(iltc,jltc)%n)=1
           end if
        end if
     else if(divflag(i).eq.2) then !再分割
        if(es(i)%n.eq.1) then
           do j=1,ndiv(i)-1
              au=dble(j)/dble(ndiv(i))
              f1=2.d0*au**3-3.d0*au**2+1.d0
              f2=-2.d0*au**3+3.d0*au**2
              f3=au**3-2.d0*au**2+au
              f4=au**3-au**2
              xcnt(:)=nodepos(:,element(1,i))*f1&
                   +nodepos(:,element(2,i))*f2&
                   +fergscale*elml(i)*atnd(:,element(1,i))*f3&
                   +fergscale*elml(i)*atnd(:,element(2,i))*f4
              !nodeを追加
              nodeposnew(:,es(i)%nd(j+1))=xcnt(:)
           end do
        else if(es(i)%n.ge.2) then
           elm1=es(i)%selm
           do j=1,ndiv(i)-1
              au=dble(j)/dble(ndiv(i))
              elm2=es(i)%selm
              leng=0.d0
              leng0=0.d0
              do k=1,es(i)%n
                 leng=leng+elml(elm2)
                 if(au*es(i)%leng.lt.leng) then
                    exit
                 else
                    leng0=leng
                    elm2=connect(2,elm2)
                 end if
              end do
              au=(au*es(i)%leng-leng0)/elml(elm2)
              f1=2.d0*au**3-3.d0*au**2+1.d0
              f2=-2.d0*au**3+3.d0*au**2
              f3=au**3-2.d0*au**2+au
              f4=au**3-au**2
              xcnt(:)=nodepos(:,element(1,elm2))*f1&
                   +nodepos(:,element(2,elm2))*f2&
                   +fergscale*elml(i)*atnd(:,element(1,elm2))*f3&
                   +fergscale*elml(i)*atnd(:,element(2,elm2))*f4

              ! nodeを追加
              nodeposnew(:,es(i)%nd(j+1))=xcnt(:)

              ! 格子辺と交差するか
              if(elm1.ne.elm2) then !格子境界をまたいだとき
                 elm1=elm2
              end if
           end do
        end if
     end if
     divflag(i)=0
  end do

  ! 交差判定
  scaleflag=0
  do i=1,sum(ndiv)
     x1(:)=nodeposnew(:,elementnew(1,i))
     x2(:)=nodeposnew(:,elementnew(2,i))
     imax=-1
     imin=xe+1
     jmax=-1
     jmin=ye+1
     do iltc=1,e2l(i)%n         !i番要素を含む格子ループ
        ix=e2l(i)%ltc(1,iltc)
        iy=e2l(i)%ltc(2,iltc)
        if(ix.gt.imax) imax=ix
        if(ix.lt.imin) imin=ix
        if(iy.gt.jmax) jmax=iy
        if(iy.lt.jmin) jmin=iy
     end do
     do ix=imin-1,imax+1      !i番要素を含む格子近傍の格子ループ
        do iy=jmin-1,jmax+1
           do k=1,l2e(ix,iy)%n     !i番要素近傍の格子内の境界要素ループ
              j=l2e(ix,iy)%elm(k)
              x3(:)=nodeposnew(:,elementnew(1,j))
              x4(:)=nodeposnew(:,elementnew(2,j))
              if((abs(x1(1)-x4(1)).lt.eps1).and.(abs(x1(2)-x4(2)).lt.eps1)) cycle
              if((abs(x2(1)-x3(1)).lt.eps1).and.(abs(x2(2)-x3(2)).lt.eps1)) cycle
              if(i.eq.j) cycle
              if(cross(x1,x2,x3,x4).eq.1) then
                 divflag(melm(j))=2
                 divflag(melm(i))=2
                 scaleflag=1
              end if
           end do
        end do
     end do
  end do

  if(scaleflag.eq.1) then
     write(*,*) 'cross'
     fergscale=fergscale*0.5d0
     goto 3333
  end if

  ! 重複する節点の統合 
  do i=1,nnode-1
     xi=nodeposnew(1,i)
     yi=nodeposnew(2,i)
     do j=i+1,nnode
        xj=nodeposnew(1,j)
        yj=nodeposnew(2,j)
        if(abs(xi-xj)<eps1.and.abs(yi-yj)<eps1) then ! iとjが一致したら
           do k=j,nnode-1
              nodeposnew(:,k)=nodeposnew(:,k+1) ! j+1 以降の点も一つずつ前にずらす
           end do
           do k=1,sum(ndiv)
              do l=1,2
                 if(elementnew(l,k).eq.j) then
                    elementnew(l,k)=i ! 要素の節点jをiに置換し
                 else if(elementnew(l,k).gt.j) then
                    elementnew(l,k)=elementnew(l,k)-1 ! j+1以降の点を一つずつ前にずらす
                 end if
              end do
           end do
           nnode=nnode-1 ! 節点が一つ減った
        end if
     end do
  end do

  ! call ltc2elm_check(nnode,nodeposnew,sum(ndiv),elementnew,l2e)
  ! stop

  deallocate(elml)
  allocate(elml(sum(ndiv)))
  do i=1,sum(ndiv) !要素長を計算
     elml(i)=sqrt(dot_product(&
          nodeposnew(:,elementnew(2,i))-nodeposnew(:,elementnew(1,i)),&
          nodeposnew(:,elementnew(2,i))-nodeposnew(:,elementnew(1,i))))
  end do
  shortestedge=minval(elml)
  longestedge=maxval(elml)
  write(*,*) "# Final mesh"
  write(*,*) '## nnode=',sum(ndiv)
  write(*,*) '## nelement=',sum(ndiv)
  write(*,*) "## (Longest edge)",longestedge
  write(*,*) "## (Shortest edge)=",shortestedge
  write(*,*) "## (Longest edge)/(Shortest edge)=",longestedge/shortestedge

  ! ! BEMに渡すため、メッシュデータをfileに書き込み
  ! open(1,file=file_elm)
  ! write(1,*) sum(ndiv)
  ! do i=1,sum(ndiv)
  !    write(1,*) i,nodeposnew(:,i),1
  ! end do
  ! write(1,*) sum(ndiv)
  ! do i=1,sum(ndiv)
  !    write(1,*) i,elementnew(:,i)
  ! end do
  ! close(1)

  ! open(107,file=file_elm_gp)
  ! do i=1,sum(ndiv) !要素改善後の境界要素の絵を描く
  !    do j=1,2
  !       write(107,*) nodeposnew(:,elementnew(j,i)),elementnew(j,i)
  !    end do
  !    write(107,*)
  ! end do
  ! close(107)
  ! ! stop 'meshimp'

  if(sum(ndiv).eq.0) iflag_bounded=0
  if(sum(ndiv).ne.0) iflag_bounded=1

  ! call mesh_peri(preset_shortestedge)
  call mesh_peri(longestedge)

  deallocate(elml,ndiv)

contains
  subroutine mesh_peri(el)
    use io
    real(8),intent(in)::el
    real(8)::ndpos,ua(2,2),xtmp(2)
    real(8),allocatable::ndposperi(:,:),ndposcross(:,:,:),length(:)
    integer,allocatable::ndcross(:,:),nnp(:),ndperi(:,:),elopps(:)
    integer::nn,tmp(4),icross,nb,jtmp,ktmp,nel,nnd

    ! unit vectors
    ua(:,1)=(/1.d0, 0.d0/)
    ua(:,2)=(/0.d0, 1.d0/)

    open(1,file=file_mesh)
    nn=int(l1/el)            !1辺の要素数
    nnd=4*nn
    nel=nnd
    allocate(ndposperi(2,nnd))
    ndposperi=0.d0;ndposperi(1,2)=dble(xe);ndposperi(2,3)=dble(ye);ndposperi(:,4)=dble(xe) !o
    do k=1,4              !I1->I2->D1->D2
       if(k.eq.1.or.k.eq.2) xtmp=ndposperi(:,1) !o
       if(k.eq.3) xtmp=ndposperi(:,3)           !o
       if(k.eq.4) xtmp=ndposperi(:,2)           !o
       do i=1,nn-1
          ndposperi(:,4+i+(k-1)*(nn-1))=xtmp& !o
               +dble(xe)*0.5d0*(1.d0-cos(pi*i/dble(nn)))*ua(:,2-mod(k,2))
       end do
    end do

    allocate(ndperi(2,nel))
    allocate(elopps(nel))
    itmp=0
    do k=1,4
       itmp=itmp+1               !要素番号
       jtmp=nnode+4+(k-1)*(nn-1)+1 !節点番号
       ktmp=(-1)**(k/3)*2*nn+itmp !反対側の要素番号
       if(k.eq.1.or.k.eq.4)then
          ndperi(1,itmp)=nnode+k/4+1 !o
          ndperi(2,itmp)=jtmp             !o
          elopps(itmp)=ktmp
          do i=1,nn-2
             itmp=itmp+1
             ndperi(1,itmp)=jtmp+i-1 !o
             ndperi(2,itmp)=jtmp+i   !o
             elopps(itmp)=ktmp+i
          end do
          itmp=itmp+1
          ndperi(1,itmp)=jtmp+nn-2 !o
          ndperi(2,itmp)=nnode+2*(k+2)/3
          elopps(itmp)=ktmp+nn-1
       else
          ndperi(1,itmp)=jtmp             !o
          ndperi(2,itmp)=nnode+2*k-3 !o
          elopps(itmp)=ktmp
          do i=1,nn-2
             itmp=itmp+1
             ndperi(1,itmp)=jtmp+i   !o
             ndperi(2,itmp)=jtmp+i-1 !o
             elopps(itmp)=ktmp+i
          end do
          itmp=itmp+1
          ndperi(1,itmp)=nnode+k+1
          ndperi(2,itmp)=jtmp+nn-2 !o
          elopps(itmp)=ktmp+nn-1
       end if
    end do

!!!!!!!!!output!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    write(1,*) nnode+nnd !節点数
    do i=1,nnode
       !          節点番号，座標
       write(1,fmt_i1d2) i,nodeposnew(1,i)*avec(:,1)/l1+nodeposnew(2,i)*avec(:,2)/l2 !長方形格子->平行四辺形格子
       ! write(1,*) i,nodeposnew(1,i)/dble(xe)*avec(:,1)+nodeposnew(2,i)/dble(ye)*avec(:,2) !長方形格子->平行四辺形格子
    end do
    do i=1,nnd
       write(1,fmt_i1d2) i+nnode,ndposperi(1,i)/dble(xe)*avec(:,1)+ndposperi(2,i)/dble(ye)*avec(:,2)
    end do
    write(1,*) sum(ndiv)+nel !要素数
    !             要素番号            ,node番号      ,ibc,iperi(1,i)               ,iperi(2,i)
    do i=1,sum(ndiv)
       write(1,*) i                  ,elementnew(:,i),1  ,0                        ,0
    end do
    itmp=0
    do k=1,4
       do j=1,nn
          itmp=itmp+1
          write(1,*) sum(ndiv)+itmp          ,ndperi(:,itmp) ,10,(mod(k,2)+1)*(-1)**(k/3+1),sum(ndiv)+elopps(itmp)
       end do
    end do
!!!!!!!!!output!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    deallocate(ndposperi,ndperi,elopps)

    close(1)

  end subroutine mesh_peri

end subroutine improve_mesh
