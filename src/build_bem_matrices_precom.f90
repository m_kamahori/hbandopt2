subroutine build_bem_matrices_precom(omgj,idomp)
  use globals
  implicit none
  integer,intent(in)::idomp
  complex(8),intent(in)::omgj

  if(islv.eq.-1)then
     call bem_matrices_precom(omgj,smat(:,:,idomp),dmat(:,:,idomp))
  else if(islv.eq.0)then
     call woodbury_precom(omgj,idomp)
  else if(islv.eq.2)then
     call hwoodbury_precom(omgj,idomp)                !build H-matrix(subb)
  else if(islv.eq.3)then
     call hlu_precom(omgj,idomp)                !build H-matrix(subb)
  end if

contains
  subroutine bem_matrices_precom(wn,s,d)
    use globals
    implicit none
    complex(8),intent(in)::wn
    complex(8),intent(out)::s(:,:),d(:,:)
    integer::i,j,ig,joffset
    integer::nj1,nj2
    real(8)::x1,x2,a,r,doty
    complex(8)::z
    complex(8)::stmp,dtmp
    complex(8)::CBJ0,CBJ1,CBY0,CBY1
    complex(8)::zcy1,zcy2

    joffset=edges(1)
    do j=1,joffset               !source
       do i=1,ne                  !observe
          nj1=nd(1,j)
          nj2=nd(2,j)
          x1=(c(1,i)-c(1,j))*at(1,j)+(c(2,i)-c(2,j))*at(2,j) !垂線の足から要素中心までの距離(符号付き)
          x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
          a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
          dtmp=zero
          do ig=1,ng
             r=sqrt((x1-a*gzai(ig))**2+x2**2)
             doty=-x2
             z=wn*r
             call CJY1_R(Z,CBJ1,CBY1)
             zcy2=CBJ1+ione*CBY1
             dtmp=dtmp+wei(ig)*a*(-wn*zcy2)*doty/r
          end do
          if(i.eq.j) dtmp=cmplx(0.d0,-2.d0,kind=8)
          d(i,j)=dtmp
       end do
    end do

    do j=joffset+1,ne
       do i=1,ne                  !observe
          nj1=nd(1,j)
          nj2=nd(2,j)
          x1=(c(1,i)-c(1,j))*at(1,j)+(c(2,i)-c(2,j))*at(2,j) !垂線の足から要素中心までの距離(符号付き)
          x2=(c(1,i)-c(1,j))*an(1,j)+(c(2,i)-c(2,j))*an(2,j) !垂線の足の長さ(符号付き)
          a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2) !要素長の半分
          stmp=zero
          dtmp=zero
          do ig=1,ng
             r=sqrt((x1-a*gzai(ig))**2+x2**2)
             doty=-x2
             z=wn*r
             call CJY01_R(Z,CBJ0,CBJ1,CBY0,CBY1)
             zcy1=CBJ0+ione*CBY0
             zcy2=CBJ1+ione*CBY1
             stmp=stmp+wei(ig)*a*zcy1
             dtmp=dtmp+wei(ig)*a*(-wn*zcy2)*doty/r
          end do
          if(i.eq.j) dtmp=cmplx(0.d0,-2.d0,kind=8)
          s(i,j-joffset)=stmp
          d(i,j)=dtmp
       end do
    end do
    s(:,:)=s(:,:)*0.25d0*ione
    d(:,:)=d(:,:)*0.25d0*ione

  end subroutine bem_matrices_precom

  subroutine woodbury_precom(omgj,idomp)
    integer,intent(in)::idomp
    integer::info
    complex(8),intent(in)::omgj
    complex(8),allocatable::subc_herm(:,:)

    call bem_matrices_precom(omgj,smat(:,:,idomp),dmat(:,:,idomp)) !B,C,D,Eのkernel計算
    subb(:,:,idomp)=dmat(1:edges(1),1:edges(1),idomp)
    subc(:,:,idomp)=dmat(edges(1)+1:ne,1:edges(1),idomp)
    call zgetrf(edges(1),edges(1),subb(:,:,idomp),edges(1),ipiv_b(:,idomp),info) !LU decomposition

    allocate(subc_herm(edges(1),sum(edges(2:5))))
    subc_herm(:,:)=transpose(subc(:,:,idomp))
    call zgetrs('T',edges(1),sum(edges(2:5)),subb(:,:,idomp),edges(1),ipiv_b(:,idomp),subc_herm,edges(1),info)
    cbinv(:,:,idomp)=transpose(subc_herm(:,:))
    deallocate(subc_herm)

  end subroutine woodbury_precom

  subroutine hwoodbury_precom(omgj,idomp)
    use class_hbem
    integer,intent(in)::idomp
    complex(8),intent(in)::omgj

    call build_block_hmats(omgj,idomp)

    hbmat(idomp)=invert_hmat(hbmat(idomp),tol)
    hcbinvmat(idomp)=clone_struct_zero_hmat(hcmat(idomp))
    call addmul_hmatrix(one,false,hcmat(idomp),false,hbmat(idomp),c_null_ptr,tol,&
         hcbinvmat(idomp)) !CB^{-1}
    
  end subroutine hwoodbury_precom

  subroutine hlu_precom(omgj,idomp)
    use class_hbem
    integer,intent(in)::idomp
    complex(8),intent(in)::omgj

    call build_block_hmats(omgj,idomp)

  end subroutine hlu_precom

  subroutine build_block_hmats(omgj,idomp)
    use class_hbem
    integer,intent(in)::idomp
    complex(8),intent(in)::omgj
    type(c_ptr)::hmats(4)
    integer::i,j,itmp,bnum

    ! -----------------hbmat---------------------------------
    hmats(1) = mkhmat(bc(1)%root_cluster,bc(1)%root_cluster,eta,eps_aca,1,2,1,omgj)
    hbmat(idomp) = integrate_hmatrix2(irep_cluster,irep_cluster,1,hmats)
    ! hbmat(idomp) = integrate_hmatrix(irep_cluster,irep_cluster,c_loc(hmats(1)))
    ! -----------------hbmat---------------------------------

    ! -----------------hcmat---------------------------------
    do i=2,5
       hmats(i-1)=mkhmat(bc(i)%root_cluster,bc(1)%root_cluster,eta,eps_aca,i,2,1,omgj)
    end do
    hcmat(idomp) = integrate_hmatrix2(peri_cluster,irep_cluster,4,hmats)
    ! hcmat(idomp) = integrate_hmatrix(peri_cluster,irep_cluster,c_loc(hmats(1)))
    ! -----------------hcmat---------------------------------

    ! -----------------hdmat---------------------------------
    itmp=0
    i=1
    do j=2,3
       itmp=itmp+1
       bnum=i+(j-1)*5
       call mkhmat_peri2(bc(i)%root_cluster,bc(j)%root_cluster,bc(j+2)%root_cluster&
            ,eta,eps_aca,bnum,1,1,omgj,hdmat_i(itmp  ,idomp))
       call mkhmat_peri2(bc(i)%root_cluster,bc(j)%root_cluster,bc(j+2)%root_cluster&
            ,eta,eps_aca,bnum,1,2,omgj,hdmat_d(itmp  ,idomp))
       bnum=i+(j+1)*5
       call mkhmat_peri2(bc(i)%root_cluster,bc(j+2)%root_cluster,bc(j)%root_cluster&
            ,eta,eps_aca,bnum,2,1,omgj,hdmat_i(itmp+2,idomp))
       call mkhmat_peri2(bc(i)%root_cluster,bc(j+2)%root_cluster,bc(j)%root_cluster&
            ,eta,eps_aca,bnum,2,2,omgj,hdmat_d(itmp+2,idomp))
    end do
    ! -----------------hdmat---------------------------------

    ! -----------------hemat---------------------------------
    itmp=0
    do j=2,3
       do i=2,5
          itmp=itmp+1
          bnum=i+(j-1)*5
          hemat_i(itmp  ,idomp) = mkhmat_peri(bc(i)%root_cluster,bc(j)%root_cluster,bc(j+2)%root_cluster&
               ,eta,eps_aca,bnum,1,1,omgj)
          hemat_d(itmp  ,idomp) = mkhmat_peri(bc(i)%root_cluster,bc(j)%root_cluster,bc(j+2)%root_cluster&
               ,eta,eps_aca,bnum,1,2,omgj)
          bnum=i+(j+1)*5
          hemat_i(itmp+8,idomp) = mkhmat_peri(bc(i)%root_cluster,bc(j+2)%root_cluster,bc(j)%root_cluster&
               ,eta,eps_aca,bnum,2,1,omgj)
          hemat_d(itmp+8,idomp) = mkhmat_peri(bc(i)%root_cluster,bc(j+2)%root_cluster,bc(j)%root_cluster&
               ,eta,eps_aca,bnum,2,2,omgj)
       end do
    end do
    ! -----------------hemat---------------------------------

    call coarsen_hmat(hbmat(idomp), c_null_ptr, tol)
    call coarsen_hmat(hcmat(idomp), c_null_ptr, tol)
    do i=1,4
       ! call coarsen_hmat(hdmat_i(i,idomp), c_null_ptr, tol)
       call coarsen_hmat(hdmat_d(i,idomp), c_null_ptr, tol)
    end do
    do i=1,16
       ! call coarsen_hmat(hemat_i(i,idomp), c_null_ptr, tol)
       call coarsen_hmat(hemat_d(i,idomp), c_null_ptr, tol)
    end do

  end subroutine build_block_hmats

end subroutine build_bem_matrices_precom


! hmat(:)の番号付け(PEC、誘電体有の場合) TE mode
! ーーーーーーーーーーーーーーーーーーーーーー
! | 1(d)  | 4(s) 7(s~) | 10(d) 13(d~) |
! ーーーーーーーーーーーーーーーーーーーーーー
! | 2(d)  | 5(s) 8(s~) | 11(d) 14(d~) |
! ーーーーーーーーーーーーーーーーーーーーーー
! | 3(d)  | 6(s) 9(s~) | 12(d) 15(d~) |
! ーーーーーーーーーーーーーーーーーーーーーー

! mkhmat(root1,root2,eta,eps_aca,cflag,ceps,kflag,n,bflag,pflag,dflag,omgj) --> mkstruct.c
! root1   : row cluster
! root2   : column cluster
! eta     : eta in admissibility condition
! eps_aca : tolerance for the ACA
! cflag   : aggromaration するかしないか
! ceps    : tolerance for agglomeration
! n       : the number of elements.(not used)
! bflag   : the number of block clusters
! pflag   : the (1:single layer, 2:double layer) potential to be calculated
! dflag   : we integrate kernel functions on (1:independent, 2:dependent) boundary.
! omgj    : angular frequency


! integrate_hmat(sons, root(sons), nhmat, hmat(nhmat)) --> solve.c
! sons        : the number of row blocks (= the number of column blocks)
! root(sons)  : array of boundary clusters
! nhmat       : the number of sub matrices
! hmat(nhmat) : array of sub matrices
