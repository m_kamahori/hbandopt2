/* 内点クラスター生成 */
#include "curve2d.h"
#include "bem2d.h"

pcluster mkclusterin(uint nmin, uint nip, double xip[nip][2])
{
  uint i,j;
  pcluster root;
  pclustergeometry cg;
  uint q;
  uint  *idx;

  q=2; /* 次元 */

  /* クラスターツリーの生成 */
  cg = new_clustergeometry(q,nip);
  idx = (uint *) allocmem(nip * sizeof(uint));
  
  for (i=0;i<nip;i++) {
    idx[i] = i;
    for (j=0;j<2;j++){
      cg->x[i][j] = xip[i][j];
      cg->smin[i][j] = xip[i][j];
      cg->smax[i][j] = xip[i][j];
    }
  }

  root = build_adaptive_cluster(cg, nip, idx, nmin);

/* cluster check */
  /* FILE *fp; */
  /* fp = fopen("rootin.res", "a"); */
  /* check_cluster(root, 0, fp); */
  /* fclose(fp); */


  return root;
}
