module io
  use globals
  implicit none
  character(len=128)::file_lsf   !level set function
  character(len=128),allocatable::file_nextlsf(:)
  character(len=128)::file_ip2   !inner point
  character(len=128)::file_ip    !inner point (for plot)
  character(len=128)::file_mesh  !mesh
  character(len=128)::file_bp    !mesh (for plot)
  character(len=128)::file_band  !band diagram
  character(len=128)::file_evslct !selected eigenvalues
  character(len=128)::file_sd     !shape derivative
  character(len=128)::file_td     !topological derivative
  character(len=128)::file_hist   !history
  character(len=128)::file_ref   !reference eigenvector
  character(len=128)::file_log="./lsf2el2.log"
  character(len=32)::fmt_i1d2='(I12,2e25.16)'
  character(len=32)::fmt_i1d3='(I12,3e25.16)'
  character(len=32)::fmt_d2='(2e25.16)'
  character(len=32)::fmt_d3='(3e25.16)'
  character(len=32)::fmt_d4='(4e25.16)'

contains
  subroutine set_io_filename(i)
    integer,intent(in)::i
    real(8)::rtmp,itmp,tmp
    character(len=32)::stepname

    write(stepname,'(i4.4)') i
    ! write(nx,*) xe
    
    ! file_lsf=trim(trim(file_dir)//"/step"//trim(stepname)//".ls2")
    ! file_ip2=trim(trim(file_dir)//"/step"//trim(stepname)//".ip2_"//trim(adjustl(nx)))
    ! file_mesh=trim(trim(file_dir)//"/step"//trim(stepname)//".el2_"//trim(adjustl(nx)))
    ! file_ip=trim(trim(file_dir)//"/step"//trim(stepname)//".ip_"//trim(adjustl(nx)))
    ! file_bp=trim(trim(file_dir)//"/step"//trim(stepname)//".bp_"//trim(adjustl(nx)))
    ! file_band=trim(trim(file_dir)//"/step"//trim(stepname)//".bnd_"//trim(adjustl(nx)))
    ! file_evslct=trim(trim(file_dir)//"/step"//trim(stepname)//".slc_"//trim(adjustl(nx)))
    file_lsf   =trim(trim(file_dir)//"/lsf/step"//trim(stepname)//".ls2")
    file_ip2   =trim(trim(file_dir)//"/ip2/step"//trim(stepname)//".ip2")
    file_mesh  =trim(trim(file_dir)//"/mesh/step"//trim(stepname)//".el2")
    file_ip    =trim(trim(file_dir)//"/ip/step"//trim(stepname)//".ip")
    file_bp    =trim(trim(file_dir)//"/bp/step"//trim(stepname)//".bp")
    file_band  =trim(trim(file_dir)//"/band/step"//trim(stepname)//".bnd")
    file_evslct=trim(trim(file_dir)//"/evslct/step"//trim(stepname)//".slc")
    file_td    =trim(trim(file_dir)//"/td/step"//trim(stepname)//".td")
    file_sd    =trim(trim(file_dir)//"/sd/step"//trim(stepname)//".sd")
    file_ref   =trim(trim(file_dir)//"/ref/step"//trim(stepname)//".ref")

  end subroutine set_io_filename

  character(len=128) function update_lsf_filename(i)
    integer,intent(in)::i
    character(len=32)::step    

    write(step,'(i4.4)') i+1
    update_lsf_filename=trim(trim(file_dir)//"/lsf/step"//trim(step)//".ls2")
    
  end function update_lsf_filename
    
end module io
