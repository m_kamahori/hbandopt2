subroutine lsf2elm(iflag_bounded)
  use globals
  use io
  implicit none
  ! character(len=32),intent(in) :: file_lsf,file_elm_gp
  ! character(len=32),intent(in) :: file_elm
  integer,intent(out)::iflag_bounded

  real(8),parameter::eps1=1.0d-8
  real(8),parameter::eps2=1.0d-14

  integer itmp,ix,iy,i,j,k,l
  integer,allocatable::element(:,:)
  real(8),allocatable::nodepos(:,:)
  real(8),allocatable::refvec(:,:)
  !refvec:「voxelの中心から外側に向かう」or
  ! 「レベルセット関数の値が最大の節点からvoxelの中心に向かう」ベクトル(cnt_nn=2のとき)
  !refvec:voxelの中心から外側に向かうベクトル (cnt_nn=4のとき)
  !refvec:固定設計領域から外側に向かうベクトル (cnt_z>0のとき)

  real(8) lsf(0:3)

  integer cnt_z,cnt_p,cnt_m
  real(8) au,av,f1,f2,ww

  integer nnode,nelement,cnt_max,cnt_nn

  real(8) lsfs
  real(8) xi,yi,xj,yj
  
  real(8) anorm

  integer imaxlsf,iminlsf
  real(8) dmaxlsf,dminlsf

  real(8),allocatable::elml(:) !elml(i): 要素iの長さ
  integer,allocatable::elm(:,:) !elm(i,j): j番節点のi番要素
  ! real(8)::x1(2),x2(2),x3(2),x4(2)
  integer::idx(0:6),intmp
  real(8),allocatable::xip(:,:)

  interface
     function ltc(s,g) result(out)
       real(8) :: s,g
       integer :: out
     end function ltc

     function cross(x1,x2,x3,x4) result(out)
       real(8) :: x1(2),x2(2),x3(2),x4(2)
       integer :: out
     end function cross     
  end interface

  idx=(/ 0, 1, 2, 3, 0, 1, 2/)  
  
  ! レベルセット関数の読み込み
  ! nlsf=(xe+1)*(ye+1)            !レベルセット関数の格子点数
  allocate(lsfunc(0:xe,0:ye))
  allocate(xlsf(2,nlsf))
  allocate(xip(2,nlsf))
  open(10,file=file_lsf)
  intmp=0
  nip=0
  do iy=0,ye
     do ix=0,xe
        intmp=intmp+1           !固定設計領域の点の番号
        read(10,*) xlsf(:,intmp),lsfunc(ix,iy)
        ! lsfunc(ix,iy)=lsfunce(ix,iy)+offset
        ! if(abs(lsfunc(ix,iy))<eps2)lsfunc(ix,iy)=0.0d0
        ! !↑要検討↑ これがないとwwのあたりでbugるらしいが...ホンマか?
        if(abs(lsfunc(ix,iy))<eps2)stop "ERROR: levelset function nearly equals to zero"
        if(ix.ne.0.and.ix.ne.xe.and.iy.ne.0.and.iy.ne.ye.and.lsfunc(ix,iy) > 0.d0)then
           nip=nip+1                !内点の数
           xip(:,nip)=xlsf(:,intmp) !内点
        end if
        ! if(ix.eq.0.or.ix.eq.xe.or.iy.eq.0.or.iy.eq.ye)then
        !    if(lsfunc(ix,iy).le.0.d0)write(82,*) ix,iy
        ! else
        !    if(lsfunc(ix,iy).le.0.d0)then
        !       write(82,*) ix,iy
        !    else                        !物体領域
        !       nip=nip+1                !内点の数
        !       xip(:,nip)=xlsf(:,intmp) !内点
        !    end if
        ! end if
     end do
  end do
  close(10)

  menseki=dble(nip)/dble(nlsf)*avec(1,1)*avec(2,2)   !物体領域の面積

  open(10,file=file_ip2)
  write(10,*) nip
  do i=1,nip
     ! grid(:,i)=xip(:,i)
     write(10,fmt_i1d2) i,xip(:,i)! grid(:,i)
  end do
  close(10)
  deallocate(xip)

  allocate(nodepos(2,xe*ye*4),element(2,xe*ye*2),refvec(2,xe*ye*2))
  allocate(l2epre(0:xe,0:ye),e2lpre(xe*ye*2))
  do i=1,xe*ye*2
     allocate(e2lpre(i)%ltc(2,1))
  end do
  nodepos(:,:)=0.d0 !nodepos(j,i):i番節点のx_j座標
  element(:,:)=0

  nnode=0
  nelement=0
  cnt_max=0
  do iy=0,ye-1    !voxelの左下の節点のloop
     do ix=0,xe-1 !voxelの左下の節点のloop
        allocate(l2epre(ix+1,iy+1)%elm(2))
        allocate(l2epre(ix+1,iy+1)%start(2))
        allocate(l2epre(ix+1,iy+1)%goal(2))
        l2epre(ix+1,iy+1)%start(:)=-1
        l2epre(ix+1,iy+1)%goal(:)=-1
        cnt_nn=0     !
        lsf(0)=lsfunc(ix,  iy  ) !voxelの左下の節点のlevel set関数
        lsf(1)=lsfunc(ix+1,iy  ) !voxelの右下の節点のlevel set関数
        lsf(2)=lsfunc(ix+1,iy+1) !voxelの右上の節点のlevel set関数
        lsf(3)=lsfunc(ix,  iy+1) !voxelの左上の節点のlevel set関数

        cnt_z=0 !voxerl内でレベルセット関数がゼロの節点の数
        cnt_p=0 !voxerl内でレベルセット関数が正の節点の数
        cnt_m=0 !voxerl内でレベルセット関数が負の節点の数
        do i=0,3
           if(dabs(lsf(i)).le.0.d0) then
              cnt_z=cnt_z+1
           elseif(lsf(i).ge.0.d0) then
              cnt_p=cnt_p+1
           else
              cnt_m=cnt_m+1
           end if
        end do

        if((cnt_z.eq.1.and.cnt_m.eq.3))cycle !当該voxelの中に要素は無い
        if(cnt_m.eq.4) cycle !これも上と同じような...?

        do i=0,3 !voxel内の節点loop
           if(gridtype.eq.1)then !原点を左下に取る場合
              if(i==0.or.i==3) au=dble(ix)*l1/dble(xe)
              if(i==1.or.i==2) au=dble(ix+1)*l1/dble(xe)
              if(i==0.or.i==1) av=dble(iy)*l2/dble(ye)
              if(i==2.or.i==3) av=dble(iy+1)*l2/dble(ye)
           else if(gridtype.eq.2)then !原点を中心に取る場合
              if(i==0.or.i==3) au=dble(ix)*l1/dble(xe)-dble(l1)*0.5d0
              if(i==1.or.i==2) au=dble(ix+1)*l1/dble(xe)-dble(l1)*0.5d0
              if(i==0.or.i==1) av=dble(iy)*l2/dble(ye)-dble(l2)*0.5d0
              if(i==2.or.i==3) av=dble(iy+1)*l2/dble(ye)-dble(l2)*0.5d0
           end if

           j=mod(i+1,4) !一つ先の(local)節点番号

           f1=lsf(i)
           f2=lsf(j)

           if (((f1>=0.d0).and.(f2<0.d0)).or.((f1<0.d0).and.(f2>=0.d0))) then ! 辺の両端の符号が違ったら
              ww=abs(lsf(i)/(lsf(i)-lsf(j))) ! 節点iからの距離 lsf(i):x=(lsf(i)-lsf(j)):1
              ! if( ww>0.85d0 ) ww=0.85d0
              ! if( ww<0.15d0 ) ww=0.15d0
              ! 節点座標計算
              if( i==0 ) au=au+ww*l1/dble(xe)
              if( i==1 ) av=av+ww*l2/dble(ye)
              if( i==2 ) au=au-ww*l1/dble(xe)
              if( i==3 ) av=av-ww*l2/dble(ye)
              ! 節点カウント
              cnt_nn=cnt_nn+1 !voxel内の境界要素の節点の数
              nnode=nnode+1
              nodepos(1,nnode)=au
              nodepos(2,nnode)=av
           end if
        end do

       ! 要素追加
        if(cnt_nn.eq.2)then
           nelement=nelement+1
           element(1,nelement)=nnode-1 !element(j,i):i番要素の第一節点のグローバル番号
           element(2,nelement)=nnode
           l2epre(ix+1,iy+1)%n=1
           l2epre(ix+1,iy+1)%elm(1)=nelement
           e2lpre(nelement)%n=1
           e2lpre(nelement)%ltc(1,1)=ix+1
           e2lpre(nelement)%ltc(2,1)=iy+1
           imaxlsf=0; dmaxlsf=lsf(0)
           iminlsf=0; dminlsf=lsf(0)
           do i=1,3
              if(lsf(i).gt.dmaxlsf) then
                 imaxlsf=i
                 dmaxlsf=lsf(i)
              end if
              if(lsf(i).lt.dminlsf) then
                 iminlsf=i
                 dminlsf=lsf(i)
              end if
           end do
           if(cnt_m.eq.1)then
              refvec(1,nelement)=dble(min(mod(iminlsf,3),1))-0.5d0
              refvec(2,nelement)=dble(iminlsf/2)-0.5d0
              l2epre(ix+1,iy+1)%start(1)=idx(iminlsf)
              l2epre(ix+1,iy+1)%goal(1) =idx(iminlsf+3)
           elseif(cnt_p.eq.1)then
              refvec(1,nelement)=0.5d0-dble(min(mod(imaxlsf,3),1))
              refvec(2,nelement)=0.5d0-dble(imaxlsf/2)
              l2epre(ix+1,iy+1)%start(1)=idx(imaxlsf+3)
              l2epre(ix+1,iy+1)%goal(1) =idx(imaxlsf)
           elseif(cnt_p.eq.2.and.cnt_m.eq.2)then
              refvec(1,nelement)=dble(min(mod(iminlsf,3),1))-0.5d0
              refvec(2,nelement)=dble(iminlsf/2)-0.5d0
              if(lsf(idx(iminlsf+1)).lt.0.d0) then
                 l2epre(ix+1,iy+1)%start(1)=idx(iminlsf+1)
                 l2epre(ix+1,iy+1)%goal(1) =idx(iminlsf+3)
              else if(lsf(idx(iminlsf+3)).lt.0.d0) then
                 l2epre(ix+1,iy+1)%start(1)=idx(iminlsf)
                 l2epre(ix+1,iy+1)%goal(1) =idx(iminlsf+2)
              end if
           else
              write(*,*) cnt_p,cnt_m,cnt_z,ix,iy,lsf(:)
              stop
           end if
        else if( cnt_nn.eq.4 )then
           lsfs=lsf(0)+lsf(1)+lsf(2)+lsf(3)
           if(lsfs.ge.0.0d0.and.lsf(0).ge.0.0d0)then
              l2epre(ix+1,iy+1)%n=2
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode-2
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=-1.d0
              l2epre(ix+1,iy+1)%elm(1)=nelement
              l2epre(ix+1,iy+1)%start(1)=1
              l2epre(ix+1,iy+1)%goal(1) =0
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
              
              nelement=nelement+1
              element(1,nelement)=nnode-1
              element(2,nelement)=nnode
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=1.d0
              l2epre(ix+1,iy+1)%elm(2)=nelement
              l2epre(ix+1,iy+1)%start(2)=3
              l2epre(ix+1,iy+1)%goal(2) =2
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
           elseif(lsfs.le.0.d0.and.lsf(0).le.0.d0)then
              l2epre(ix+1,iy+1)%n=2
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode-2
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=1.d0
              l2epre(ix+1,iy+1)%elm(1)=nelement
              l2epre(ix+1,iy+1)%start(1)=0
              l2epre(ix+1,iy+1)%goal(1) =1
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
              
              nelement=nelement+1
              element(1,nelement)=nnode-1
              element(2,nelement)=nnode
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=-1.d0
              l2epre(ix+1,iy+1)%elm(2)=nelement
              l2epre(ix+1,iy+1)%start(2)=2
              l2epre(ix+1,iy+1)%goal(2)=3
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
           elseif(lsfs.ge.0.d0.and.lsf(0).le.0.d0)then
              l2epre(ix+1,iy+1)%n=2
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=-1.d0
              l2epre(ix+1,iy+1)%elm(1)=nelement
              l2epre(ix+1,iy+1)%start(1)=0
              l2epre(ix+1,iy+1)%goal(1) =3
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
              
              nelement=nelement+1
              element(1,nelement)=nnode-2
              element(2,nelement)=nnode-1
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=1.d0
              l2epre(ix+1,iy+1)%elm(2)=nelement
              l2epre(ix+1,iy+1)%start(2)=2
              l2epre(ix+1,iy+1)%goal(2)=1
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
           elseif(lsfs.le.0.d0.and.lsf(0).ge.0.d0)then
              l2epre(ix+1,iy+1)%n=2
              nelement=nelement+1
              element(1,nelement)=nnode-3
              element(2,nelement)=nnode
              refvec(1,nelement)=1.d0
              refvec(2,nelement)=1.d0
              l2epre(ix+1,iy+1)%elm(1)=nelement
              l2epre(ix+1,iy+1)%start(1)=3
              l2epre(ix+1,iy+1)%goal(1) =0
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
              
              nelement=nelement+1
              element(1,nelement)=nnode-2
              element(2,nelement)=nnode-1
              refvec(1,nelement)=-1.d0
              refvec(2,nelement)=-1.d0
              l2epre(ix+1,iy+1)%elm(2)=nelement
              l2epre(ix+1,iy+1)%start(2)=1
              l2epre(ix+1,iy+1)%goal(2)=2
              e2lpre(nelement)%n=1
              e2lpre(nelement)%ltc(1,1)=ix+1
              e2lpre(nelement)%ltc(2,1)=iy+1
           end if
        end if
     end do
  end do

  write(*,*) "# nnode (before integration)=",nnode !統合前の節点数
  write(*,*) "# nelement (before integration)",nelement !統合前の要素数


  ! 重複する節点の統合 
  do i=1,nnode-1
     xi=nodepos(1,i)
     yi=nodepos(2,i)
     do j=i+1,nnode
        xj=nodepos(1,j)
        yj=nodepos(2,j)
        if(abs(xi-xj)<eps1.and.abs(yi-yj)<eps1) then ! iとjが一致したら
           do k=j,nnode-1
              nodepos(:,k)=nodepos(:,k+1) ! j+1 以降の点も一つずつ前にずらす
           end do
           do k=1,nelement
              do l=1,2
                 if(element(l,k).eq.j) then
                    element(l,k)=i ! 要素の節点jをiに置換し
                 else if(element(l,k).gt.j) then
                    element(l,k)=element(l,k)-1 ! j+1以降の点を一つずつ前にずらす
                 end if
              end do
           end do
           nnode=nnode-1 ! 節点が一つ減った
        end if
     end do
  end do


  ! do i=1,nelement !境界要素の絵を描く (w vec)
  !    write(*,*) i
  !    write(88,*) nodepos(:,element(1,i)),nodepos(:,element(2,i))-nodepos(:,element(1,i))
  ! end do

  allocate(el%an(2,nelement),el%at(2,nelement))
! 法線を作る
  do i=1,nelement
     anorm=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))**2+(nodepos(2,element(2,i))-nodepos(2,element(1,i)))**2
     anorm=1d0/sqrt(anorm)
     el%at(1,i)=(nodepos(1,element(2,i))-nodepos(1,element(1,i)))*anorm
     el%at(2,i)=(nodepos(2,element(2,i))-nodepos(2,element(1,i)))*anorm
     el%an(1,i)= el%at(2,i)
     el%an(2,i)=-el%at(1,i)
     if(el%an(1,i)*refvec(1,i)+el%an(2,i)*refvec(2,i).le.0.d0)then ! 向きが揃っていなければ
        itmp=element(1,i) !要素番号を入れ替え
        element(1,i)=element(2,i)
        element(2,i)=itmp
        el%an(1,i)=-el%an(1,i)  !法線をひっくり返す
        el%an(2,i)=-el%an(2,i)
        el%at(1,i)=-el%at(1,i)  !接線もひっくり返す
        el%at(2,i)=-el%at(2,i)  
     end if
  enddo

!!$ 要素チェック
  allocate(elml(nelement))
  do i=1,nelement !要素長
     elml(i)=sqrt(dot_product(&
          nodepos(:,element(2,i))-nodepos(:,element(1,i)),&
          nodepos(:,element(2,i))-nodepos(:,element(1,i))))
  end do
  write(*,*) "# Original mesh"
  write(*,*) '## nnode=',nnode !統合後の節点数
  write(*,*) '## nelement=',nelement !統合後の要素数
  write(*,*) "## (Longest edge)=",maxval(elml)
  write(*,*) "## (Shortest edge)=",minval(elml)
  write(*,*) "## (Longest edge)/(Shortest edge)=",maxval(elml)/minval(elml)

  ! open(1,file='mesh_pre')
  ! do i=1,nnode
  !    write(1,*) nodepos(:,element(1,i))
  !    write(1,*) nodepos(:,element(2,i))
  !    write(1,*) 
  ! end do
  ! close(1)
  ! stop

  ! do i=1,nelement !要素改善後の境界要素の絵を描く
  !    do j=1,2
  !       write(85,'(2e16.8)') nodepos(:,element(j,i))
  !    end do
  !    write(85,*) 
  ! end do
  ! do i=1,nelement !境界要素の絵を描く (w vec)
  !    write(85,*) nodepos(:,element(1,i)),nodepos(:,element(2,i))-nodepos(:,element(1,i))
  ! end do
  ! stop

  allocate(elm(2,nnode)) !elm(i,j): j番節点のi番要素を返す配列を作る
  do i=1,nnode
     do j=1,nelement
        if(i.eq.element(1,j))then
           elm(1,i)=j
           goto 100
        elseif(i.eq.element(2,j))then
           elm(2,i)=j
           goto 100
        end if
100     continue
     end do
  end do

  allocate(nd4at(4,nnode),orgat(2,nnode),ltc2nd(0:3,xe,ye))
  ltc2nd(:,:,:)=0
  do i=1,nnode
     orgat(:,i)=elml(elm(1,i))*el%at(:,elm(1,i))+elml(elm(2,i))*el%at(:,elm(2,i))
     anorm=1.d0/sqrt(dot_product(orgat(:,i),orgat(:,i)))
     orgat(:,i)=orgat(:,i)*anorm

     ix=e2lpre(elm(1,i))%ltc(1,1)
     iy=e2lpre(elm(1,i))%ltc(2,1)
     do j=1,l2epre(ix,iy)%n
        if(l2epre(ix,iy)%elm(j).eq.elm(1,i)) then
           ltc2nd(l2epre(ix,iy)%start(j),ix,iy)=i
        end if
     end do
     nd4at(3:4,i)=nodepos(:,element(2,elm(1,i)))

     ix=e2lpre(elm(2,i))%ltc(1,1)
     iy=e2lpre(elm(2,i))%ltc(2,1)
     do j=1,l2epre(ix,iy)%n
        if(l2epre(ix,iy)%elm(j).eq.elm(2,i)) then
           ltc2nd(l2epre(ix,iy)%goal(j),ix,iy)=i
        end if
     end do
     nd4at(1:2,i)=nodepos(:,element(1,elm(2,i)))
  end do
  deallocate(elml)

  ! 要素改善
  call improve_mesh(nnode,nelement,nodepos,element,eps1,iflag_bounded)
  
  ! open(1,file=file_elm)
  ! write(1,*) nnode
  ! do i=1,nnode
  !    write(1,*) i,nodepos(:,i)
  ! end do
  ! write(1,*) nelement
  ! do i=1,nelement
  !    write(1,*) i,element(:,i)
  ! end do
  ! close(1)

  deallocate(el%an,el%at)

end subroutine lsf2elm
