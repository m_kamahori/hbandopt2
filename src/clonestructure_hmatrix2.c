#include <stdio.h>
/* #include "settings.h" */
#include "hmatrix.h"
#include "harith.h"
#include "rkmatrix.h"
#include "amatrix.h"



prkmatrix
init_rkmatrix2(prkmatrix r, uint rows, uint cols, uint k)
{
  init_zero_amatrix(&r->A, rows, k);
  init_zero_amatrix(&r->B, cols, k);
  r->k = k;

  return r;
}

prkmatrix
new_rkmatrix2(uint rows, uint cols, uint k)
{
  prkmatrix r;

  r = (prkmatrix) allocmem(sizeof(rkmatrix));

  return init_rkmatrix2(r, rows, cols, k);
}

pamatrix
new_amatrix2(uint rows, uint cols)
{
  pamatrix  a;

  a = (pamatrix) allocmem(sizeof(amatrix));

  init_zero_amatrix(a, rows, cols);

  return a;
}

phmatrix
new_full_hmatrix2(pccluster rc, pccluster cc)
{
  phmatrix  hm;

  hm = new_hmatrix(rc, cc);

  hm->f = new_amatrix2(rc->size, cc->size);

  hm->desc = 1;

  return hm;
}

phmatrix
new_rk_hmatrix2(pccluster rc, pccluster cc, uint k)
{
  phmatrix  hm;

  hm = new_hmatrix(rc, cc);

  hm->r = new_rkmatrix2(rc->size, cc->size, k);

  hm->desc = 1;

  return hm;
}

phmatrix
clonestructure_hmatrix2(pchmatrix src)
{
  const uint rsons = src->rsons;
  const uint csons = src->csons;

  phmatrix  hm1;
  uint      i, j;

  phmatrix  hm;

  if (src->son != NULL) {
    hm = new_super_hmatrix(src->rc, src->cc, rsons, csons);
    for (j = 0; j < csons; ++j) {
      for (i = 0; i < rsons; ++i) {
	hm1 = clonestructure_hmatrix2(src->son[i + j * rsons]);
	ref_hmatrix(hm->son + i + j * rsons, hm1);
      }
    }
  }
  else if (src->r != NULL) {
    hm = new_rk_hmatrix2(src->rc, src->cc, src->r->k);
  }
  else {
    /* printf("direct!\n"); */
    assert(src->f != NULL);
    hm = new_full_hmatrix2(src->rc, src->cc);
  }

  update_hmatrix(hm);

  return hm;
}

/* 構造が同じhmatを作成(zeroで初期化)*/
phmatrix
clone_struct_zero_hmat(pchmatrix origin){
  phmatrix tmp;

  /* tmp = (phmatrix *) allocmem((size_t)sizeof(phmatrix)); */
  /* clone=clone_hmatrix(origin); */
  /* clone=clonestructure_hmatrix(origin); */
  /* printf("@clone_struct_hmat:origin->rsons,csons:%d,%d\n",origin->rsons,origin->csons); */
  /* printf(" origin->son[0]->rsons,csons=%d,%d\n",origin->son[0]->rsons,origin->son[0]->csons); */
  /* assert(origin->son[0]->rsons != 0); */
  /* assert(origin->son[0]->csons != 0); */
  /* tmp=clone_hmatrix(origin); */
  tmp=clonestructure_hmatrix2(origin);

  return tmp;
}
