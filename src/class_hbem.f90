module class_hbem
  use iso_c_binding
  use globals
  implicit none
  
  interface
     function mkcluster(edges, vertices, x, nd, nmin, an, lngth) result(out) bind(c)
       use iso_c_binding
       integer(c_int),value :: edges ! 要素数
       integer(c_int),value :: vertices ! 接点数
       real(c_double)::x(vertices,2)
       integer(c_int)::nd(vertices,2)
       integer(c_int),value :: nmin
       real(c_double)::an(vertices,2)
       real(c_double)::lngth(vertices)
       type(c_ptr) :: out ! pcluster型
     end function

     function mkclusterin(nmin, nip, xip) result(out) bind(c)
       use iso_c_binding
       integer(c_int),value :: nmin ! 要素数
       integer(c_int),value :: nip
       real(c_double) :: xip(nip,2)
       type(c_ptr) :: out ! pcluster型
     end function

     function integrate_cluster(n, clusters) result(out) bind(c)
       use iso_c_binding
       integer(c_int),value :: n ! integrateするclusterの数
       type(c_ptr) :: clusters(n) ! pcluster型の配列
       type(c_ptr) :: out ! pcluster型
     end function

     function mkhmat(rc, cc, eta, eps_aca, bflag, pflag, dflag, wn) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: rc
       type(c_ptr),value :: cc
       real(c_double) :: eta
       real(c_double),value :: eps_aca
       integer(c_int),value :: bflag
       integer(c_int),value :: pflag
       integer(c_int),value :: dflag
       complex(c_double),value :: wn
       type(c_ptr) :: out ! phmatrix型
     end function

     function mkhmat_peri(root1, root2, root3, eta, eps_aca, bflag, pflag, dflag,&
          wn) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: root1, root2, root3
       real(c_double) :: eta
       real(c_double),value :: eps_aca
       integer(c_int),value :: bflag, pflag, dflag
       complex(c_double),value :: wn
       type(c_ptr) :: out ! phmatrix型
     end function

     function integrate_hmatrix(rc, cc, hmats) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: rc
       type(c_ptr),value :: cc
       type(c_ptr),value :: hmats
       type(c_ptr) :: out ! phmatrix型
     end function

     function integrate_hmatrix2(rc, cc, nhmat, hmats) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: rc
       type(c_ptr),value :: cc
       integer(c_int),value :: nhmat
       type(c_ptr) :: hmats(nhmat)
       type(c_ptr) :: out ! phmatrix型
     end function

     ! function integrate_hmatrix1(sons, root, nhmat, hmats) result(out) bind(c)
     !   use iso_c_binding
     !   integer(c_int),value :: sons, nhmat
     !   type(c_ptr),value :: root, hmats
     !   type(c_ptr) :: out ! phmatrix型
     ! end function

     function clone_struct_zero_hmat(origin) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: origin
       type(c_ptr) :: out ! phmatrix型
     end function

     function clone_hmatrix(origin) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: origin
       type(c_ptr) :: out ! phmatrix型
     end function

     function invert_hmat(hmat, tol) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: hmat
       real(c_double),value :: tol
       type(c_ptr) :: out       !phmatrix
     end function invert_hmat

     ! curve2d.c
     function build_from_el2_curve2d(edges, vertices, x, nd, nmin, an, lngth) result(out) bind(c)
       use iso_c_binding
       integer(c_int),value :: edges ! 要素数
       integer(c_int),value :: vertices ! 接点数
       type(c_ptr),value :: x
       type(c_ptr),value :: nd
       integer(c_int),value :: nmin
       type(c_ptr),value :: an
       type(c_ptr),value :: lngth
       type(c_ptr) :: out ! pcluster型
     end function build_from_el2_curve2d

     ! bem2d.c
     function new_bem2d(gr) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: gr
       type(c_ptr) :: out
     end function new_bem2d

     ! cluster.c
     function new_constant_bem2d_cluster(bem, nmin) result(out) bind(c)
       use iso_c_binding
       type(c_ptr),value :: bem
       integer(c_int),value :: nmin
       type(c_ptr) :: out ! pcluster型
     end function new_constant_bem2d_cluster

     type(c_ptr) function build_innerpoint_bem2d_clustergeometry(n, x) bind(c)
       use iso_c_binding
       integer(c_int),value :: n
       type(c_ptr),value :: x
     end function build_innerpoint_bem2d_clustergeometry

     type(c_ptr) function new_innerpoint_bem_cluster(cf, nmin) bind(c)
       use iso_c_binding
       type(c_ptr),value :: cf
       integer(c_int),value :: nmin
     end function new_innerpoint_bem_cluster

     subroutine mkhmat_peri2(root1, root2, root3, eta, eps_aca, bflag, pflag, dflag, wn, hmats) bind(c)
       use iso_c_binding
       type(c_ptr),value :: root1, root2, root3
       real(c_double) :: eta
       real(c_double),value :: eps_aca
       integer(c_int),value :: bflag, pflag, dflag
       complex(c_double),value :: wn
       type(c_ptr) :: hmats ! phmatrix型
     end subroutine mkhmat_peri2
     
     subroutine mkhmat_peri3(root1, root2, root3, eta, eps_aca, bflag, pflag, dflag, wn, nhmat, hmats) bind(c)
       use iso_c_binding
       type(c_ptr),value :: root1, root2, root3
       real(c_double) :: eta
       real(c_double),value :: eps_aca
       integer(c_int),value :: bflag, pflag, dflag
       complex(c_double),value :: wn
       integer(c_int),value :: nhmat
       type(c_ptr) :: hmats(nhmat) ! phmatrix型
     end subroutine mkhmat_peri3
     
     subroutine mkhmatin(root1, root2, eta, eps_aca, hrows, hcols, dflag, nin, ne,&
          xfwd, yfwd) bind(c)
       use iso_c_binding
       type(c_ptr),value :: root1, root2
       real(c_double) :: eta
       real(c_double),value :: eps_aca
       integer(c_int),value :: hrows, hcols
       integer(c_int) :: dflag(hrows*hcols)
       integer(c_int),value :: nin, ne
       complex(c_double) :: xfwd(2*ne), yfwd(3*nin)
     end subroutine mkhmatin

     subroutine coarsen_hmat(G, tm, eps) bind(c)
       use iso_c_binding
       type(c_ptr),value :: G
       type(c_ptr),value :: tm
       real(c_double),value :: eps
     end subroutine coarsen_hmat

     subroutine lrdecomp_hmatrix(a, tm, eps) bind(c)
       use iso_c_binding
       type(c_ptr),value :: a ! phmatrix
       type(c_ptr),value :: tm ! pctruncmode
       real(c_double),value :: eps
     end subroutine lrdecomp_hmatrix

     subroutine lusolve_hmatrix(atrans, hmat, brows, bcols, btmp) bind(c)
       use iso_c_binding
       logical(c_bool),value :: atrans
       type(c_ptr),value :: hmat ! phmatrix
       integer(c_int),value :: brows, bcols
       complex(c_double) :: btmp(bcols,brows)
     end subroutine lusolve_hmatrix

     ! subroutine invert_hmat(hmat, tol) bind(c)
     !   use iso_c_binding
     !   type(c_ptr),value :: hmat
     !   real(c_double),value :: tol
     ! end subroutine invert_hmat

     subroutine addmul_hmatrix(alpha, xtrans, x, ytrans, y, tm, eps, z) bind(c)
       use iso_c_binding
       complex(c_double),value :: alpha
       logical(c_bool),value :: xtrans, ytrans
       type(c_ptr),value :: x, y
       type(c_ptr),value :: tm
       real(c_double),value :: eps
       type(c_ptr),value :: z
     end subroutine addmul_hmatrix

     subroutine addmul_hmat_amat_amat(alpha, lda, ldb, rhb, atrans, a, btrans, b, ctrans, c) bind(c)
       use iso_c_binding
       complex(c_double),value :: alpha
       integer(c_int),value :: lda, ldb, rhb
       logical(c_bool),value :: atrans, btrans, ctrans
       type(c_ptr),value :: a
       complex(c_double) :: b(rhb,ldb),c(rhb,lda)
     end subroutine addmul_hmat_amat_amat

     subroutine add_hmat_amat(alpha, ldb, rhb, atrans, a, b) bind(c)
       use iso_c_binding
       complex(c_double),value :: alpha
       integer(c_int),value :: ldb, rhb
       logical(c_bool),value :: atrans
       type(c_ptr),value :: a
       complex(c_double) :: b(rhb,ldb)
     end subroutine add_hmat_amat

     subroutine add_hmatrix(alpha, a, tm, eps, b) bind(c)
       use iso_c_binding
       complex(c_double),value :: alpha
       type(c_ptr),value :: a, b, tm
       real(c_double),value :: eps
     end subroutine add_hmatrix

     subroutine del_hmatrix(hm) bind(c)
       use iso_c_binding
       type(c_ptr),value :: hm
     end subroutine del_hmatrix

     subroutine del_curve2d(gr) bind(c)
       use iso_c_binding
       type(c_ptr),value :: gr
     end subroutine del_curve2d

     subroutine del_bem2d(bem) bind(c)
       use iso_c_binding
       type(c_ptr),value :: bem
     end subroutine del_bem2d

     subroutine del_cluster(t) bind(c)
       use iso_c_binding
       type(c_ptr),value :: t ! pcluster型
     end subroutine del_cluster

     subroutine del_idx_cluster(t) bind(c)
       use iso_c_binding
       type(c_ptr),value :: t ! pcluster型
     end subroutine del_idx_cluster

     subroutine w_check_cluster(root) bind(c)
       use iso_c_binding
       type(c_ptr),value :: root ! pcluster型
     end subroutine w_check_cluster

     ! subroutine del_cluster(t) bind(c)
     !   use iso_c_binding
     !   type(c_ptr),value :: t
     ! end subroutine del_cluster

     ! function test(f) result(out) bind(c)
     !   use iso_c_binding
     !   type(c_funptr),value :: f
     !   !complex(c_double) :: out
     !   type(c_ptr) :: out
     ! end function test

     ! subroutine del_cluster(t) bind(c)
     !   use iso_c_binding
     !   type(c_ptr),value :: t ! pcluster型
     ! end subroutine del_cluster

     ! subroutine del_hmatrix(hm) bind(c)
     !   use iso_c_binding
     !   type(c_ptr),value :: hm
     ! end subroutine del_hmatrix
     
  end interface

  type :: cluster
     integer :: n ! = ne = np
     integer :: nmin
     real(8),pointer :: p(:,:) ! 節点
     integer,pointer :: nd(:,:)
     real(8),pointer :: an(:,:) ! 法線ベクトル
     real(8),pointer :: le(:) ! 要素長
     type(c_ptr) :: gr
     type(c_ptr) :: bem
     type(c_ptr) :: root_cluster ! root cluster
  end type cluster
  type(cluster),target::bc(0:5)

  type :: cluster_in
     integer :: nin             !内点の数
     integer :: nmin
     real(8),pointer :: xin(:,:) ! 内点
     type(c_ptr) :: cg_in
     type(c_ptr) :: root_cluster_in ! root cluster
  end type cluster_in
  type(cluster_in)::ic


contains

  type(cluster) function new_cluster(n, ne, p, nd, an, le, nmin)
    integer,intent(in) :: n,ne
    real(8),intent(in) :: p(2,ne)
    integer,intent(in) :: nd(2,n)
    real(8),intent(in) :: an(2,ne)
    real(8),intent(in) :: le(ne)
    integer,intent(in) :: nmin
    ! real(8),intent(in) :: eta

    integer,pointer :: nd_tmp(:,:)

    new_cluster%n = n
    allocate(new_cluster%p(2,ne))
    new_cluster%p(:,:) = p(:,:)
    allocate(new_cluster%nd(2,n))
    new_cluster%nd(:,:) = nd(:,:)
    allocate(new_cluster%an(2,ne))
    new_cluster%an(:,:) = an(:,:)
    allocate(new_cluster%le(ne))
    new_cluster%le(:) = le(:)
    
    new_cluster%nmin = nmin
    ! new_cluster%eta = eta

    ! C用にndをコピーしてindexを1減らす
    allocate(nd_tmp(2,n))
    nd_tmp(:,:) = new_cluster%nd(:,:) - 1

    ! do i=1,n
    !    write(*,*) nd_tmp(:,i),new_cluster%nd(:,i)
    !    write(89,*)new_cluster%p(:,nd_tmp(1,i))
    !    write(89,*)new_cluster%p(:,nd_tmp(2,i))
    !    write(89,*)
    ! end do

    ! do i=1,240
    !    write(90,*)new_cluster%p(:,i)
    ! end do
    
    
    ! write(*,*) new_cluster%n,new_cluster%p(1,1),nd_tmp(1,1)

    ! curve2d構造体の生成
    ! new_cluster%gr = build_from_el2_curve2d(new_cluster%n,new_cluster%n,c_loc(new_cluster%p),&
    !      c_loc(nd_tmp),new_cluster%nmin, c_loc(new_cluster%an), c_loc(new_cluster%le(1)))
    new_cluster%gr = build_from_el2_curve2d(new_cluster%n,new_cluster%n,c_loc(new_cluster%p(1,1)),&
         c_loc(nd_tmp(1,1)),new_cluster%nmin, c_loc(new_cluster%an(1,1)), c_loc(new_cluster%le(1)))

    ! bem2d構造体の生成
    new_cluster%bem = new_bem2d(new_cluster%gr)

    ! root cluster treeの生成
    new_cluster%root_cluster = new_constant_bem2d_cluster(new_cluster%bem, new_cluster%nmin)

  end function new_cluster

  subroutine generate_cluster
    integer::i
    type(c_ptr),target :: clusters(0:5)

    ! cluster for boundary
    bc(0)=new_cluster(ne      ,ne,p,nd                     ,an,lngth,nmin)
    bc(1)=new_cluster(edges(1),ne,p,nd                     ,an,lngth,nmin)
    bc(2)=new_cluster(edges(2),ne,p,nd(:,edges(1)+1)       ,an,lngth,nmin)
    bc(3)=new_cluster(edges(3),ne,p,nd(:,sum(edges(1:2))+1),an,lngth,nmin)
    bc(4)=new_cluster(edges(4),ne,p,nd(:,sum(edges(1:3))+1),an,lngth,nmin)
    bc(5)=new_cluster(edges(5),ne,p,nd(:,sum(edges(1:4))+1),an,lngth,nmin)


    ! call w_check_cluster(bc(5)%root_cluster)
    ! stop

    do i=0,5
       clusters(i)=bc(i)%root_cluster
    end do
    peri_cluster=integrate_cluster(4,clusters(2:5)) !\Gamma_D cluster
    irep_cluster=integrate_cluster(1,clusters(1)) !\Gamma_I cluster
    ! peri_cluster=integrate_cluster(4,c_loc(clusters(2))) !\Gamma_D cluster
    ! irep_cluster=integrate_cluster(1,c_loc(clusters(1))) !\Gamma_I cluster

    clusters(0)=irep_cluster
    clusters(1)=peri_cluster
    all_cluster=integrate_cluster(2,clusters) !\Gamma cluster
    ! all_cluster=integrate_cluster(2,c_loc(clusters(0))) !\Gamma cluster

  end subroutine generate_cluster

  type(cluster_in) function new_cluster_in(nin, xin, nmin)
    integer,intent(in) :: nin
    real(8),intent(in) :: xin(2,nin)
    integer,intent(in) :: nmin

    new_cluster_in%nin = nin
    allocate(new_cluster_in%xin(2,nin))
    new_cluster_in%xin(:,:) = xin(:,:)
    
    new_cluster_in%nmin = nmin

    new_cluster_in%cg_in = build_innerpoint_bem2d_clustergeometry(nin, c_loc(new_cluster_in%xin(1,1)))
    new_cluster_in%root_cluster_in = new_innerpoint_bem_cluster(new_cluster_in%cg_in, new_cluster_in%nmin)

  end function new_cluster_in

  subroutine delete_clusters(n,c)
    integer::i
    integer,intent(in)::n
    type(cluster),intent(inout) :: c(n)

    do i=1,n
       ! c,ndはFortranでdeallocate (p,an,leはdel_curve2dでdeallocateされる)
       if(associated(c(i)%nd)) deallocate(c(i)%nd)
       if(c_associated(c(i)%gr)) call del_curve2d(c(i)%gr)
       if(c_associated(c(i)%bem)) call del_bem2d(c(i)%bem)
       if(c_associated(c(i)%root_cluster)) then
          call del_idx_cluster(c(i)%root_cluster)
          call del_cluster(c(i)%root_cluster)
       end if
    end do

  end subroutine delete_clusters

  subroutine delete_cluster_in(c)
    type(cluster_in),intent(inout) :: c
    
    ! c,nd,x_inはFortranでdeallocate (p,an,leはdel_curve2dでdeallocateされる)
    if(associated(c%xin)) deallocate(c%xin)
    if(c_associated(c%root_cluster_in)) then
       call del_idx_cluster(c%root_cluster_in)
       call del_cluster(c%root_cluster_in)
    end if
  end subroutine delete_cluster_in

  subroutine delete_hmatrices(idomp)
    integer::i
    integer,intent(in)::idomp
    if(islv.eq.1)then
       if(c_associated(hbmat(idomp)))call del_hmatrix(hbmat(idomp))
    elseif(islv.ge.2)then
       if(c_associated(hbmat(idomp)))call del_hmatrix(hbmat(idomp))
       if(c_associated(hcmat(idomp)))call del_hmatrix(hcmat(idomp))
       if(islv.eq.2)then
          if(c_associated(hcbinvmat(idomp)))call del_hmatrix(hcbinvmat(idomp))
       end if
       do i=1,4
          if(c_associated(hdmat_i(i,idomp)))call del_hmatrix(hdmat_i(i,idomp))
          if(c_associated(hdmat_d(i,idomp)))call del_hmatrix(hdmat_d(i,idomp))
       end do
       do i=1,16
          if(c_associated(hemat_i(i,idomp)))call del_hmatrix(hemat_i(i,idomp))
          if(c_associated(hemat_d(i,idomp)))call del_hmatrix(hemat_d(i,idomp))
       end do
    end if
  end subroutine delete_hmatrices

end module class_hbem
