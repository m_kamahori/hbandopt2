subroutine topological_derivative
  use globals
  use io
  implicit none
  integer::i,j,k,ibril,ix,iy,itmp,jtmp
  integer::iltc,jltc
  real(8),allocatable::td_ev(:,:,:)
  real(8),allocatable::td_sum(:)
  real(8)::dx,dy,rglrz,ltcl
  real(8),parameter::gosa=1.d-6
  character(len=32)::stepname,nx
  type(lattice)::ltc(2,2)

  
  allocate(td_ev(nip,2,nbril))
  allocate(td_sum(nip))

  td_sum(:)=0.d0
  do ibril=1,nbril
     do k=1,2
        !calculate T_{ij}
        call td_for_each_ev(nip,ds,ugrid(:,k,ibril),dugrid(:,:,k,ibril),ev_slc(k,ibril),&
             td_ev(:,k,ibril),rglrz)
     end do !k
     !トポロジー導関数を計算
     td_sum(:)=td_sum(:)+(td_ev(:,2,ibril)-td_ev(:,1,ibril))/(real(ev_slc(2,ibril))-real(ev_slc(1,ibril)))
  end do

  !トポロジー導関数が計算されたところ(naiten)をtd(:,:)に放り込む
  allocate(td(0:xe,0:xe))
  td(:,:)=0.d0
  do iy=0,ye
     do ix=0,xe
        ! dx=xelm*dble(ix)
        ! dy=yelm*dble(iy)
        dx=dble(ix)/dble(xe)*avec(1,1)+dble(iy)/dble(xe)*avec(1,2)
        dy=dble(ix)/dble(xe)*avec(2,1)+dble(iy)/dble(xe)*avec(2,2)

        do i=1,nip
           ! write(500,*) dx,dy,td(ix,iy)
           if(dx-gosa<grid(1,i) .and. grid(1,i)<dx+gosa .and. dy-gosa<grid(2,i) .and. grid(2,i)<dy+gosa)then !naiten
              td(ix,iy)=td_sum(i)
           end if
        end do
        !        if(lsfunc(ix,iy).lt.0.d0) td(ix,iy)=-td(ix,iy) !minimisation
        ! if(lsfunc(ix,iy).ge.0.d0) td(ix,iy)=-td(ix,iy) !maximisation
        ! evol=evol+abs(td(ix,iy))

     end do
  end do

  deallocate(grid,td_sum)

  call impose_symmetry_avg(xe,td(:,:))

  ! 最も外側の格子内に要素が出来ないようにする
  do iy=1,ye-1
     if(iy.eq.1.or.iy.eq.ye-1)then
        do ix=1,xe-1
           td(ix,iy)=0.d0
        end do
     else
        td(1,iy)=0.d0;td(xe-1,iy)=0.d0
     end if
  end do

  ! 境界近傍の格子点におけるトポロジー導関数をゼロにする
  ltcl=l1/dble(xe)
  do iy=1,ye-1
     do ix=1,xe-1
        itmp=ix+(xe+1)*iy+1
        ! 格子点(ix,iy)を含む格子情報作成
        do i=ix,ix+1
           do j=iy,iy+1
              iltc=i-ix+1
              jltc=j-iy+1
              call lattice_info(lsfunc,ltcl,ix,iy,i,j,ltc(iltc,jltc))
              if(ltc(iltc,jltc)%flag.eq.1)td(ix,iy)=0.d0
           end do
        end do
     end do
  end do

  open(500,file=file_td)
  do iy=0,ye
     do ix=0,xe
        dx=dble(ix)/dble(xe)*avec(1,1)+dble(iy)/dble(xe)*avec(1,2)
        dy=dble(ix)/dble(xe)*avec(2,1)+dble(iy)/dble(xe)*avec(2,2)
        write(500,*) dx,dy,td(ix,iy)
     end do
     write(500,*)
  end do
  close(500)

  deallocate(td_ev,ugrid,dugrid)
contains
  subroutine td_for_each_ev(nip,ds,uin,qin,evij,td,rglrz)
    integer               ::i
    integer   ,intent(in) ::nip
    real(8)   ,intent(in) ::ds
    complex(8),intent(in) ::evij
    complex(8),intent(in) ::uin(nip),qin(2,nip)
    real(8)   ,intent(out)::td(nip),rglrz

    rglrz=0.d0

    do i=1,nip
       rglrz=rglrz+(real(uin(i))**2+aimag(uin(i))**2)*menseki/dble(nip)
       ! rglrz=rglrz+(real(uin(i))**2+aimag(uin(i))**2)*ds
    end do
    ! rglrz=1.d0/sqrt(rglrz)        !regularizer←結構アバウト?

    do i=1,nip              !topological derivative calculation
       td(i)=-2.d0*(real(qin(1,i))**2+aimag(qin(1,i))**2&
            +real(qin(2,i))**2+aimag(qin(2,i))**2)&
            +real(evij)**2*(real(uin(i))**2+aimag(uin(i))**2)
       ! td(i)=-2.d0*((rglrz*real(qin(1,i)))**2+(rglrz*aimag(qin(1,i)))**2&
       !      +(rglrz*real(qin(2,i)))**2+(rglrz*aimag(qin(2,i)))**2)&
       !      +real(evij)**2*((rglrz*real(uin(i)))**2+(rglrz*aimag(uin(i)))**2)
    end do
    td(:)=td(:)/rglrz

  end subroutine td_for_each_ev

end subroutine topological_derivative

