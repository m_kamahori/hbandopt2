subroutine normal_vectors
  use globals
  implicit none
  integer::i
  real(8)::anorm

  do i=1,ne
     anorm=(p(1,nd(2,i))-p(1,nd(1,i)))**2+(p(2,nd(2,i))-p(2,nd(1,i)))**2
     lngth(i)=sqrt(anorm)
     anorm=1d0/lngth(i)
     at(:,i)=(p(:,nd(2,i))-p(:,nd(1,i)))*anorm
     an(1,i)= at(2,i)
     an(2,i)=-at(1,i)

     !       2
     !       |
     !       |
     !       |--> n
     !       |
     !       |
     !       1

  enddo

#ifdef DETAIL
  !法線ベクトルチェック用
  do i=1,ne
     write(85,*) c(:,i)
     write(85,*) c(:,i)+an(:,i)
     write(85,*) 
     write(85,*) 
  end do
#endif

end subroutine normal_vectors
