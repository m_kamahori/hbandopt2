program main
  implicit none
  integer::n,m,mmax,nbril
  real(8)::pi,ev,l1,l2,theta,rtmp,dp
  real(8)::avec(2,2),gvec(2,2),wvtmp(2),vtmp(2)
  real(8)::g1,g2,gg1,gg2,ggg1,ggg2,gtmp
  real(8),allocatable::wv(:,:)
  integer::m1,m2,n1,n2
  integer::mm
  integer::i,j,ii,itmp
  complex(8),allocatable::amat(:,:)

  integer::lwork,info
  real(8),allocatable::rwork(:)
  complex(8),allocatable::w(:),vl(:,:),vr(:,:),work(:)
  complex(8),parameter::ione=(0.d0,1.d0)
  integer,parameter::nb=6

  pi=acos(-1.d0)
  l1=1.d0
  l2=1.d0
  mmax=2


  ! square lattice vector
  theta=pi/2.d0
  avec(1,1)=l1
  avec(2,1)=0.d0
  avec(1,2)=l2*cos(theta)
  avec(2,2)=l2*sin(theta)

  ! ! hexagonal lattice vector
  ! theta=pi/3.d0
  ! avec(1,1)=l1
  ! avec(2,1)=0.d0
  ! avec(1,2)=l2*cos(theta)
  ! avec(2,2)=l2*sin(theta)
  
  ! 逆格子ベクトル
  rtmp=-cos(theta)/(l1*l2)
  gvec(:,1)=avec(:,1)/(l1*l1)+avec(:,2)*rtmp
  gvec(:,2)=avec(:,1)*rtmp+avec(:,2)/(l2*l2)
  gvec=gvec*2.d0*pi/(sin(theta)*sin(theta))

  !   ! 第1brillouin zone の波数ベクトル
  ! nbril=3*nb*(nb+1)
  ! allocate(wv(2,nbril))
  ! vtmp(:)=(gvec(:,1)+gvec(:,2))*0.5d0/nb
  ! itmp=0
  ! do i=1,nb
  !    do j=0,nb-i
  !       itmp=itmp+1
  !       wv(:,itmp)=dble(i)/nb*gvec(:,2)*0.5d0+dble(j)*vtmp(:)
  !    end do
  ! end do
  ! do i=1,itmp
  !    write(77,*) wv(:,i)
  ! end do
  ! do i=1,5                      !copy roop
  !    do j=1,nb*(nb+1)/2
  !       wv(1,itmp+j)=cos(i*theta)*wv(1,j)-sin(i*theta)*wv(2,j)
  !       wv(2,itmp+j)=sin(i*theta)*wv(1,j)+cos(i*theta)*wv(2,j)
  !    end do
  !    itmp=itmp+nb*(nb+1)/2
  ! end do
  !  do i=1,nbril
  !    write(222,*) wv(:,i)       !first brillouin zone
  ! end do
  ! ! 第1brillouin zone の波数ベクトル


  ! allocate(wv(2,-nb:nb*2))
  ! ! 縮約された第1brillouin zone の波数ベクトル
  ! rtmp=sqrt(gvec(1,1)*gvec(1,1)+gvec(2,1)*gvec(2,1))/sqrt(3.d0) !Gamma_K no nagasa
  ! do i=-1,-nb,-1
  !    j=-i
  !    wv(:,i)=dble(j)/nb*rtmp*avec(:,1)/l1
  ! end do
  ! i=0; wv(:,i)=0.d0
  ! do i=1,nb
  !    wv(:,i)=dble(i)/nb*0.5d0*gvec(:,1)
  ! end do
  ! do i=nb+1,2*nb
  !    j=i-nb
  !    wv(:,i)=gvec(:,1)+dble(j)/nb*rtmp*avec(:,2)/l2
  !    wv(:,i)=0.5d0*wv(:,i)
  ! end do
  ! ! 縮約された第1brillouin zone の波数ベクトル

  nbril=nb*3
  allocate(wv(2,nbril))
  do i=1,nb            !K-Gamma
     wv(1,i)=dble(nb-i+1)/nb*pi/l1-pi/l1/(2*nb)
     wv(2,i)=dble(nb-i+1)/nb*pi/l2-pi/l2/(2*nb)
  end do
  do i=1,nb            !Gamma-M
     wv(1,nb+i)=dble(i)/nb*pi/l1-pi/l1/(2*nb)
     wv(2,nb+i)=0.d0
  end do
  do i=1,nb         !M-K
     wv(1,2*nb+i)=pi/l1
     wv(2,2*nb+i)=dble(i)/nb*pi/l2-pi/l2/(2*nb)
  end do

  do i=1,nbril
     write(222,*) wv(:,i)       !reduced first brillouin zone
  end do

  mm=(2*mmax+1)**2
  allocate(amat(mm,mm));amat(:,:)=dcmplx(0.d0,0.d0)
  write(*,*) mm
  lwork=2*mm
  allocate(w(mm))
  allocate(vl(1,mm))
  allocate(vr(mm,mm))
  allocate(work(lwork))
  allocate(rwork(2*mm))


  ! do ii=-nb,2*nb
  do ii=1,nbril
     write(*,*) ii
     do i=1,mm

        m1=(i-1)/(2*mmax+1)-mmax
        m2=mod(i-1,2*mmax+1)-mmax
        ! g1=sqrt(3.d0)*pi*m1     !g
        ! g2=(-m1+2.d0*m2)*pi
        g1=m1*gvec(1,1)+m2*gvec(1,2)
        g2=m1*gvec(2,1)+m2*gvec(2,2)
        write(111,*) g1,g2
        do j=1,mm
           n1=(j-1)/(2*mmax+1)-mmax
           n2=mod(j-1,2*mmax+1)-mmax

           ! gg1=sqrt(3.d0)*pi*n1 !g dash
           ! gg2=(-n1+2.d0*n2)*pi
           gg1=n1*gvec(1,1)+n2*gvec(1,2)
           gg2=n1*gvec(2,1)+n2*gvec(2,2)
           ggg1=g1-gg1          !g-(g dash)
           ggg2=g2-gg2

           ! write(333,*) gg1,gg2,ggg1,ggg2

           dp=(wv(1,ii)+g1)*(wv(1,ii)+gg1)&
                +(wv(2,ii)+g2)*(wv(2,ii)+gg2)
           ! write(444,*) dp

           if(ggg1.eq.0.d0)then
              if(ggg2.eq.0.d0)then !ok
                 amat(i,j)=dp
              else
                 gtmp=-sqrt(3.d0)*ggg2*0.5d0
                 amat(i,j)=ione*(exp(ione*gtmp)-1.d0)/gtmp*dp !ok
                 ! write(888,*) i,j,amat(i,j)
              end if
           else
              gtmp=(sqrt(3.d0)*ggg2+ggg1)*0.5d0
              if(gtmp.eq.0.d0)then
                 amat(i,j)=ione*(exp(-ione*ggg1)-1.d0)/ggg1*dp !ok
              else
                 ! write(555,*) ggg1,gtmp,ggg1*gtmp
                 amat(i,j)=(1.d0-exp(-ione*ggg1))*(exp(-ione*gtmp)-1.d0)/(ggg1*gtmp)*dp !ok
                 ! write(888,*) i,j,amat(i,j)
              end if
           end if

        end do!j
     end do!i

     write(*,*) "amat constructed",ii
     do i=1,mm
        do j=1,mm
           write(777,*) amat(i,j)
        end do
        write(777,*)
     end do

     call ZGEEV("N","V",mm,amat(:,:),mm,W(:), VL, 1, VR, mm,WORK, LWORK, RWORK, INFO )

     write(*,*) "eigensolver finished",ii


     do i=1,mm
        if(dreal(w(i)).ge.0.d0) then
           if(ii.le.nb)then
              write(999,*) -sqrt(dot_product(wv(:,ii),wv(:,ii))),sqrt(real(w(i))),sqrt(aimag(w(i)))
           elseif(ii.le.2*nb)then
              write(999,*) sqrt(dot_product(wv(:,ii),wv(:,ii))),sqrt(real(w(i))),sqrt(aimag(w(i)))
           else
              write(999,*) 0.5d0*sqrt(dot_product(gvec(:,1),gvec(:,1)))+&
                   sqrt(dot_product(wv(:,ii)-0.5d0*gvec(:,1),wv(:,ii)-0.5d0*gvec(:,1)))&
                   ,sqrt(real(w(i))),sqrt(aimag(w(i)))
           end if
           ! write(999,*) dble(ii),sqrt(dreal(w(i))),dimag(w(i))
           ! write(999,*) wv(:,ii),sqrt(dreal(w(i)))
        end if
        ! end if
     end do
     write(*,*) "end output"


  end do !ii

  ! do ii=-nb,2*nb
  !    do i=1,mm
  !       if(real(w(i,ii)).ge.0.d0)then
  !          write(999,*) dble(ii),sqrt(real(w(i,ii))),aimag(w(i,ii))
  !       end if
  !    end do
  ! end do

  deallocate(w)
  deallocate(vl)
  deallocate(vr)
  deallocate(work)
  deallocate(rwork)

  stop
end program main
