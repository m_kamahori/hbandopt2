import matplotlib.pyplot as plt
import numpy as np

xmin=0
ymin=0
xmax=100
ymax=100
xsize=xmax-xmin
ysize=ymax-ymin

inpfile='uin.dat'

xtmp = np.arange(xmin,xmax+1)
ytmp = np.arange(ymin,ymax+1)
cx, cy = np.meshgrid(xtmp, ytmp)

re=np.zeros((xsize+1,ysize+1))
im=np.zeros((xsize+1,ysize+1))
u=np.loadtxt(inpfile)

fin = open(inpfile)
cnt=0
for line in fin:
    x=int(u[cnt][0])
    y=int(u[cnt][1])
    re[x-xmin][y-ymin]=u[cnt][2]
    im[x-xmin][y-ymin]=u[cnt][3]
    cnt+=1

fin.close()

fig=plt.figure(figsize=(9.0,9.0))
plt.pcolor(cy,cx,re,vmin=-1,vmax=1)

# meshfile='../is/is.pt'
# meshfile='./is.pt'
meshfile='./meshsample.pt'
fin = open(meshfile)

meshx=[]
meshy=[]
for line in fin:
    dat = line.rstrip('\n')
    dat = dat.split(',')
    if float(dat[0]) == -1.0:
        plt.plot(meshx,meshy,color='black',linewidth=5.0)
        meshx=[]
        meshy=[]
    else:
        meshx.append(float(dat[0]))
        meshy.append(float(dat[1]))
fin.close()

outfile='./uin.png'
plt.savefig(outfile)

plt.show()
