import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(9.0,9.0))
ax = fig.add_subplot(111)

fin = open("block.res")
x=[]
y=[]

for line in fin:
    dat = line.rstrip('\n')
    dat = dat.split(',')
    if int(dat[0]) == -1:
        if int(dat[2]) == 0:    # admissible
            h = abs(x[0]-x[2])
            w = abs(y[0]-y[2])
            rect = plt.Rectangle((y[0],x[0]),w,h,fc='gray',linewidth=0.2)
            ax.add_patch(rect)
            # ax.plot(y,x,color='black',linewidth=1.5)
        elif int(dat[2]) == 1:  # inadmissible
            h = abs(x[0]-x[2])
            w = abs(y[0]-y[2])
            rect = plt.Rectangle((y[0],x[0]),w,h,fc='white',linewidth=0.2)
            ax.add_patch(rect)
            # ax.plot(y,x,color='black',linewidth=1.5)
        x=[]
        y=[]
    elif int(dat[0]) == -2:
        rsize = int(dat[1])
        csize = int(dat[2])
    else:
        x.append(int(dat[0]))
        y.append(int(dat[1]))
fin.close()

plt.axis([0,csize,rsize,0])
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)

plt.show()
outfile='./block.png'
plt.savefig(outfile) 





