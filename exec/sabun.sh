#cd ../src && cp makefile_prop makefile && make clean && make && cd ../exec
DIR=/home/m_kamahori/git-repos/hbandopt2/sabun2/exec/data/41
mkdir $DIR
HOME=`pwd`
deltaphi=1.d-3
mflag=1;idiv=1
for xe in 8 16 32 64 128 #256 512 1024 #2048 4096 #`seq 21 10 201`
do
    cd $HOME
    r=0.1				# PECの半径
    ###形状導関数の計算###
    echo "xe=$xe, r=$r"
    cd ../src &&  make && cd $HOME
    step1=0;step2=0;islv=-1
    if [ $step1 -eq 0 ];then
    	rm band/*.bnd bp/*.bp lsf/*.ls2 ip2/*.ip2 ip/*.ip
    	if [ $mflag -eq 0 ];then
    	    mesher=circle_peri.f90
    	    cd mesh && gfortran $mesher && echo $xe 0${r}d0 | ./a.out&& cd -
    	    # mesher=square.f90
    	    # cd mesh && gfortran $mesher && echo 1280 40  | ./a.out&& cd -
    	fi
    fi
    echo 32                !threadの数 > input
    echo $mflag           !初期形状を0:外部プログラムで生成，1:内部プログラムで生成。 >> input
    echo $xe              !x yousosu >> input # 偶数のみ
    echo $xe              !y yousosu >> input
    echo 1.d0             !a_1 no okisa >> input
    echo 1.d0             !a_2 no okisa >> input
    echo 9.d1             !a_1 to a_2 no nasu kaku >> input
    echo 2                !0:assymmetry, 1:1/4 symmetry, 2:1/8 symmetry >> input
    echo 0                !0:gamma点なし, 1:gamma点あり >> input
    echo ${r}d0            !ana no hankei >> input
    echo ${islv}	      !-1:amat, 0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat >> input
    echo 100	      !ev no saidai kosu >> input
    echo 32              !nskbn:積分点の数 >> input
    echo 2                !sekibun en no kazu >> input
    echo 2.d0             !rad:積分円の半径 >> input
    echo 0.2d0            !mrate:積分円の半径に対するマージンの割合 >> input
    # echo 1                !sekibun en no kazu >> input
    # echo 4.03d0 0.d0       !1st sekibun en no chusin >> input
    # echo 2.d-1             !rad:積分円の半径 >> input
    # echo 0.05d0            !margin:積分円のマージン >> input
    echo 5                !nblck:blockの数、固有値の重複度がnblck以下になるようにしておく >> input
    echo 10               !nhank:Hankel行列のサイズ >> input
    echo $step1           !start step >> input
    echo $step2           !end step >> input
    echo $idiv            !1youso no bunkatsusu >> input
    echo 1                !chiisaku shitai mode >> input
    echo 2                !okiku shitai mode >> input
    echo '"'${DIR}'"'           !lsf dirctory >>input
    echo ${deltaphi}      !レベルセット関数の増分>> input
    ./a.out < input
    cp fort.999 ${DIR}/sd.dat_${xe}
    ###形状導関数の計算###
    ###差分の計算###
    cd ../sabun2/exec
    # r=`echo "scale=15; ${r}+${v}*${epsln}"|bc`
    echo "####sabun xe=$xe####"
    cd ../src && make && cd -
    step1=1;step2=`awk 'NR==1' ${DIR}/sd.dat_${xe}`;islv=-1
    if [ $step1 -eq 0 ];then
    	rm band/*.bnd bp/*.bp lsf/*.ls2 ip2/*.ip2 ip/*.ip
    	if [ $mflag -eq 0 ];then
    	    mesher=circle_peri.f90
    	    cd mesh && gfortran $mesher && echo $xe 0${r}d0 | ./a.out&& cd -
    	    # mesher=square.f90
    	    # cd mesh && gfortran $mesher && echo 1280 40  | ./a.out&& cd -
    	fi
    fi
    echo 32                !threadの数 > input
    echo $mflag           !初期形状を0:外部プログラムで生成，1:内部プログラムで生成。 >> input
    echo $xe              !x yousosu >> input # 偶数のみ
    echo $xe              !y yousosu >> input
    echo 1.d0             !a_1 no okisa >> input
    echo 1.d0             !a_2 no okisa >> input
    echo 9.d1             !a_1 to a_2 no nasu kaku >> input
    echo 2                !0:assymmetry, 1:1/4 symmetry, 2:1/8 symmetry >> input
    echo 0                !0:gamma点なし, 1:gamma点あり >> input
    echo ${r}d0            !ana no hankei >> input
    echo ${islv}	      !-1:amat, 0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat >> input
    echo 100	      !ev no saidai kosu >> input
    echo 32              !nskbn:積分点の数 >> input
    echo 2                !sekibun en no kazu >> input
    echo 2.d0             !rad:積分円の半径 >> input
    echo 0.2d0            !mrate:積分円の半径に対するマージンの割合 >> input
    # echo 1                !sekibun en no kazu >> input
    # echo 4.03d0 0.d0       !1st sekibun en no chusin >> input
    # echo 2.d-1             !rad:積分円の半径 >> input
    # echo 0.05d0            !margin:積分円のマージン >> input
    echo 5                !nblck:blockの数、固有値の重複度がnblck以下になるようにしておく >> input
    echo 10               !nhank:Hankel行列のサイズ >> input
    echo $step1           !start step >> input
    echo $step2           !end step >> input
    echo $idiv                !1youso no bunkatsusu >> input
    echo 1                !chiisaku shitai mode >> input
    echo 2                !okiku shitai mode >> input
    # echo 36		  !points at which shape derivatives are calculated >> input
    echo '"'${DIR}'"'           !lsf dirctory >>input
    ./a.out < input
    ###差分の計算###
done
