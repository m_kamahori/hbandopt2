program mesh_square
  implicit none
  integer::i,itmp,ic
  integer::nperi(4),nq,nqmax,nc
  integer,allocatable::n(:)     !各円の要素数
  real(8)::x,y,rad,le,rmax
  integer,parameter::nqmin=10   !最小半径の円の要素数
  integer,parameter::nqdelta=4
  real(8),parameter::pi=acos(-1.d0)
  real(8),parameter::pich=0.1d0
  real(8),parameter::h1=0.0129534d0
  real(8),parameter::h2=0.0103752d0
  real(8),parameter::l1=1.d0
  real(8),parameter::margin=0.05d0
  real(8)::theta,thetamin,thetamax

  read(*,*) nq,itmp

!!!!!!!!!!!!!!!!!!!円の個数と各円の要素数を決める!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  nperi(:)=itmp
  le=l1/dble(nperi(1))          !要素長
  rmax=l1/2.d0-margin           !rmaxの初期値
  rad=dble(nq)*le/(2*pi)
  if(rad > rmax)then
     itmp=nq
     do while (rad > rmax)
        itmp=itmp-1
        rad=dble(itmp)*le/(2*pi)
     end do
     nqmax=itmp                 !最大半径の円の要素数
     ! write(*,*) "nqmax=",nqmax

     i=1
     do while (itmp<nq-nqdelta)      !円の個数を決めるループ
        i=i+1
        itmp= i*(2*nqmax-(i-1)*nqdelta)/2 !i個の円の要素数の総和
        if(i>nqmax/nqdelta)stop "nqdelta or nq is too large"
     end do
     nc=i

     ! write(*,*) "nc=",nc
     allocate(n(0:nc))
     n(0)=0
     itmp=nqmax
     do ic=1,nc-1
        n(ic)=itmp
        itmp=itmp-nqdelta
     end do
     n(nc)=nq-sum(n(1:nc-1))
  else
     nc=1
     allocate(n(0:nc))
     n(0)=0
     n(1)=nq
  end if
!!!!!!!!!!!!!!!!!!!円の個数と各円の要素数を決める!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! write(*,*) n(:)

  open(101,file="step0000.el2")
  write(101,*) nq+sum(nperi)

  ! en
  do ic=1,nc
     rad=dble(n(ic))*le/(2*pi)
     theta=(-1.d0)**ic*2.d0*pi/dble(n(ic))
     do i=1,n(ic)
        x=rad*cos(theta*dble(i))
        y=rad*sin(theta*dble(i))
        write(101,*) i+sum(n(0:ic-1)),x+0.5d0,y+0.5d0
     end do
  end do

  ! peri
  x=-0.5d0
  y=-0.5d0
  ! shita
  do i=1,nperi(1)
     x=x+1.d0/dble(nperi(1))
     !            節点番号       ,x座標  ,y座標
     write(101,*) i+nq,x+0.5d0,y+0.5d0
  end do
  ! migi
  do i=1,nperi(2)
     y=y+1.d0/dble(nperi(2))
     write(101,*) i+nq+nperi(1),x+0.5d0,y+0.5d0
  end do
  ! ue
  do i=1,nperi(3)
     x=x-1.d0/dble(nperi(3))
     write(101,*) i+nq+sum(nperi(1:2)),x+0.5d0,y+0.5d0
  end do
  ! hidari
  do i=1,nperi(4)
     y=y-1.d0/dble(nperi(4))
     write(101,*) i+nq+sum(nperi(1:3)),x+0.5d0,y+0.5d0
  end do
  


  write(101,*) nq+sum(nperi)

  ! en
  do ic=1,nc
     do i=1,n(ic)-1
        write(101,*) i+sum(n(0:ic-1)),i+sum(n(0:ic-1)),i+sum(n(0:ic-1))+1,1,0,0
     end do
     write(101,*) sum(n(0:ic)),sum(n(0:ic)),sum(n(0:ic-1))+1,1,0,0
  end do

  ! peri
  !               要素番号         ,node番号              ,node番号              ,ibc,iperi(:,:)
  write(101,*)    nq+1             ,nq+sum(nperi)  ,nq+1            ,10 ,-2,nq+sum(nperi(1:2))+1
!shita
  do i=2,nperi(1) 
     write(101,*) i+nq             ,nq+i-1        ,nq+i            ,10 ,-2,nq+sum(nperi(1:2))+i
  end do
!hidari
  do i=1,nperi(4)
     write(101,*) i+nq+nperi(1)       ,nq+sum(nperi)-i,nq+sum(nperi)-i+1,10 ,-1,nq+sum(nperi(1:3))+i
  end do
!ue
  do i=1,nperi(3)
     write(101,*) i+nq+sum(nperi(1:2)),nq+sum(nperi(1:3))-i  ,nq+sum(nperi(1:3))-i+1  ,10 ,2 ,nq+i
  end do
!migi
  do i=1,nperi(2)
     write(101,*) i+nq+sum(nperi(1:3)),nq+nperi(1)+i-1     ,nq+nperi(1)+i         ,10 ,1 ,nq+nperi(1)+i
  end do

  close(101)

end program mesh_square
