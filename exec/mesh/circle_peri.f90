program circle_peri
  implicit none
  !---------inputs-----------
  integer::nq!ノイマン境界の要素数
  integer::n !周期境界の一辺の分割数
  integer::nip                  !内点の数
  integer,parameter::flag=0     !内点の間隔を境界要素長にあわせる(0)かどうか(1)
  integer,parameter::nx=40      !境界要素長にあわせない場合の格子点数(x_1方向)
  real(8)::cx!中心のx座標
  real(8)::cy!中心のy座標
  real(8),parameter::a1=1.d0    !単位格子ベクトルの大きさ
  real(8)::el                   !要素長
  !--------------------------
  integer::i,j,itmp
  real(8)::x,y,theta,pi,l2min,l2,r,xo,yo,dx
  real(8),allocatable::grid(:,:)

  integer::nn(4)

  read(*,*) n,r

  el=a1/n
  pi=acos(-1.d0)

  ! 周期境界とノイマン境界の要素長の差が最小になるnqを決定
  nq=2
  l2min=(2.d0*pi*r-el)**2
  l2=(2.d0*r*sin(pi/nq)-el)**2
  do while(l2min > l2)
     l2min=l2
     nq=nq+1
     !半径rの円をnq分割した時の要素長と周期境界の要素長の差の2-norm     
     l2=(2.d0*r*sin(pi/nq)-el)**2
  end do
  nq=nq-1

  theta=2.d0*pi/nq
  nn(:)=n

  open(1,file="step0000.el2")
  ! !!!!!!!!!!!!!!node!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  write(1,*) nq+sum(nn)
  ! physical boundary
  cx=a1*0.5d0;cy=a1*0.5d0
  do i=1,nq
     x=cx+r*cos(-i*theta)
     y=cy+r*sin(-i*theta)
     write(1,*) i,x,y
  end do
  ! periodic boundary
  x=-0.5d0
  y=-0.5d0
  ! shita
  do i=1,nn(1)
     x=x+a1/dble(nn(1))
     !          節点番号       ,x座標  ,y座標
     write(1,*) i+nq,x+0.5d0,y+0.5d0
  end do
  ! migi
  do i=1,nn(2)
     y=y+a1/dble(nn(2))
     write(1,*) i+nq+nn(1),x+0.5d0,y+0.5d0
  end do
  ! ue
  do i=1,nn(3)
     x=x-a1/dble(nn(3))
     write(1,*) i+nq+sum(nn(1:2)),x+0.5d0,y+0.5d0
  end do
  ! hidari
  do i=1,nn(4)
     y=y-a1/dble(nn(4))
     write(1,*) i+nq+sum(nn(1:3)),x+0.5d0,y+0.5d0
  end do
  ! !!!!!!!!!!!!!!node!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! !!!!!!!!!!!!!!element!!!!!!!!!!!!!!!!!!!!!!!!!
  write(1,*) nq+sum(nn)
  !             要素番号         ,node番号    ,node番号        ,ibc,iperi(:,:)
  write(1,*)    1                ,nq          ,1               ,1  ,0 ,0
  do i=2,nq
     write(1,*) i                ,i-1         ,i               ,1  ,0 ,0
  end do
  write(1,*)    nq+1             ,nq+sum(nn)  ,nq+1            ,10 ,-2,nq+sum(nn(1:2))+1
!shita
  do i=2,nn(1) 
     write(1,*) i+nq             ,nq+i-1        ,nq+i            ,10 ,-2,nq+sum(nn(1:2))+i
  end do
!hidari
  do i=1,nn(4)
     write(1,*) i+nq+nn(1)       ,nq+sum(nn)-i,nq+sum(nn)-i+1,10 ,-1,nq+sum(nn(1:3))+i
  end do
!ue
  do i=1,nn(3)
     write(1,*) i+nq+sum(nn(1:2)),nq+sum(nn(1:3))-i  ,nq+sum(nn(1:3))-i+1  ,10 ,2 ,nq+i
  end do
!migi
  do i=1,nn(2)
     write(1,*) i+nq+sum(nn(1:3)),nq+nn(1)+i-1     ,nq+nn(1)+i         ,10 ,1 ,nq+nn(1)+i
  end do
  ! !!!!!!!!!!!!!!element!!!!!!!!!!!!!!!!!!!!!!!!!
  close(1)

  if(flag.eq.0)then
     dx=el
  else
     n=nx
     dx=a1/n
  end if
  allocate(grid(2,n**2))
  y=dx*0.5d0
  itmp=0
  do j=1,n
     yo=y-cy
     x=dx*0.5d0
     do i=1,n
        xo=x-cx
        if(sqrt(xo**2+yo**2) > r)then
           itmp=itmp+1
           grid(1,itmp)=x
           grid(2,itmp)=y
        end if
        x=x+dx
     end do
     y=y+dx
  end do
  nip=itmp
  open(1,file="../ip2/step0000.ip2")
  write(1,*) nip
  do i=1,nip
     write(1,*) i,grid(:,i)
  end do
  close(1)
  
  deallocate(grid)
  stop
  
end program circle_peri
