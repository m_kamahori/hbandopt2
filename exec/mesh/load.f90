subroutine load
  use globals
  implicit none
  integer::i,j,itmp

  !各種データ読み込み
  read(*,*) meshfile !メッシュファイル
  read(*,*) naitenfile !内点ファイル
  read(*,*) rho      !密度
  read(*,*) kappa    !体積弾性率
  read(*,*) w        !角周波数
  read(*,*) z        !インピーダンス
  read(*,*) ainc     !入射角
  read(*,*) tol,ldstrt !GMRESの許容誤差、リスタート定数

  !メッシュデータの読み込み
  open(2,file=meshfile)
  read(2,*) np
  allocate(p(2,np));call allomem(iid,2*np,imem)
  do i=1,np
     read(2,*)itmp,p(:,i)
  end do
  read(2,*) ne
  allocate(nd(2,ne));call allomem(iid,2*ne,imem)
  do i=1,ne
     read(2,*)itmp,nd(:,i)
  end do
  close(2)

  allocate(c(2,ne));call allomem(iid,2*ne,imem)!要素中心の座標
  do i=1,ne
     c(:,i)=0.5d0*(p(:,nd(1,i))+p(:,nd(2,i)))
  end do

  !メッシュを試し書き
  do i=1,ne
     do j=1,2
        write(88,*) p(:,nd(j,i))
     end do
     write(88,*)
     write(88,*)
  end do

  !入射波
  ainc=ainc*pi/180.d0
  pinc(1)=cos(ainc) !入射波の進む方向
  pinc(2)=sin(ainc)
  wn=w*sqrt(rho/kappa)
  !GMRES
  lwork=ldstrt**2+ldstrt*(ne+5)+5*ne+1 !作業用領域のサイズ
  allocate(work(lwork));call allomem(iiz,lwork,imem) !作業用

  !法線ベクトルを設定
  allocate(an(2,ne))   !要素法線
  allocate(at(2,ne))   !要素接線
  call allomem(iid,4*ne,imem)
  call normal_vectors

  allocate(amat(ne,ne))!BEMの係数行列
  allocate(smat(ne,ne))!一重層(adjointの右辺の計算で必要なので記憶する)
  call allomem(iiz,2*ne*ne,imem)

  !内点を読み込み
  open(1,file=naitenfile)
  read(1,*) nip
  allocate(grid(2,nip))!内点の座標
  call allomem(iid,2*nip,imem)
  do i=1,nip
     read(1,*) itmp,grid(:,i)
  end do
  do i=1,nip
     write(84,*) grid(:,i)
  end do

  allocate(lsf(nip))!内点が計算領域に属するなら正
  call allomem(iid,nip,imem)
  lsf(:)=1.d0
  do i=1,nip
     if(grid(1,i)**2+grid(2,i)**2.le.10.d0**2)lsf(i)=-1.d0
  end do

  !time averaged energy
  allocate(energy(2,nip))
  call allomem(iid,2*nip,imem)
  
end subroutine load
